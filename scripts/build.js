'use strict';

// Do this as the first thing so that any code reading it knows the right env.
process.env.BABEL_ENV = 'production';
process.env.NODE_ENV = 'production';

// Makes the script crash on unhandled rejections instead of silently
// ignoring them. In the future, promise rejections that are not handled will
// terminate the Node.js process with a non-zero exit code.
process.on('unhandledRejection', err => {
  throw err;
});

// Ensure environment variables are read.
require('../config/env');

const path = require('path');
const chalk = require('chalk');
const fs = require('fs-extra');
const webpack = require('webpack');
const config = require('../config/webpack.config.prod');
const paths = require('../config/paths');
const checkRequiredFiles = require('react-dev-utils/checkRequiredFiles');
const formatWebpackMessages = require('react-dev-utils/formatWebpackMessages');
const printHostingInstructions = require('react-dev-utils/printHostingInstructions');
const FileSizeReporter = require('react-dev-utils/FileSizeReporter');

const measureFileSizesBeforeBuild =
  FileSizeReporter.measureFileSizesBeforeBuild;
const printFileSizesAfterBuild = FileSizeReporter.printFileSizesAfterBuild;
const useYarn = fs.existsSync(paths.yarnLockFile);

// These sizes are pretty large. We'll warn for bundles exceeding them.
const WARN_AFTER_BUNDLE_GZIP_SIZE = 512 * 1024;
const WARN_AFTER_CHUNK_GZIP_SIZE = 1024 * 1024;

// Warn and crash if required files are missing
if (!checkRequiredFiles([paths.appHtml, paths.appIndexJs])) {
  process.exit(1);
}

// First, read the current file sizes in build directory.
// This lets us display how much they changed later.
measureFileSizesBeforeBuild(paths.appBuild)
  .then(previousFileSizes => {
    // Remove all content but keep the directory so that
    // if you're in it, you don't end up in Trash
    fs.emptyDirSync(paths.appBuild);
    // Merge with the public folder
    copyPublicFolder();
    console.log('start read public config', paths.appBuild);
    fs.readFile(paths.appBuild + '/config.js', "utf8", (err, data) => {
      if (err) throw err;
      let str = "process.config = " + data.slice(data.indexOf("{"));
      fs.writeFile(paths.appBuild + '/config_temp.js', str, err => {
        if (err) throw err;
        console.log('temp config created success');
        require(paths.appBuild + '/config_temp.js');
        console.log("process temp config", process.config.version);
        process.config.environments = {
          ifaas: {
            host: "127.0.0.1:8066",
            apiBaseURL: "http://127.0.0.1:8083",
            centerProxyServerPrefix: 'http://127.0.0.1', //中心代理服务器前缀
            mqttServer: {
              host: "127.0.0.1",
              port: 9001
            },
            domain: {
              replaceDomain: false
            },
            pathMiddle: ""
          },
          internet: {
            host: '183.3.223.120:8066',
            apiBaseURL: 'http://183.3.223.120:7083', //api端口
            centerProxyServerPrefix: 'http://127.0.0.1', //中心代理服务器前缀
            mqttServer: {
              host: '192.168.11.65',
              port: 9001
            },
            pathMiddle: "",
            domain: {
              replaceDomain: true,
              path: {
                1:{
                  src: /192.168.11.193:80/g,
                  dst: '183.3.223.120:7081'
                },
                2:{
                  src: /192.168.11.193:8066/g,
                  dst: '183.3.223.120:7081'
                },
                3:{
                  src: /192.168.2.27:80/g,
                  dst: '183.3.223.120:7081'
                },
                4:{
                  src: /192.168.11.193/g,
                  dst: '183.3.223.120:7081'
                }
              }
            }
          }
        }
        process.config.centerProxyServerPrefix = 'http://127.0.0.1';
        let configStr = "window.config = " + JSON.stringify(process.config, null, 4);
        fs.writeFile(paths.appBuild + '/config.js', configStr, err => {
          if (err) throw err;
          console.log('change config enviroment success');
          fs.unlink(paths.appBuild + '/config_temp.js', err => {
            if (err) throw err;
            console.log('delete temp config success')
          })
        })
      })
    })
    // Start the webpack build
    return build(previousFileSizes);
  })
  .then(
    ({ stats, previousFileSizes, warnings }) => {
      if (warnings.length) {
        console.log(chalk.yellow('Compiled with warnings.\n'));
        console.log(warnings.join('\n\n'));
        console.log(
          '\nSearch for the ' +
            chalk.underline(chalk.yellow('keywords')) +
            ' to learn more about each warning.'
        );
        console.log(
          'To ignore, add ' +
            chalk.cyan('// eslint-disable-next-line') +
            ' to the line before.\n'
        );
      } else {
        console.log(chalk.green('Compiled successfully.\n'));
      }

      console.log('File sizes after gzip:\n');
      printFileSizesAfterBuild(
        stats,
        previousFileSizes,
        paths.appBuild,
        WARN_AFTER_BUNDLE_GZIP_SIZE,
        WARN_AFTER_CHUNK_GZIP_SIZE
      );
      console.log();

      const appPackage = require(paths.appPackageJson);
      const publicUrl = paths.publicUrl;
      const publicPath = config.output.publicPath;
      const buildFolder = path.relative(process.cwd(), paths.appBuild);
      printHostingInstructions(
        appPackage,
        publicUrl,
        publicPath,
        buildFolder,
        useYarn
      );
    },
    err => {
      console.log(chalk.red('Failed to compile.\n'));
      console.log((err.message || err) + '\n');
      process.exit(1);
    }
  );

// Create the production build and print the deployment instructions.
function build(previousFileSizes) {
  console.log('Creating an optimized production build...');

  let compiler = webpack(config);
  return new Promise((resolve, reject) => {
    compiler.run((err, stats) => {
      if (err) {
        return reject(err);
      }
      const messages = formatWebpackMessages(stats.toJson({}, true));
      if (messages.errors.length) {
        return reject(new Error(messages.errors.join('\n\n')));
      }
      if (
        process.env.CI &&
        (typeof process.env.CI !== 'string' ||
          process.env.CI.toLowerCase() !== 'false') &&
        messages.warnings.length
      ) {
        console.log(
          chalk.yellow(
            '\nTreating warnings as errors because process.env.CI = true.\n' +
              'Most CI servers set it automatically.\n'
          )
        );
        return reject(new Error(messages.warnings.join('\n\n')));
      }
      return resolve({
        stats,
        previousFileSizes,
        warnings: messages.warnings,
      });
    });
  });
}

function copyPublicFolder() {
  fs.copySync(paths.appPublic, paths.appBuild, {
    dereference: true,
    filter: file => file !== paths.appHtml,
  });
}
