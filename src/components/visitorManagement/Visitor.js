

import React, {Component} from 'react';
import {Row, Col, Card, Icon, Input, Select, Button, Table,Modal,Cascader,Pagination, Spin} from 'antd';
import BreadcrumbCustom from '../BreadcrumbCustom';
import ModalForm from '../systemAccount/ModalForm';
import UpLoadModal from '../userManagement/components/UpLoadModal';
import intl from 'react-intl-universal';
import request from '@/utils/request.js';
// import {deleteStudentInfo} from '@/actions/userManagement/StudentsManagementAction';
import './Visitor.less';
import VisitorDetail from './VisitorDetail'
const confirm = Modal.confirm;
const Search = Input.Search;
const Option = Select.Option;
const ExampleFace = 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png';

const initdata = [
  {
    id:1,
    name:'name',
    className:'三年级8班',
    photoData: ExampleFace,
    //  接口字段
    idCard:456135134568,
    phone:'13660721158',
    pStatus: '1',//学生(1、在籍 2、不在籍)
    gender:'男',
    age: '中年',
    detailId: 1451
  },
  {
    id:2,
    name:'name',
    idCard:456135134568,
    className:'三年级2班',
    photoData: ExampleFace,
    phone:'13660721158',
    pStatus: '2',//学生(1、在籍 2、不在籍)
    gender:'女',
    age: '中年',
    detailId: 1451

  },
];

class Visitor extends Component {
  state={
    selectedRowKeys: [], // Check here to configure the default column
    page: 1,
    pageSize: 10,
    total:20,
    spinning: false,
    searchName:'',//姓名或者学号
    classRoom:[], //【年级，班级】
    registered:'',// 在籍状态 all：全部  registered : 在籍   noRegistered：不在籍
    showDetail: false,
    sex: '',
    ageArea: '',
    visitorId: 0
  };

  componentWillMount() {
    console.log('sdj Visitor', this.props.location.state);
    // 初始化state
    this.setState({
      showDetail: false,
    });
    this.getTaskList({...this.state});

  }

  //改变信息的模板
  changeState = (name, value) => {
    this.setState({
      [name]:value
    })
  }

  searchName= (e)=> {
    this.changeState('searchName', e.target.value)
  }

  addAccount = (record) => {
    const {name, phone,sex, ageArea, photoData, idCard, detailId} = record;
    this.setState({
      showDetail: true,
      visitorId: detailId
    })
  }

  // 改变性别
  onSexChange = val => {
    this.changeState('sex', val);
  }

  handleAgeChange = val => {
    this.changeState('ageArea', val);
  }

  // //  导入
  // upLoad = () => {
  //   this.props.changeModalUpload({
  //     multiRegisterVisible: true,
  //     multiRegisterLoading: false,
  //   })
  // }

  //  导出
  downLoad = () => {
    const {searchName, classRoom, registered, page, pageSize} = this.state;
    this.props.exportExcel(
      {
        queryStr: searchName,
        page,
        pageSize,
        pType: 1,
        status: registered==='registered'?'1': registered==='noRegistered'?'2':'',
        gradeId: classRoom.length>1? classRoom[classRoom.length - 2]:'',
        classId: classRoom.length>0?classRoom[classRoom.length-1]:'',
      }
    )
  }

  onSelectChange = (selectedRowKeys) => {
    console.log('selectedRowKeys changed: ', selectedRowKeys);
    this.setState({ selectedRowKeys });
  }

  //删除
  showConfirm = (key) => {
    confirm({
      title: '你确定删除这条记录吗?',
      content: '当你点击确认的时候,就会在1s后删除所有相关的记录',
      okText: '确认',
      cancelText: '取消',
      onOk:() =>{
        return new Promise((resolve, reject) => {
          console.log('sdj deleteStudentInfo')
          setTimeout(resolve(this.props.deleteStudentInfo(key, this, this.state)), 1000);
        }).catch(() => console.log('Oops errors!'));
      },
      onCancel() {
      },
    });
  }

  //  操作记录
  recordHandle = (record, type=1) => {
    console.log('sdj record', record)
    this.props.history.push({
      pathname: `/app/userManagement/studentsManagementRecord`,
      state:{...record, type}
    });
  }

  // 搜索查询
  searchButton= ()=> {
    const {searchName, sex, ageArea, page, pageSize} = this.state;
    console.log('sdj', searchName, sex, ageArea, page, pageSize);
    this.getTaskList({
      ...this.state,
      page: 1
    })
    // 重置搜索显示的page
    this.setState({
      page: 1
    })
  }

  //获取任务列表
  getTaskList = ({searchName, sex, ageArea, page, pageSize},isFirst = false) => {
    this.props.getVisitorList({
      queryStr: searchName,
      page,
      pageSize,
      sex,
      ageArea,
      pType: 5
    },isFirst)
  }

  // 分页
  changePage = (page, pageSize)=> {
    console.log('sdj',page)
    this.setState({
      page: page
    },() => {
      this.getTaskList({...this.state})
    });
    //  发请求
  }

  // 调整每页的数量
  onShowSizeChange = (current, pageSize)=> {
    console.log('sdj', current, pageSize);
    this.setState({
      page:1,
      pageSize: pageSize,
      spining: true,
    }, () => {
      this.getTaskList({...this.state})
    });
  }

  render() {
    const { selectedRowKeys, page, pageSize, showDetail, visitorId} = this.state;
    const searchStyle = {
      width: 200,
      marginRight: 20,
      marginBottom: 5
    };

    const tableHandle ={
      margin:'0 10px'
    };

    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
      hideDefaultSelections: true,
      selections: [{
        key: 'all-data',
        text: '选择全部',
        onSelect: () => {
          this.setState({
            selectedRowKeys: [...Array(46).keys()], // 0...45
          });
        },
      }, {
        key: 'odd',
        text: '选择奇数列',
        onSelect: (changableRowKeys) => {
          let newSelectedRowKeys = [];
          newSelectedRowKeys = changableRowKeys.filter((key, index) => {
            if (index % 2 !== 0) {
              return false;
            }
            return true;
          });
          this.setState({ selectedRowKeys: newSelectedRowKeys });
        },
      }, {
        key: 'even',
        text: '选择偶数列',
        onSelect: (changableRowKeys) => {
          let newSelectedRowKeys = [];
          newSelectedRowKeys = changableRowKeys.filter((key, index) => {
            if (index % 2 !== 0) {
              return true;
            }
            return false;
          });
          this.setState({ selectedRowKeys: newSelectedRowKeys });
        },
      }],
      onSelection: this.onSelection,
    };
    const tableData = this.props.visitorList && this.props.visitorList.length? this.props.visitorList : initdata;
    const columns=[
      {title:'姓名',className: 'tab-1',dataIndex:'',key:'name', render: (text, record) =>(
        <span className="sudents-col-name">{record.name}</span>
      )},
      {title:'联系电话',className: 'tab-2',dataIndex:'phone',key:'phone'},
      {title:'身份证号',className: 'tab-3',dataIndex:'idCard',key:'IDcard',},
      // {title:'性别',className: 'tab-4',dataIndex:'age',key:'age',},
      {title:'人像照片',className: 'tab-5',dataIndex:'faceImg',key:'faceImg',render: (text, record) => (
        <img src={record.photoData ? record.photoData:''} alt={""} />
      )},
      {title:'操作',className:'', dataIndex:'',key:'y', render: (text,record) => (
      <span>
        <Button style={tableHandle} title="查看" onClick={this.addAccount.bind(this, record)}>
          <Icon type="edit" />
        </Button>
        {/*<Button style={tableHandle} title="删除"onClick={this.showConfirm.bind(this, record.detailId, 'delete')}>*/}
          {/*<Icon type="delete" />*/}
        {/*</Button>*/}
      </span>
    )}
    ];
    const clientHeight = document.body.clientHeight;
    const tableHeight =(clientHeight - 370);
    console.log('sdj clientHeight',tableHeight)
    return (
      <div className="gutter-example">
        <BreadcrumbCustom first="访客管理" second="访客档案" />
        <div style={{display: !showDetail?'block':'none'}}>
          <Row gutter={16}>
            <Col className="gutter-row" md={24}>
              <div className="gutter-box">
                <Input
                  placeholder="请输入访客姓名"
                  onChange={this.searchName}
                  style={searchStyle}
                />

                <Select
                  showSearch
                  style={searchStyle}
                  // options={classRoomOptions}
                  onChange={this.onSexChange}
                  placeholder="请选择性别"
                  // displayRender={this.displayRender}
                >
                  <Option value="">全部</Option>
                  <Option value="0">男</Option>
                  <Option value="1">女</Option>
                </Select>

                <Select
                  showSearch
                  style={searchStyle}
                  placeholder="请选择年龄"
                  onChange={this.handleAgeChange}
                  // onFocus={this.handleFocus}
                  // onBlur={this.handleBlur}
                >
                  <Option value="">全部</Option>
                  <Option value="0">儿童</Option>
                  <Option value="1">少年</Option>
                  <Option value="2">青年</Option>
                  <Option value="3">中年</Option>
                  <Option value="4">老年</Option>
                </Select>

                <Button onClick={this.searchButton} type="primary"><Icon type="search" />查询</Button>
                {/*<Button onClick={this.upLoad}><Icon type="upload" />导入</Button>*/}
                <Button onClick={this.downLoad}><Icon type="download" />导出</Button>
                {/*<Button><Icon type="download" />下载模板</Button>*/}
                {/*<UpLoadModal {...UpLoadModalProps} />*/}
              </div>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col className="gutter-row" md={24}>
              <div className="gutter-box">
                <Card bordered={false}>
                  {
                    !this.props.studentsList
                      // && this.props.studentsList.length
                      ?
                      (
                        <div className="student-management-content">
                          <Table
                            className="visitor-tab "
                            rowSelection={rowSelection}
                            columns={columns}
                            dataSource={tableData}
                            pagination={false}
                            scroll={{ y: tableHeight}}
                            bordered
                          />
                          <Pagination
                            key={1}
                            className="footer center "
                            total={this.props.total}
                            defaultCurrent={1}
                            showSizeChanger
                            showQuickJumper
                            current={page}
                            pageSize={pageSize}
                            defaultPageSize={10}
                            pageSizeOptions={['10','20','50']}
                            onShowSizeChange={this.onShowSizeChange}
                            onChange={this.changePage}
                          />
                        </div>
                      ):''
                  }
                </Card>
              </div>
            </Col>
          </Row>
        </div>

        <VisitorDetail
          showDetail={showDetail}
          onChangeProps={this.changeState}
          visitorId={visitorId}
          changeSpining={this.props.changeSpining}
        />

        <Spin
          className="wp spin-center zi1060"
          size="large"
          spinning={this.props.viewListSpinning}
        />
      </div>
    )
  }
}


export default Visitor;