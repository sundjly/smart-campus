import PropTypes from 'prop-types';
import React, {Component } from 'react';
import {Row, Col, Button, Card, Table, Pagination, DatePicker, message} from 'antd';
import './VisitorDetail.less';
import MonthPickerRange from '../userManagement/components/MonthPickerRange';
import moment from 'moment';
import {getMothEnd} from '@/utils/common';
import {getSigleVisitorList} from '@/actions/visitorManagement/VisitorAction'
const { MonthPicker } = DatePicker;
const ExampleFace = 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png';

class VisitorDetail extends Component {

  static propsTYpes ={
    showDetail: PropTypes.bool,
    onChangeProps: PropTypes.func,
    visitorDetail: PropTypes.object,
    changeSpining: PropTypes.func,

  }

  static defaultProps ={
    showDetail: false,// 是否展示
    onChangeProps: null,//
    visitorDetail: {},
    changeSpining: null, // 改变加载状态

  }

  state={
    page: 1,
    pageSize: 10,
    monthQuery: moment().format('YYYY-MM'),
    singleResData: [],
    total:0,
    visitorDetail:{
      visitorId: '',
      realName: "",
      realGender: 2,
      phone: "",
      cid: null,
      photoData: "http://192.168.2.15:80/batch1/api/uploads/2018-05-14-18-03-14-096_432731_format_f.jpg",
    },
    visitorId:''
  }

  componentWillMount () {
    this.setState({
      visitorId: this.props.visitorId?this.props.visitorId:''
    })
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.visitorId !== this.props.visitorId){
      // 获取数据然后展示
      this.setState({
        visitorId: nextProps.visitorId
      },()=>{
        this.getVisitorRecordList({...this.state});
      })
    }
  }

  changeMonth = (date, dateString) => {
    console.log(date, dateString);
    this.setState({
      monthQuery: dateString
    })
  }

  // 分页
  changePage = (page, pageSize)=> {
    console.log('sdj',page)
    this.setState({
      page: page
    },() => {
      this.getVisitorRecordList({...this.state});
    });
  }

  // 调整每页的数量
  onShowSizeChange = (current, pageSize)=> {
    console.log('sdj', current, pageSize);
    this.setState({
      page:1,
      pageSize: pageSize,
    }, () => {
      this.getVisitorRecordList({...this.state});
    });
  }

  searchRecord = ()=>{
    this.setState({
      page: 1,
      pageSize: 10
    },()=>{
      this.getVisitorRecordList({
        ...this.state,
      });
    })
  }
  //  搜索的方法
  getVisitorRecordList =({monthQuery, page, pageSize, visitorId}, isFirst = false) => {
    if(visitorId !=='' && visitorId){
      this.props.changeSpining(true);
      getSigleVisitorList({
        monthQuery,
        page,
        pageSize,
        visitorId
      }, isFirst).then(res => {
        if(!res.errCode && res.data && res.data.hasOwnProperty('recordList')){
          const {recordList, realName, ...visitorDetail} = res.data
          this.setState({
            singleResData: recordList.data,
            total: recordList.total,
            visitorDetail
          })
        }else{
          this.setState({
            singleResData: [],
            total: 0,
            visitorDetail:{}

          })
          message.error(`出现错误，返回${res.data}`)
        }
        this.props.changeSpining(false);
      })
    }else{
      message.error('获取不到访客的id')
    }
  }

  //返回
  returnRecord =()=> {
    this.props.onChangeProps('showDetail',false);
  }

  getRequireTime = (time,type) => {
    if(time){
      return moment(new Date(time)).format(type)
    }else{
      return '无时间';
    }
  }

  render()  {
    const {page, pageSize, visitorDetail} = this.state;
    // const {visitorDetail} = this.props;
    const columns=[
      {title:'日期',dataIndex:'',key:'name', render: (text, record)=> (
        <span>{this.getRequireTime(record.actualVisitTime, 'YYYY-MM')}</span>
      )},
      {title:'访问时间',dataIndex:'',key:'num',render :(text, record)=>(
        <span>{this.getRequireTime(record.actualVisitTime, 'HH:MM:SS')}</span>
      )},
      {title:'受访人',dataIndex:'intervieweeName',key:'intervieweeName',},
    ];
    return (
      <div
        className="visitor-detail"
        style={{display: this.props.showDetail?'block': 'none'}}
      >
        <span className="visitor-title">基本信息</span>
        <Row justify="center">

          <Col span={20} className="visitor-col">

            <Row gutter={16} className="row-margin">
              <Col span={4} className="col-right">
                姓名
              </Col>
              <Col span={20} className="col-left">
                {visitorDetail.realName?visitorDetail.realName:'无'}
              </Col>
            </Row>

            <Row gutter={16}  className="row-margin">
              <Col span={4} className="col-right">
                身份证号
              </Col>
              <Col span={20} className="col-left">
                {visitorDetail.cid?visitorDetail.cid:'无'}
              </Col>
            </Row>

            <Row gutter={16}  className="row-margin">
              <Col span={4} className="col-right">
                手机号
              </Col>
              <Col span={20} className="col-left">
                {visitorDetail.phone? visitorDetail.phone:'无'}
              </Col>
            </Row>
            <Row gutter={16} className="row-margin">
              <Col span={4} className="col-right">
                性别
              </Col>
              <Col span={20} className="col-left">
                {visitorDetail.realGender == 0? '男':visitorDetail.realGender == 1?'女':'无'}
              </Col>
            </Row>
            <Row gutter={16} className="row-margin">
              <Col span={4} className="col-right">
                年龄
              </Col>
              <Col span={20} className="col-left">
                换成无
              </Col>
            </Row>

          </Col>

          <Col span={4} className="visitor-col">
            <span>
              <img className="visitor-detail-img" src={ExampleFace} alt="" />
            </span>
          </Col>
        </Row>

        <span className="visitor-title">到访记录</span>
        <nav style={{margin:"10px 0"}}>
          {/*<MonthPickerRange*/}
            {/*startTime={startTime}*/}
            {/*endTime={endTime}*/}
            {/*onChange ={this.changeState}*/}
          {/*/>*/}
          <MonthPicker
            placeholder="Select month"
            onChange={this.changeMonth}
            style={{marginRight: 15}}
          />
          <Button  type="primary" onClick={this.searchRecord}>搜索</Button>
          <Button style={{float: "right", marginRight: 15}}onClick={this.returnRecord} className={"search-button"}>
            返回
          </Button>
        </nav>
        <Card className={"record-container"} bordered={false}>
          <Table
            columns={columns}
            dataSource={this.state.singleResData}
            bordered
            pagination={false}
          />
          <Pagination
            key={2}
            className="center footer"
            total={this.state.total}
            defaultCurrent={1}
            showSizeChanger
            showQuickJumper
            current={page}
            pageSize={pageSize}
            defaultPageSize={10}
            pageSizeOptions={['10','20','50']}
            onShowSizeChange={this.onShowSizeChange}
            onChange={this.changePage}
          />
        </Card>
      </div>
    )
  }
}

export default VisitorDetail;