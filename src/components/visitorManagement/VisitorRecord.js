import PropTypes from 'prop-types';

import React , {Component} from 'react';
import {Table, Button, Pagination ,Card, Spin, Input, Select, DatePicker} from 'antd';
import BreadcrumbCustom from '../BreadcrumbCustom';
import moment from 'moment';
// import MonthPickerRange from '../userManagement/components/MonthPickerRange';
import {getMothEnd} from '@/utils/common';
import VisitorDetail from './VisitorDetail';
const { MonthPicker } = DatePicker;

const marginStle={
  marginBottom: 15,
  marginRight: 15
}

const VISITOR_LIST = [
  {
    id:11,
    visitorName:'fasdfs',
    identityType: 1,
    actualVisitTime: 1526367887000,
    intervieweeName:'杨来时',
    visitorId:12321
  },
  {
    id:1,
    visitorName:'fasdfs',
    identityType: 0,
    actualVisitTime: 1526367887000,
    intervieweeName:'yige ',
    visitorId:12322
  }
]
const Option = Select.Option;
class VisitorRecord extends Component {
  state ={
    page: 1,
    pageSize: 10,
    monthQuery:"",
    visitorName:'',
    identityType:'',
    showDetail: false,
    visitorId:0
  }

  componentWillMount() {
    this.setState({
      showDetail: false,
    });
  }

  changeState= (name, value) => {
    this.setState({
      [name]:value
    })
  }

  //搜索人员出入校信息
  searchRecord = () => {
    const { startTime, endTime, page, pageSize } = this.state;
    //2018-05-16 14:27:55
    this.getStudentRecordList({
      ...this.state,
    });
  }

  //  搜索的方法
  // 将方法暴露出来 
  getStudentRecordList =({monthQuery, visitorName, identityType, page, pageSize}, isFirst = false) => {
    
    this.props.getVisitorRecord({
      monthQuery,
      visitorName,
      identityType,
      page,
      pageSize,
    }, isFirst);
  }

  // 分页
  changePage = (page, pageSize)=> {
    console.log('sdj',page)
    this.setState({
      page: page
    },() => {
      this.getStudentRecordList({...this.state});
    });
  }

  // 调整每页的数量
  onShowSizeChange = (current, pageSize)=> {
    console.log('sdj', current, pageSize);
    this.setState({
      page:1,
      pageSize: pageSize,
    }, () => {
      this.getStudentRecordList({...this.state});
    });
  }

  // 时间戳转为 YYYY-MM-DD HH:mm:ss
  transformTime = (time, type = 'YYYY-MM-DD HH:mm:ss')=> {
    if(time && time !==''){
      return moment(new Date(time)).format(type);
    }else{
      return '- -'
    }
  }

  changeMonth = (date, dateString) => {
    console.log(date, dateString);
    this.setState({
      monthQuery: dateString
    })
  }

  handleChange = val => {
    console.log('sdj',val);
    this.setState({
      identityType: val
    })
  }

  searchName= val => {
    console.log('sdj', val.target.value);
    this.setState({
      visitorName: val.target.value
    })
  }

  //查看访客资料，跳转至访客或亲属档案详情页
  routeDetail = detail => {
    const {identityType, visitorId} = detail;

    console.log('detail',detail)
    if(identityType==0){
      //  访客
      this.setState({
        visitorId:visitorId,
        showDetail: true
      })
    }else if(identityType == 1){
    //学生家长
    }
  }
  render() {
    // console.log('sdj, this.props.location', this.props.location);
    const columns=[
      {title:'日期',dataIndex:'',key:'name',render :(text, record)=>(
        <span>
          {this.transformTime(record.actualVisitTime, 'YYYY-MM-DD')}
        </span>
      )},
      {title:'访问时间',dataIndex:'',key:'num',render :(text, record)=>(
        <span>
          {this.transformTime(record.actualVisitTime, 'HH:MM:SS')}
        </span>
      )},
      {title:'受访人',dataIndex:'intervieweeName',key:'intervieweeName',},
      {title:'访客身份',dataIndex:'',key:'mobile',render :(text, record)=>(
        <span>
          {record.identityType ==0?'其他':record.identityType ==1?'学生家长':'后台字段错误'}
        </span>
      )},
      {title:'访客姓名',dataIndex:'',key:'visitorName',render: (text, record) => (
        <a onClick={this.routeDetail.bind(this, record)}>{record.visitorName?record.visitorName:''}</a>
      )},
    ];
    const {page, pageSize, showDetail,visitorId} = this.state;
    const searchStyle = {
      width: 200,
      marginRight: 20,
      marginBottom: 5
    };
    return (
      <div>
        <BreadcrumbCustom first="用户管理" second={'访问档案' } />
        <div style={{display: !showDetail?'block':'none'}}>
          <nav style={{margin:"10px 0"}}>
            <Input
              placeholder="请输入姓名"
              onChange={this.searchName}
              style={searchStyle}
            />
            <Select
              showSearch
              style={searchStyle}
              placeholder="请选择访客身份"
              optionFilterProp="children"
              onChange={this.handleChange}
            >
              <Option value="1">学生家长</Option>
              <Option value="0">其他</Option>
            </Select>
            {/*<MonthPickerRange*/}
              {/*startTime={startTime}*/}
              {/*endTime={endTime}*/}
              {/*onChange ={this.changeState}*/}
            {/*/>*/}
            <MonthPicker
              placeholder="选择月份"
              onChange={this.changeMonth}
              style={{marginRight: 15}}
            />
            <Button  type="primary" onClick={this.searchRecord}>搜索</Button>
            {/*<Button style={{float: "right", marginRight: 15}}onClick={this.returnRecord} className={"search-button"}>*/}
              {/*返回*/}
            {/*</Button>*/}
          </nav>
          <Card className={"record-container"} bordered={false}>
          <Table
            columns={columns}
            dataSource={this.props.visitorsList.length? this.props.visitorsList:VISITOR_LIST}
            bordered
            pagination={false}
          />
          <Pagination
            className="center footer"
            total={this.props.total}
            defaultCurrent={1}
            showSizeChanger
            showQuickJumper
            current={page}
            pageSize={pageSize}
            defaultPageSize={10}
            pageSizeOptions={['10','20','50']}
            onShowSizeChange={this.onShowSizeChange}
            onChange={this.changePage}
          />
        </Card>
        </div>
        <VisitorDetail
          showDetail={showDetail}
          onChangeProps={this.changeState}
          visitorId={visitorId}
          changeSpining={this.props.changeSpining}
        />
        <Spin
          className="wp spin-center zi1060"
          size="large"
          spinning={this.props.viewListSpinning}
        />
      </div>
    )
  }
}

export default VisitorRecord;