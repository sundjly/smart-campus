
import React from "react";
import { Row, Col, Card, Timeline, Icon ,Layout} from "antd";
import BreadcrumbCustom from "../BreadcrumbCustom";
import EchartsViews from "./EchartsViews";
// import EchartsProjects from "./EchartsProjects";
import b1 from "../../assets/imgs/b1.jpg";
const { Content, Footer } = Layout;
class Dashboard extends React.Component {
  render() {
    return (
      <div className="gutter-example button-demo">
        <BreadcrumbCustom />
        <Row gutter={10}>
          <Col className="gutter-row" md={8}>
            <div className="gutter-box">
              <Card bordered={false}>
                <div className="pb-m">
                  <h3>任务</h3>
                  <small>10个已经完成，2个待完成，1个正在进行中</small>
                </div>
                <a className="card-tool">
                  <Icon type="sync" />
                </a>
                <Timeline>
                  <Timeline.Item color="green">新版本迭代会</Timeline.Item>
                  <Timeline.Item color="green">完成网站设计初版</Timeline.Item>
                  <Timeline.Item color="red">
                    <p>联调接口</p>
                    <p>功能验收</p>
                  </Timeline.Item>

                  <Timeline.Item color="#108ee9">
                    <p>登录功能设计</p>
                    <p>权限验证</p>
                    <p>页面排版</p>
                  </Timeline.Item>
                </Timeline>
              </Card>
            </div>
          </Col>
          <Col className="gutter-row" md={8}>
            <div className="gutter-box">
              <Card bordered={false}>
                <div className="pb-m">
                  <h3>消息栏</h3>
                </div>
                <a className="card-tool">
                  <Icon type="sync" />
                </a>
                <ul className="list-group no-border">
                  <li className="list-group-item">
                    <a href="" className="pull-left w-40 mr-m">
                      <img
                        src={b1}
                        className="img-responsive img-circle"
                        alt="test"
                      />
                    </a>
                    <div className="clear">
                      <a href="" className="block">
                        学生
                      </a>
                      <span className="text-muted">进入学校了！</span>
                    </div>
                  </li>
                  <li className="list-group-item">
                    <a href="" className="pull-left w-40 mr-m">
                      <img
                        src={b1}
                        className="img-responsive img-circle"
                        alt="test"
                      />
                    </a>
                    <div className="clear">
                      <a href="" className="block">
                        学生
                      </a>
                      <span className="text-muted">进入学校了！</span>
                    </div>
                  </li>
                  <li className="list-group-item">
                    <a href="" className="pull-left w-40 mr-m">
                      <img
                        src={b1}
                        className="img-responsive img-circle"
                        alt="test"
                      />
                    </a>
                    <div className="clear">
                      <a href="" className="block">
                        学生
                      </a>
                      <span className="text-muted">进入学校了！</span>
                    </div>
                  </li>
                  <li className="list-group-item">
                    <a href="" className="pull-left w-40 mr-m">
                      <img
                        src={b1}
                        className="img-responsive img-circle"
                        alt="test"
                      />
                    </a>
                    <div className="clear">
                      <a href="" className="block">
                        学生
                      </a>
                      <span className="text-muted">进入学校了！</span>
                    </div>
                  </li>
                </ul>
              </Card>
            </div>
          </Col>
          <Col className="gutter-row" md={8}>
            <div className="gutter-box">
              <Card bordered={false}>
                <div className="pb-m">
                  <h3>访问量统计</h3>
                  <small>最近7天用户访问量</small>
                </div>
                <a className="card-tool">
                  <Icon type="sync" />
                </a>
                <EchartsViews />
              </Card>
            </div>
          </Col>
        </Row>
        <footer className="footer copyright">
          <p>
            Copyright©2014-2018&nbsp;intellif.com All Rights Reserved.云天励飞
            版权所有
          </p>
        </footer>
      </div>
    );
  }
}

export default Dashboard;
