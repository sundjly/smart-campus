/* 可复用的对话框弹窗 */
import React, { Component } from "react";
import { Modal } from "antd";

import "./CommonModal.less";

const propTypes = {};

class CommonModal extends Component {
  onOk = () => {
    console.log("click modal ok button");
    if (this.props.modalOption.okCallback) {
      this.props.modalOption.okCallback();
    }
  };
  onCancel = () => {
    console.log("click modal cancel or close button");
    if (this.props.modalOption.cancelCallback) {
      this.props.modalOption.cancelCallback();
    }
  };
  render() {
    return (
      <div>
        <Modal onOk={this.onOk} onCancel={this.onCancel} {...this.props.modalOption}>
          {this.props.modalOption.content}
        </Modal>
      </div>
    );
  }
}

CommonModal.prototypes = propTypes;
export default CommonModal;
