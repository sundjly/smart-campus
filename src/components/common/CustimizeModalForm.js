import React from "react";
import ReactDOM from 'react-dom';
import { Button, Modal, Form } from "antd";
import intl from "react-intl-universal";

const FormItem = Form.Item;
class ModalFormComponent extends React.Component {
    // componentDidMount() {
    //     this.props.setFieldsValueObj && this.props.form.setFieldsValue(
    //         this.props.setFieldsValueObj
    //     )
    // }

    render() {
        const {
            visible,
            onCancel,
            onOk,
            form,
            confirmLoading,
            formItems,
            discription,
            title,
            operationRequestParams
        } = this.props;
        const { getFieldDecorator } = form;
        const formItemLayout = {
            labelCol: { span: 5 },
            wrapperCol: { span: 14, offset: 2 }
        };
        let submitBtnRef = null;
        const handleSubmit = (e) => {
            e.preventDefault();
            form.validateFieldsAndScroll((err, values) => {
                if (!err) {
                    const properties = Object.keys(operationRequestParams)
                    properties.length > 0 && properties.map((item) => {
                        values[item] = operationRequestParams[item];
                    });
                    onOk(values);
                }
            });
        }
        const handleOk = () => {
            const node = ReactDOM.findDOMNode(submitBtnRef);
            //原生js模拟点击事件的触发（ie和choroem均适用）
            var evt = new MouseEvent('click', {
                bubbles: true,
                view: window
            });
            node.dispatchEvent(evt);
        }
        return (
            <Modal
                className="form-modal"
                visible={visible}
                title={title}
                footer={
                    [
                        <Button
                            type="primary"
                            size="large"
                            onClick={handleOk}
                            className="modalFormBtn"
                            style={{ marginRight: '20px' }}
                            loading={confirmLoading}
                            key={0}
                        >
                            确定
                    </Button>,
                        <Button
                            size="large"
                            className="modalFormBtn"
                            onClick={onCancel}
                            key={1}
                        >
                            取消
                    </Button>
                    ]
                }
                onCancel={onCancel}
            >
                {discription}
                <Form onSubmit={handleSubmit}>
                    {formItems.map((item, index, array) => {
                        return <FormItem label={item.labelName} {...formItemLayout} key={index}>
                            {getFieldDecorator(item.property, item.options)(item.content)}
                        </FormItem>
                    })}
                    <FormItem >
                        <Button
                            type="primary"
                            size="large"
                            htmlType="submit"
                            className="modalFormBtn"
                            loading={confirmLoading}
                            ref={(ele) => { submitBtnRef = ele }}
                            style={{ display: "none" }}
                        >
                            确定
                    </Button>
                    </FormItem>
                </Form>
            </Modal>
        );
    }
}
const CustimizeModalForm = Form.create({
    mapPropsToFields(props) {
        let copyObj = { ...props.setFieldsValueObj };
        for (let key in copyObj) {
            copyObj[key] = Form.createFormField({
                value: copyObj[key],
            })
        }
        return copyObj;
    },
})(ModalFormComponent);
export default CustimizeModalForm;