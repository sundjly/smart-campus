import React, { Component } from "react";
import { Button, Icon } from "antd";
import CustimizeModalForm from './CustimizeModalForm';
import intl from "react-intl-universal";
/*
{
    operationRequest //点击确认按钮请求
    renderRequest,//请求成功后渲染页面请求
    type //按钮类型
    btnName //按钮名称
    formItems  //表单对象
    例如：formItems= [
                        {
                            labelName: "班级名称",
                            property: "name",
                            options: {
                                rules: [{ required: true, message: "名称格式不符合规范!", max: 15 }],
                                initialValue: ""
                            },
                            content: <Input type="text" />
                        }
                    ]
    
    operationRequestParams //额外的操作请求入参
    renderRequestParams //额外的渲染请求入参
    setFieldsValueObj //设定初始值
    title //弹框名称
    discription //弹框内容
}
*/
class CustimizeModal extends Component {
    state = {
        visible: false,
        confirmLoading: false

    };

    showModal = () => {
        this.setState({ visible: true });
    };

    handleCancel = () => {
        this.setState({ visible: false });
    };

    handleOk = (values) => {
        const {
            operationRequest,
            renderRequest,
            renderRequestParams
        } = this.props;
        this.setState({
            confirmLoading: true
        });
        typeof operationRequest === "function" && operationRequest(values).then((code) => {
            if (code === 0) {
                this.setState({
                    visible: false,
                    confirmLoading: false
                });
                setTimeout(() => {
                    typeof renderRequest === "function" && renderRequest(renderRequestParams ? renderRequestParams : null);
                }, 1000);
            } else {
                this.setState({
                    confirmLoading: false
                });
            }
        });
    };

    render() {
        const { type, btnName, formItems, operationRequestParams, title, discription, styleObj, setFieldsValueObj } = this.props;
        return (
            <div
                style={
                    styleObj
                        ?
                        styleObj
                        :
                        {
                            display: "inline-block",
                            marginRight: "10px"
                        }
                }
            >
                <Button
                    onClick={this.showModal}
                >
                    <Icon type={type} />
                    {btnName ? btnName : null}
                </Button>
                <CustimizeModalForm
                    visible={this.state.visible}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                    confirmLoading={this.state.confirmLoading}
                    formItems={formItems}
                    operationRequestParams={operationRequestParams}
                    title={title}
                    discription={discription}
                    setFieldsValueObj={setFieldsValueObj}
                />
            </div>
        );
    }
}

export default CustimizeModal;
