import PropTypes from 'prop-types';
import React, { Component } from 'react';
import moment from 'moment';
import { DatePicker } from 'antd';

const defaultProps = {
  startDate: '',
  endDate: ''
};

const propTypes = {
  startDate: PropTypes.string.isRequired,
  endDate: PropTypes.string.isRequired,
  onStartChange: PropTypes.func.isRequired,
  onEndChange: PropTypes.func.isRequired
};
class DeepEyeDatePicker extends Component {
  state = {
    endOpen: false //控制开始结束日期弹窗关闭联动
  };
  //控制不可选开始日期
  disabledStartDate = startValue => {
    const endValue = moment(this.props.endDate);
    if (!startValue || !endValue) {
      return false;
    }
    return startValue.valueOf() > endValue.valueOf();
  };
  //控制不可选结束日期
  disabledEndDate = endValue => {
    const startValue = moment(this.props.startDate);
    if (!endValue || !startValue) {
      return false;
    }
    return endValue.valueOf() <= startValue.valueOf();
  };
  //控制开始日期弹窗显示
  handleStartOpenChange = open => {
    if (!open) {
      this.setState({ endOpen: true });
    }
  };
  //控制结束日期弹窗显示
  handleEndOpenChange = open => {
    this.setState({ endOpen: open });
  };
  //改变开始日期
  onStartChange = (date, dateString) => {
    if (this.props.onStartChange) {
      this.props.onStartChange(date, dateString);
    }
  };
  //改变结束日期
  onEndChange = (date, dateString) => {
    if (this.props.onEndChange) {
      this.props.onEndChange(date, dateString);
    }
  };
  render() {
    const { startDate, endDate } = this.props;
    return (
      <div style={{ display: 'inline-block', verticalAlign: 'middle' }}>
        <DatePicker
          disabledDate={this.disabledStartDate}
          showTime={{
            hideDisabledOptions: true,
            defaultValue: moment('00:00:00', 'HH:mm:ss')
          }}
          allowClear={false}
          format="YYYY-MM-DD HH:mm:ss"
          value={moment(startDate)}
          placeholder="开始日期"
          onChange={this.onStartChange}
          onOpenChange={this.handleStartOpenChange}
        />
        <DatePicker
          disabledDate={this.disabledEndDate}
          showTime={{
            hideDisabledOptions: true,
            defaultValue: moment('23:59:59', 'HH:mm:ss')
          }}
          allowClear={false}
          format="YYYY-MM-DD HH:mm:ss"
          value={moment(endDate)}
          placeholder="结束日期"
          onChange={this.onEndChange}
          open={this.state.endOpen}
          onOpenChange={this.handleEndOpenChange}
        />
      </div>
    );
  }
}

DeepEyeDatePicker.defaultProps = defaultProps;
DeepEyeDatePicker.propTypes = propTypes;

export default DeepEyeDatePicker;
