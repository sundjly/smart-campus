import React, { Component } from "react";
import BreadcrumbCustom from "../../BreadcrumbCustom";
import {
    Form,
    Select,
    Row,
    Col,
    Button,
    message,
    Spin,
    Input,
    Icon,
    Card
} from "antd";

const FormItem = Form.Item;
const Option = Select.Option;

class NewAccountForm extends Component {
    state = {
        roleId: ""
    };
    closeCreatePage = () => {
        this.props.setActivePage("list");
    };
    handleChange = value => {
        this.setState({ roleId: value });
    };
    handleSubmit(e) {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (err) {
                return;
            }
            if (values.newPsw !== values.repeatPsw) {
                alert("两次密码不一致");
                return;
            }
            var params = {
                login: values.account,
                name: values.name,
                mobile: values.telphone,
                roleIds: values.role,
                password: values.newPsw,
                roleIds: this.state.roleId
            };
            console.log(params);
            if (this.props.activePage == "add") {
                const { addAccount } = this.props;
                addAccount(params, this.props.setActivePage).then(() => {});
            }
            if (this.props.activePage == "revise") {
                params.id = this.props.item.id;
                const { reviseAccount } = this.props;
                reviseAccount(params, this.props.setActivePage);
            }
        });
    }
    render() {
        const { getFieldDecorator } = this.props.form;
        const { roleList } = this.props;
        const roleOptions = roleList.map(role => (
            <Option value={role.id} key={role.id}>
                {role.name}
            </Option>
        ));

        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 4 }
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 18 }
            }
        };
        return (
            <div>
                <Row style={{ maxWidth: "800px", margin: "40px auto" }}>
                    <Card title="基本信息" bordered={false}>
                        <Icon
                            type="close"
                            className="meeting-create-close-btn"
                            onClick={this.closeCreatePage}
                        />{" "}
                        <Form>
                            <FormItem {...formItemLayout} label={"账号名"}>
                                {getFieldDecorator("account", {
                                    rules: [
                                        {
                                            required: true,
                                            message: "账号名不能为空"
                                        }
                                    ]
                                })(
                                    <Input placeholder="1-20位数字、英文及组合" />
                                )}
                            </FormItem>
                            <FormItem
                                {...formItemLayout}
                                label="请输入新的密码"
                            >
                                {getFieldDecorator("newPsw", {
                                    rules: [
                                        {
                                            required: true,
                                            message: "请输入新的密码!"
                                        }
                                    ]
                                })(<Input type="password" />)}
                            </FormItem>
                            <FormItem
                                {...formItemLayout}
                                label="请重复新的密码"
                            >
                                {getFieldDecorator("repeatPsw", {
                                    rules: [
                                        {
                                            required: true,
                                            message: "请输入重复密码!"
                                        }
                                    ]
                                })(<Input type="password" />)}
                            </FormItem>
                            <FormItem {...formItemLayout} label={"姓名"}>
                                {getFieldDecorator("name", {
                                    rules: [
                                        {
                                            required: true,
                                            message: "姓名不能为空"
                                        }
                                    ]
                                })(<Input placeholder="2-10位汉字" />)}
                            </FormItem>
                            <FormItem {...formItemLayout} label={"联系电话"}>
                                {getFieldDecorator("telphone", {
                                    rules: [
                                        {
                                            required: true,
                                            message: "联系电话不能为空"
                                        }
                                    ]
                                })(<Input placeholder="11位手机号码" />)}
                            </FormItem>
                            <FormItem {...formItemLayout} label={"选择角色"}>
                                {getFieldDecorator("role", {
                                    rules: [
                                        {
                                            required: true,
                                            message: "角色不能为空"
                                        }
                                    ]
                                })(
                                    <Select
                                        showSearch
                                        placeholder="请选择角色名称"
                                        onChange={this.handleChange}
                                    >
                                        {roleOptions}
                                    </Select>
                                )}
                            </FormItem>
                        </Form>
                    </Card>
                </Row>
                <div className="meeting-submit-btns">
                    <Button
                        type="primary"
                        htmlType="submit"
                        onClick={this.handleSubmit.bind(this)}
                    >
                        保存
                    </Button>
                    <Button onClick={this.closeCreatePage}>取消</Button>
                </div>
            </div>
        );
    }
}

const NewAccount = Form.create({
    mapPropsToFields(props) {
        return {
            account: Form.createFormField({
                value: props.item ? props.item.login : ""
            }),
            name: Form.createFormField({
                value: props.item ? props.item.name : ""
            }),
            telphone: Form.createFormField({
                value: props.item ? props.item.mobile : ""
            }),
            role: Form.createFormField({
                value: props.item ? props.item.roles : ""
            })
        };
    }
})(NewAccountForm);

export default NewAccount;
