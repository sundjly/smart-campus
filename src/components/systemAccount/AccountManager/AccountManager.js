/**
 * Created by hao.cheng on 2017/4/22.
 */
import React, { Component } from "react";
import {
    Row,
    Col,
    Card,
    Icon,
    Input,
    Select,
    Button,
    Table,
    Modal,
    Pagination
} from "antd";
import BreadcrumbCustom from "../../BreadcrumbCustom";
import NewAccount from "./NewAccount";
import "./AccountManager.less";
import { resolve } from "path";

const confirm = Modal.confirm;
const Search = Input.Search;
const Option = Select.Option;

class AccountManagement extends Component {
    state = {
        activePage: "list",
        loading: true,
        item: {},
        name: "",
        roleId: ""
    };
    componentWillMount() {
        const { getAllUserAccount, getRoleList } = this.props;
        getRoleList().then(() => {});
        getAllUserAccount().then(() => {
            this.setState({ loading: false });
        });
    }

    onPageChange = (page, pageSize) => {
        const { getAllUserAccount } = this.props;
        getAllUserAccount(page, pageSize);
    };

    onShowSizeChange = (current, pageSize) => {
        const { getAllUserAccount } = this.props;
        getAllUserAccount(current, pageSize);
    };

    //查询
    checkList = () => {
        const { name, roleId } = this.state;
        const { getAllUserAccount } = this.props;
        getAllUserAccount("", "", name, roleId);
    };

    handleChange = value => {
        this.setState({ roleId: value });
    };

    inputChange = ev => {
        this.setState({ name: ev.target.value });
    };
    addAccount = () => {};

    setActivePage = activePage => {
        this.setState({
            activePage
        });
    };

    // 修改
    editAccount = item => {
        console.log(item);
        this.setActivePage("revise");
        this.setState({
            item
        });
    };

    //删除
    showConfirm = id => {
        const { deleteAccount } = this.props;
        confirm({
            title: "你确定删除这条记录吗?",
            content: "当你点击确认的时候，就会在1s后删除这条记录",
            okText: "确认",
            cancelText: "取消",
            onOk() {
                console.log(id);
                deleteAccount(id).then(() => {});
            },
            onCancel() {
                console.log("cancel");
            }
        });
    };

    render() {
        const { activePage } = this.state;
        const { UserAccountList, roleList } = this.props;
        const roleOptions = roleList.map(role => (
            <Option value={role.id} key={role.id}>
                {role.name}
            </Option>
        ));
        const searchStyle = {
            width: 200,
            marginRight: 20
        };
        const tableHandle = {
            margin: "0 10px"
        };
        const columns = [
            { title: "账号名", dataIndex: "login", key: "login" },
            { title: "姓名", dataIndex: "name", key: "name" },
            { title: "联系电话", dataIndex: "mobile", key: "mobile" },
            { title: "角色名称", dataIndex: "roles", key: "roles" },
            { title: "上次登录时间", dataIndex: "updated", key: "updated" },
            {
                title: "操作",
                key: "action",
                render: (text, record) => (
                    <span>
                        <Button
                            style={tableHandle}
                            title="编辑"
                            onClick={this.editAccount.bind(this, record)}
                        >
                            <Icon type="edit" />
                        </Button>
                        <Button
                            style={tableHandle}
                            title="删除"
                            onClick={this.showConfirm.bind(
                                this,
                                record.id,
                                "delete"
                            )}
                        >
                            <Icon type="delete" />
                        </Button>
                    </span>
                )
            }
        ];
        return (
            <div className="account-manage-wrap">
                <div
                    className="gutter-example"
                    style={{
                        display: activePage === "list" ? "block" : "none"
                    }}
                >
                    <BreadcrumbCustom first="系统账号" second="账号管理" />
                    <Row gutter={16}>
                        <Col className="gutter-row" md={12}>
                            <div className="gutter-box">
                                <Input
                                    placeholder="请输入名字"
                                    onChange={this.inputChange}
                                    style={searchStyle}
                                />
                                <Select
                                    showSearch
                                    style={searchStyle}
                                    placeholder="请选择角色名称"
                                    optionFilterProp="children"
                                    onChange={this.handleChange}
                                >
                                    {roleOptions}
                                </Select>
                                <Button type="primary" onClick={this.checkList}>
                                    <Icon type="search" />查询
                                </Button>
                            </div>
                        </Col>
                    </Row>
                    <Row gutter={16}>
                        <Col className="gutter-row" md={24}>
                            <div className="gutter-box">
                                <Card bordered={false}>
                                    <Button
                                        type="primary"
                                        onClick={this.setActivePage.bind(
                                            this,
                                            "add"
                                        )}
                                    >
                                        <Icon type="plus" />新增
                                    </Button>
                                    <Table
                                        columns={columns}
                                        dataSource={UserAccountList}
                                        loading={this.state.loading}
                                        pagination={false}
                                    />
                                    <Pagination
                                        showSizeChanger
                                        showQuickJumper={true}
                                        onChange={this.onPageChange}
                                        onShowSizeChange={this.onShowSizeChange}
                                        defaultCurrent={1}
                                        total={this.props.total}
                                    />
                                </Card>
                            </div>
                        </Col>
                    </Row>
                </div>
                <div
                    className="meeting-create-page"
                    style={{
                        display:
                            activePage === "add" || activePage === "revise"
                                ? "block"
                                : "none"
                    }}
                >
                    <BreadcrumbCustom
                        first="系统账号"
                        second="账号管理"
                        third={activePage === "add" ? "新增账号" : "修改账号"}
                    />
                    <NewAccount
                        {...this.props}
                        item={activePage === "add" ? "" : this.state.item}
                        setActivePage={this.setActivePage}
                        activePage={activePage}
                    />
                </div>
            </div>
        );
    }
}

export default AccountManagement;
