/**
 * Created by hao.cheng on 2017/4/15.
 */
import React, { Component } from 'react';
import { Button, Modal, Form, Input, Icon } from 'antd';
const FormItem = Form.Item;

const CollectionCreateForm = Form.create()(
    (props) => {
        const { visible, onCancel, onCreate, form } = props;
        const { getFieldDecorator } = form;
        return (
            <Modal
                visible={visible}
                title="修改密码"
                okText="确定"
                cancelText="取消"
                onCancel={onCancel}
                onOk={onCreate}
            >
                <Form layout="vertical">
                    <FormItem label="请输入新的密码">
                        {getFieldDecorator('title', {
                            rules: [{ required: true, message: '请输入收藏的标题!' }],
                        })(
                            <Input type="password" />
                        )}
                    </FormItem>
                    <FormItem label="请重复新的密码">
                        {getFieldDecorator('description')(
                          <Input type="password" />
                        )}
                    </FormItem>
                    {/*<FormItem className="collection-create-form_last-form-item" style={{marginBottom: 0}}>*/}
                        {/*{getFieldDecorator('modifier', {*/}
                            {/*initialValue: 'public',*/}
                        {/*})(*/}
                            {/*<Radio.Group>*/}
                                {/*<Radio value="public">公开</Radio>*/}
                                {/*<Radio value="private">私有</Radio>*/}
                            {/*</Radio.Group>*/}
                        {/*)}*/}
                    {/*</FormItem>*/}
                </Form>
            </Modal>
        );
    }
);

class ModalForm extends Component {
    state = {
        visible: false,
    };
    showModal = () => {
        this.setState({ visible: true });
    };
    handleCancel = () => {
        this.setState({ visible: false });
    };
    handleCreate = () => {
        const form = this.form;
        form.validateFields((err, values) => {
            if (err) {
                return;
            }

            console.log('Received values of form: ', values);
            form.resetFields();
            this.setState({ visible: false });
        });
    };
    saveFormRef = (form) => {
        this.form = form;
    };
    render() {
        return (
            <div style={{display:'inline-block'}}>
                <Button title="修改密码" type="primary" onClick={this.showModal}><Icon type="unlock" /></Button>
                <CollectionCreateForm
                    ref={this.saveFormRef}
                    visible={this.state.visible}
                    onCancel={this.handleCancel}
                    onCreate={this.handleCreate}
                />
            </div>
        );
    }
}

export default ModalForm;