import React from 'react';
import { Table, Input, Icon, Button, Popconfirm,Row,Pagination,Col,Card,message } from 'antd';
import BreadcrumbCustom from '../BreadcrumbCustom';   

import { api } from '../../constants/API';
import request from '../../utils/request';
//import {Row, Col, Card, Button, Table, Icon, Divider, Modal,message,Pagination,Input,Popconfirm} from 'antd';

//time,name,desc,curNum,  
//created 
//角色修改的时候需要加ID。  

Date.prototype.format = function(format){
  var o = {
      "M+" : this.getMonth()+1, //month
      "d+" : this.getDate(), //day
      "h+" : this.getHours(), //hour
      "m+" : this.getMinutes(), //minute
      "s+" : this.getSeconds(), //second
      "q+" : Math.floor((this.getMonth()+3)/3), //quarter
      "S" : this.getMilliseconds() //millisecond
  }
  if(/(y+)/.test(format)) {
      format = format.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));
  }
  for(var k in o) {
      if(new RegExp("("+ k +")").test(format)) {
          format = format.replace(RegExp.$1, RegExp.$1.length==1 ? o[k] : ("00"+ o[k]).substr((""+ o[k]).length));
      }
  }
  return format;   
} 

class RoleManagement extends React.Component {

  constructor(props) {
    super(props);
    //表格列定义
    this.columns = [{
      title: '创建时间',
      dataIndex: 'created',
      render:(text)=>(
        new Date(text).format("yyyy-MM-dd hh:mm:ss")
      ),
    }, {
      title: '角色名称',
      dataIndex: 'name',
    }, {
      title: '角色描述',
      dataIndex: 'description',
    }, 
    {
      title: '当前人数',
      dataIndex: 'rCount',
    }, 
    { 
      title: '操作',
      dataIndex: 'operation',
      render: (text, record) => {

        const tableHandle ={
          margin:'0 10px'
        }

        return (
          this.state.dataSource.length > 1 ?
          (   
            <Popconfirm title="确认删除该记录吗?" onConfirm={() => this.onDelete(record.id)}>

              <Button style={tableHandle} title="编辑" onClick={this.addRole.bind(this, record.id,record)}><Icon type="edit" style={{ fontSize: 16, color: '#08c' }}/></Button>
              <Button style={tableHandle} title="删除" ><Icon type="delete" style={{ fontSize: 16, color: '#08c' }}/></Button>
              
            </Popconfirm>
          ) : null
        );

      },
    }];

    this.state = {
      dataSource: [],
      count: 2,
      page:1,
      pageSize:10,
      currentPage:1,
      //初始值未0
      itemtotal:0,
    };

  }

  addAccount = () => {
    
  }

  //里面只有删除的功能。
  showConfirm = (obj,key,method)=>{

      console.log('obj,key,method',obj,key,method);

  }

  //添加。
  addRole = (key,data) => {
    // console.log('key,data',key,data);
    this.props.history.push(
      {  
        pathname: "/app/systemAccount/newRoleManagement",
        state: key?key:'',
        data:data,
      } 
    );
  }

  onCellChange = (key, dataIndex) => {
    return (value) => {
      const dataSource = [...this.state.dataSource];
      const target = dataSource.find(item => item.key === key);
      if (target) {
        target[dataIndex] = value;
        this.setState({ dataSource });
      }
    };
  }

  onDelete = (key) => {
    // 还需要向后台发送数据。
    request({
      url:api.roleManage+'/'+key,
      method:'delete',
      data:{}
    }).then(res => {

      console.log("wl getRoleList",res);
      
      if(res.errCode===0)
      {
        this.getRoleList();
      }else if(res.errCode===1001){
        message.error('该角色有绑定账号');
      }
      //根据返回的信息来进行判断。
    })

  }
  handleAdd = () => {
    const { count, dataSource } = this.state;
    // const newData = {
    //   key: count,
    //   name: `Edward King ${count}`,
    //   age: 32,
    //   address: `London, Park Lane no. ${count}`,
    // };
    this.setState({
      dataSource: [...dataSource],
      count: count + 1,
    });
  }

  //组件加载的时候
  componentDidMount(){

    //角色列表
    this.getRoleList();

  }

  getRoleList = (page) => {

    // console.log('page',page);

    request({
      url:api.roleList,
      method:'post',
      data:{page:page?page:this.state.page,pageSize:this.state.pageSize}
    }).then(res => {
      // console.log("wl getRoleList",res);
      // console.log('max page',res.maxPage);
      let tempData = res.data;  
      let list=[];
      let key = 0;
      // console.log('tempData',tempData);
      if(res.errCode===0&&res.data&&res.data.length>0){
        this.setState({dataSource:res.data,total:res.total});
        this.setState({itemtotal:res.total});
      }
    })
}
  
  onShowSizeChange = (current, pageSize)=> {
    console.log('sdj', current, pageSize);
    this.setState({
      page:1,
      pageSize: pageSize,
      //spining: true,
    })
  }

  changePage = (page, pageSize)=> {
    // console.log('sdj',page)
    this.setState({
      page: page,
      currentPage:page
    });
    //发请求
    this.getRoleList(page);
    
  }

  render() {
    const { dataSource } = this.state;
    const columns = this.columns;
    return (

      <div className="gutter-example button-demo">
        <BreadcrumbCustom first="系统账号" second="角色管理" />
        <Row gutter={16}>
          <Col className="gutter-row" md={24}>
            <div className="gutter-box">
                <Card bordered={false}>
                  <Button className="editable-add-btn" onClick={this.addRole.bind(this,false,false)}><Icon type="plus"/>新增</Button>
                  <Table bordered dataSource={this.state.dataSource} columns={columns} pagination={false}/>
                </Card>
            </div>
          </Col>
        </Row>
        <Row type="flex" justify="end" style={{margin: '12px 0'}}>
              <Pagination
                        total={this.state.itemtotal}
                        defaultCurrent={1}
                        showSizeChanger
                        showQuickJumper 
                        current={this.state.currentPage}
                        pageSize={this.state.pageSize}
                        defaultPageSize={10}
                        pageSizeOptions={['10','20','50']}
                        onShowSizeChange={this.onShowSizeChange}
                        onChange={this.changePage}/>
        </Row>

        <style>
          {`
          .button-demo .ant-btn {
              margin-right: 8px;
              margin-bottom: 12px;
          }
          `}
          </style>
      </div>
    );
  }
}

// ReactDOM.render(<RoleManagement />, mountNode);
export default RoleManagement;

