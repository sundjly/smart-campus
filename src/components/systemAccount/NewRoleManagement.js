import React, { Component } from 'react';
import BreadcrumbCustom from '../BreadcrumbCustom';
import { Row, Col, Form, Input, Button, Checkbox, Icon, message, Radio } from 'antd';

import './NewRoleManagement.less';
import request from '../../utils/request';
import { api } from '../../constants/API';

const RadioGroup = Radio.Group;

const FormItem = Form.Item;
const CheckboxGroup = Checkbox.Group;
// 系统账号
const plainOptions1 = [
  { label: '只读', value: '10000' },
  { label: '修改', value: '10001' },
];
//系统设置
const plainOptions2 = [
  { label: '只读', value: '10100' },
  { label: '修改', value: '10101' },
];
//用户管理
const plainOptions3 = [
  { label: '只读', value: '20000' },
  { label: '修改', value: '20001' },
];
//访客管理
const plainOptions4 = [
  { label: '只读', value: '20100' },
  { label: '修改', value: '20101' },
];
//基础信息管理
const plainOptions5 = [
  { label: '只读', value: '30000' },
  { label: '修改', value: '30001' },
];
//业务管理
const plainOptions6 = [
  { label: '只读', value: '30100' },
  { label: '修改', value: '30101' },
];

const success = (txt) => {
  message.success(txt, 2);
};

const error = (txt) => {
  message.error(txt, 2);
};

const defaultCheckedList = [];

class RoleManagementNew extends Component {

  state = {
    checkedList: defaultCheckedList,
    indeterminate: true,
    checkAll: false,
    info: [],
    loading: false,
    roleName: "",
    description: "",
    resIds: [],
    account: [],
    setting: [],
    user: [],
    visitor: [],
    base: [],
    business: [],
    selectedID: null,
    value1: '10000',
    value2: '10100',
    value3: '20000',
    value4: '20100',
    value5: '30000',
    value6: '30100',
  };

  componentWillMount() {
    //初始化单选框中的值，不需要。
    //选中的元素的值。
    const key = this.props.location.state;
    let that = this;
    this.setState({ selectedID: key });

    console.log('this.props', this.props);
    console.log('sdj', key);

    if (key) {
      console.log(this.props.location.data)
      // this.getRoleInfo(key);
      const data = this.props.location.data;

      //获取当前角色的详情信息
      if (data) {
        const resIds = data.resIds.split(',');
        let account = [];
        let setting = [];
        let user = [];
        let visitor = [];
        let base = [];
        let business = [];
        resIds.map(function (value, index) {
          switch (value) {
            case '10000':
              that.state.value1 = '10000';
              break;
            case '10001':
              that.state.value1 = '10001';
              account.push(value)
              break;
            case '10100':
              that.state.value2 = '10100';
              break;
            case '10101':
              that.state.value2 = '10101';
              setting.push(value)
              break;
            case '20000':
              that.state.value3 = '20000';
              break;
            case '20001':
              that.state.value3 = '20001';
              user.push(value)
              break;
            case '20100':
              that.state.value4 = '20100';
              break;
            case '20101':
              that.state.value4 = '20101';
              visitor.push(value)
              break;
            case '30000':
              that.state.value5 = '30000';
              break;
            case '30001':
              that.state.value5 = '30001';
              base.push(value)
              break;
            case '30100':
              that.state.value6 = '30100';
              break;
            case '30101':
              that.state.value6 = '30101';
              business.push(value)
              break;
            default:
              break;
          }
        })
        this.setState({
          resIds: resIds,
          roleName: data.name,
          description: data.description,
          account: account,
          setting: setting,
          user: user,
          visitor: visitor,
          base: base,
          business: business,
        })
      } else {
        this.backToList()
      }
    }

  }

  //全选function 
  onCheckAllChange = (e) => {

    this.setState({
      //checkedList: e.target.checked ? plainOptions : [],
      indeterminate: false,
      checkAll: e.target.checked,
      value1: '10001',
      value2: '10101',
      value3: '20001',
      value4: '20101',
      value5: '30001',
      value6: '30101',
    });

  }
  backToList = () => {
    this.props.history.push('/app/systemAccount/RoleManagement');
  }

  handleSubmit = () => {

    this.setState({ loading: true });

    const data = this.props.form.getFieldsValue();
    console.log('handleSubmit', data);

    const resIds = this.state.checkAll ? this.state.account.concat('10000', '10100', '20000', '20100', '30000', '30100').join(',') : this.state.account.concat(this.state.value1, this.state.value2, this.state.value3, this.state.value4, this.state.value5, this.state.value6).join(',')
    console.log('resIds', resIds);
    //新增时的请求的函数。判断key值。
    request({
      url: api.roleManage,//+(this.props.location.state?"/"+this.props.location.state:""),
      method: this.props.location.state?"PUT":"POST",
      data: this.state.selectedID ? { name: data.username, description: data.description, resIds: resIds, id: this.state.selectedID } : { name: data.username, description: data.description, resIds: resIds }
    }).then(res => {
      console.log(res);
      if (res.errCode === 0) {
        this.setState({ loading: false });
        success(this.props.location.state ? "修改成功" : "添加成功")
        setTimeout(() => {

          this.backToList()
        }, 1000)
      } else {
        error(res.data)
      }
    })
  }

  //全选回调
  onChange = (e) => {
    console.log(`checked = ${e.target.checked}`);

    this.setState({
      checkAll: e.target.checked,
      value1: '10001',
      value2: '10101',
      value3: '20001',
      value4: '20101',
      value5: '30001',
      value6: '30101',
    });

  }

  onChange1 = (e) => {
    console.log('radio1 checked', e.target.value);
    this.setState({
      value1: e.target.value,
    });
  }
  onChange2 = (e) => {
    console.log('radio2 checked', e.target.value);
    this.setState({
      value2: e.target.value,
    });
  }
  onChange3 = (e) => {
    console.log('radio3 checked', e.target.value);
    this.setState({
      value3: e.target.value,
    });
  }
  onChange4 = (e) => {
    console.log('radio4 checked', e.target.value);
    this.setState({
      value4: e.target.value,
    });
  }
  onChange5 = (e) => {
    console.log('radio5 checked', e.target.value);
    this.setState({
      value5: e.target.value,
    });
  }
  onChange6 = (e) => {
    console.log('radio6 checked', e.target.value);
    this.setState({
      value6: e.target.value,
    });
  }

  render() {
    const key = this.props.location.state
    console.log(this.props)
    const { getFieldDecorator } = this.props.form;
    this.props.form.getFieldsValue()
    const formItemLayout = {
      labelCol: { span: 6 },
      wrapperCol: { span: 14 },
    };

    return (
      <div>
        <BreadcrumbCustom first="系统账号" second={`角色管理 / ${key ? '编辑' : '新建'}`} />

        <Form>
          <Row gutter={16}>
            <Col md={12}>
              <FormItem {...formItemLayout} label="角色名称">
                {getFieldDecorator('username', {
                  rules: [{
                    required: true,
                    message: 'Please input role name',
                  }],
                  initialValue: this.state.roleName
                })(
                  <Input placeholder="1到8位汉字" />
                )}
              </FormItem>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col md={12}>
              <FormItem {...formItemLayout} label="角色描述">
                {getFieldDecorator('description', {
                  rules: [{
                    required: true,
                    message: 'Please input role description',
                  }],
                  initialValue: this.state.description
                })(
                  <Input.TextArea rows={4} placeholder="50个字以内" />
                )}
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col md={12}>
              <FormItem {...formItemLayout} label="操作权限">
                {getFieldDecorator('permission', {
                  rules: [{
                    required: false,
                    message: 'Please input your name',
                  }],
                })(
                  <table cellSpacing="0" className="new-role-table">
                    <tbody>
                      <tr>
                        <td width="150px">
                          {/* <Checkbox
                        indeterminate={this.state.indeterminate}
                        onChange={this.onCheckAllChange}
                        checked={this.state.checkAll}
                        >
                          全选
                        </Checkbox> */}

                          <Checkbox onChange={this.onChange}>全选</Checkbox>

                        </td>
                        <td width="250px"></td>
                        <td></td>
                      </tr>
                      <tr>
                        <td rowSpan="2">系统设置</td>
                        <td>
                          系统账号
                      </td>
                        <td>
                          {/* <CheckboxGroup
                          onChange={this.onAccountChange}
                          options={plainOptions1}
                          defaultValue={this.state.account}
                        /> */}
                          <RadioGroup options={plainOptions1} onChange={this.onChange1} defaultValue={plainOptions1[0].value} value={this.state.value1} />
                        </td>
                      </tr>
                      <tr>
                        <td>
                          系统设置
                      </td>
                        <td>
                          {/* <CheckboxGroup
                          onChange={this.onSettingChange}
                          options={plainOptions2}
                          defaultValue={this.state.setting}
                        /> */}
                          <RadioGroup options={plainOptions2} onChange={this.onChange2} defaultValue={plainOptions2[0].value} value={this.state.value2} />
                        </td>
                      </tr>
                      <tr>
                        <td rowSpan="2">用户管理</td>
                        <td>
                          用户管理
                      </td>
                        <td>
                          {/* <CheckboxGroup
                          onChange={this.onUserChange}
                          options={plainOptions3}
                          defaultValue={this.state.user}
                        /> */}
                          <RadioGroup options={plainOptions3} onChange={this.onChange3} defaultValue={plainOptions3[0].value} value={this.state.value3} />
                        </td>
                      </tr>
                      <tr>
                        <td>
                          访客管理
                      </td>
                        <td>
                          {/* <CheckboxGroup
                          onChange={this.onVisitorChange}
                          options={plainOptions4}
                          defaultValue={this.state.visitor} */}
                          <RadioGroup options={plainOptions4} onChange={this.onChange4} defaultValue={plainOptions4[0].value} value={this.state.value4} />

                        </td>
                      </tr>
                      <tr>
                        <td rowSpan="2">基础信息管理</td>
                        <td>
                          基础信息管理
                      </td>
                        <td>
                          {/* <CheckboxGroup
                          onChange={this.onBaseChange}
                          options={plainOptions5}
                          defaultValue={this.state.base}
                        /> */}
                          <RadioGroup options={plainOptions5} onChange={this.onChange5} defaultValue={plainOptions5[0].value} value={this.state.value5} />
                        </td>
                      </tr>
                      <tr>
                        <td>
                          业务管理
                      </td>
                        <td>
                          {/* <CheckboxGroup
                          onChange={this.onBusinessChange}
                          options={plainOptions6}
                          defaultValue={this.state.business}
                        /> */}
                          {/* 设定一个默认值 */}
                          <RadioGroup options={plainOptions6} onChange={this.onChange6} defaultValue={plainOptions6[0].value} value={this.state.value6} />

                        </td>
                      </tr>
                    </tbody>
                  </table>
                )}
              </FormItem>
            </Col>
          </Row>

          <Row>



          </Row>

          <Row>
            <Col md={12} style={{ textAlign: 'center' }}>
              <Button loading={this.state.loading} onClick={this.handleSubmit} type="primary"><Icon type="check" />保存</Button>
              <Button onClick={this.backToList} ><Icon type="close" />取消</Button>
            </Col>
          </Row>

        </Form>
      </div>
    )

  }
}

export default Form.create()(RoleManagementNew);

