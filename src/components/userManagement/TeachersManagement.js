import React, {Component} from 'react';
import {Row, Col, Card, Icon, Input, Select, Button, Table, Modal, Cascader, Pagination, Spin} from 'antd';
import BreadcrumbCustom from '../BreadcrumbCustom';
import ModalForm from '../systemAccount/ModalForm';
import UpLoadModal from './components/UpLoadModal';
import intl from 'react-intl-universal';
import {deleteStudentInfo, exportExcel} from '@/actions/userManagement/TeachersManagementAction';
import './RelativesMangement.less';

const confirm = Modal.confirm;
const Search = Input.Search;
const Option = Select.Option;
const ExampleFace = 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png';

const Relativedata = [
  {
    id: 1,
    name: 'name',
    className: '三年级8班',
    faceImg: ExampleFace,
    //  接口字段
    number: '11232',
    phone: '13660721158',
    pStatus: '1',//学生(1、在籍 2、不在籍)
    gender: '男',
  },
  {
    id: 2,
    name: 'name',
    className: '三年级2班',
    faceImg: ExampleFace,
    number: '11232',
    phone: '13660721158',
    pStatus: '2',//学生(1、在籍 2、不在籍)
    gender: '女'
  },
];


class TeachersManagement extends Component {
  state = {
    selectedRowKeys: [], // Check here to configure the default column
    page: 1,
    pageSize: 10,
    total: 20,
    spinning: false,
    searchName: '',//姓名或者学号
    classRoom: [], //【年级，班级】
    registered: '',// 在籍状态 all：全部  registered : 在籍   noRegistered：不在籍
    pType: -1,//初始值  默认是教师和职工
    status: ''
  };

  componentWillMount() {
    this.getTaskList({...this.state}, true)
  }

  //改变信息的模板
  changeState = (name, value) => {
    this.setState({
      [name]: value
    })
  }

  searchName = (e) => {
    this.changeState('searchName', e.target.value)
  }

  addAccount = (record) => {
    this.props.history.push(
      {
        pathname: "/app/userManagement/newTeacherManagement",
        state: record
      }
    )
  }

  onTypeChange = val => {
    this.changeState('pType', val)
  }

  handleJobChange = val => {
    this.changeState('status', val)
  }
  //  导入
  upLoad = () => {
    this.props.changeModalUpload({
      multiRegisterVisible: true,
      multiRegisterLoading: false,
    })
  }

  //  导出
  downLoad = () => {
    const {searchName, pType, status, page, pageSize} = this.state;
    exportExcel(
      {
        queryStr: searchName,
        page,
        pageSize,
        pType,
        status,
      }
    )
  }

  onSelectChange = (selectedRowKeys) => {
    console.log('selectedRowKeys changed: ', selectedRowKeys);
    this.setState({selectedRowKeys});
  }

  //删除
  showConfirm = (key) => {
    confirm({
      title: '你确定删除这条记录吗?',
      content: '当你点击确认的时候,就会在1s后删除所有相关的记录',
      okText: '确认',
      cancelText: '取消',
      onOk: () => {
        return new Promise((resolve, reject) => {
          console.log('sdj deleteStudentInfo')
          setTimeout(resolve(deleteStudentInfo(key, this, this.state)), 1000);
        }).catch(() => console.log('Oops errors!'));
      },
      onCancel() {
      },
    });
  }

  //  操作记录
  recordHandle = (record, type = 1) => {
    console.log('sdj record', record)
    this.props.history.push({
      pathname: `/app/userManagement/studentsManagementRecord`,
      state: {...record, type}
    });
  }

  // 搜索查询
  searchButton = () => {
    const {searchName, classRoom, registered, page, pageSize} = this.state;
    console.log('sdj', searchName, classRoom, registered, page, pageSize);
    this.getTaskList({
      ...this.state,
      page: 1
    })
    // 重置搜索显示的page
    this.setState({
      page: 1
    })
  }

  //获取任务列表 : 0,
  getTaskList = ({searchName, status, pType, page, pageSize}, isFirst = false) => {
    this.props.getRelativesList({
      queryStr: searchName,
      status,
      page,
      pageSize,
      pType: pType
    }, isFirst)
  }

  // 分页
  changePage = (page, pageSize) => {
    console.log('sdj', page)
    this.setState({
      page: page
    }, () => {
      this.getTaskList({...this.state})
    });
    //  发请求
  }

  // 调整每页的数量
  onShowSizeChange = (current, pageSize) => {
    console.log('sdj', current, pageSize);
    this.setState({
      page: 1,
      pageSize: pageSize,
      spining: true,
    }, () => {
      this.getTaskList({...this.state})
    });
  }

  render() {
    const {selectedRowKeys, page, pageSize} = this.state;
    const searchStyle = {
      width: 200,
      marginRight: 20,
      marginBottom: 5
    };

    const tableHandle = {
      margin: '0 10px'
    };

    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
      hideDefaultSelections: true,
      selections: [{
        key: 'all-data',
        text: '选择全部',
        onSelect: () => {
          this.setState({
            selectedRowKeys: [...Array(46).keys()], // 0...45
          });
        },
      }, {
        key: 'odd',
        text: '选择奇数列',
        onSelect: (changableRowKeys) => {
          let newSelectedRowKeys = [];
          newSelectedRowKeys = changableRowKeys.filter((key, index) => {
            if (index % 2 !== 0) {
              return false;
            }
            return true;
          });
          this.setState({selectedRowKeys: newSelectedRowKeys});
        },
      }, {
        key: 'even',
        text: '选择偶数列',
        onSelect: (changableRowKeys) => {
          let newSelectedRowKeys = [];
          newSelectedRowKeys = changableRowKeys.filter((key, index) => {
            if (index % 2 !== 0) {
              return true;
            }
            return false;
          });
          this.setState({selectedRowKeys: newSelectedRowKeys});
        },
      }],
      onSelection: this.onSelection,
    };
    const tableData = this.props.relativesList && this.props.relativesList.length ? this.props.relativesList : Relativedata;
    const columns = [
      {
        title: '姓名', className: '', dataIndex: '', key: 'name', render: (text, record) => (
        <span className="sudents-col-name">{record.name}</span>
      )
      },
      {title: '工号', className: '', dataIndex: 'number', key: 'num'},
      {
        title: '类别', className: '', dataIndex: '', key: 'className', render: (text, record) => (
        <span>{record.ptype && record.ptype ==2 ? '教师': record.ptype ==3? '职工':'未知'}</span>
      )
      },
      {title: '联系电话', className: '', dataIndex: 'phone', key: 'mobile'},
      {
        title: '人像照片', className: '', dataIndex: '', key: 'face', render: (text, record) => (
        <span>
          {
            record && record.photoData ? <img style={{width: 80, height: 80}} src={record.photoData} alt="人脸照片"/> : ''
          }
        </span>
      )
      },
      {
        title: '在职状态', className: '', dataIndex: '', key: 'roleIds', render: (text, record) =>
        (
          <span>
          {
            record.status == '1' ? '在职' : record.status == '2' ? '离职' : '未知'
          }
        </span>
        )
      },
      {
        title: '操作', className: '', dataIndex: '', key: 'x', render: (text, record) => (
        <span>
          <Button style={tableHandle} title="编辑" onClick={this.addAccount.bind(this, record.detailId)}>
            <Icon type="edit"/>
          </Button>
          <Button style={tableHandle} title="删除" onClick={this.showConfirm.bind(this, record.detailId)}>
            <Icon type="delete"/>
          </Button>
        </span>
      )
      },
    ];
    const {multiRegisterVisible, progressError, multiRegisterLoading, progressErrorInfo, cancelUpLoadModal, multiRegisterProgress, multiRegisterProgressVisible, errorInfo, changeModalUpload, submitTemplate_dispatch, operateModal_dispatch} = this.props;
    const UpLoadModalProps = {
      multiRegisterVisible,
      multiRegisterLoading,
      errorInfo,// 错误提示信息
      changeModalUpload,
      submitTemplate_dispatch,
      operateModal_dispatch,
      multiRegisterProgress,
      multiRegisterProgressVisible,
      cancelUpLoadModal,
      progressError,
      progressErrorInfo,
    };
    const clientHeight = document.body.clientHeight;
    const tableHeight = (clientHeight - 370);
    console.log('sdj clientHeight', tableHeight)
    return (
      <div className="gutter-example">
        <BreadcrumbCustom first="用户管理" second="教职工档案"/>
        <Row gutter={16}>
          <Col className="gutter-row" md={24}>
            <div className="gutter-box">
              <Input
                placeholder="请输入姓名"
                onChange={this.searchName}
                style={searchStyle}
              />
              <Select
                showSearch
                style={searchStyle}
                // options={classRoomOptions}
                onChange={this.onTypeChange}
                placeholder="请选择类别"
                // displayRender={this.displayRender}
              >
                <Option value="-1">全部</Option>
                <Option value="2">教师</Option>
                <Option value="3">职工</Option>
              </Select>

              <Select
                showSearch
                style={searchStyle}
                placeholder="请选择在职状态"
                onChange={this.handleJobChange}
              >
                <Option value="">全部</Option>
                <Option value="1">在职</Option>
                <Option value="2">离职</Option>

              </Select>

              <Button onClick={this.searchButton} type="primary"><Icon type="search"/>查询</Button>
              <Button onClick={this.upLoad}><Icon type="upload"/>导入</Button>
              <Button onClick={this.downLoad}><Icon type="download"/>导出</Button>
              {/*<Button><Icon type="download" />下载模板</Button>*/}
              <UpLoadModal {...UpLoadModalProps} />
            </div>
          </Col>
        </Row>

        <Row gutter={16}>
          <Col className="gutter-row" md={24}>
            <div className="gutter-box">
              <Card bordered={false}>
                <Button
                  onClick={this.addAccount.bind(this, {})}
                  style={{marginBottom: 15}}
                >
                  <Icon type="plus"/>新增
                </Button>

                {
                  !this.props.studentsList
                    // && this.props.studentsList.length
                    ?
                    (
                      <div className="student-management-content">
                        <Table
                          className="students-tab "
                          rowSelection={rowSelection}
                          columns={columns}
                          dataSource={tableData}
                          pagination={false}
                          scroll={{y: tableHeight}}
                        />
                        <Pagination
                          className="footer center "
                          total={this.props.total}
                          defaultCurrent={1}
                          showSizeChanger
                          showQuickJumper
                          current={page}
                          pageSize={pageSize}
                          defaultPageSize={10}
                          pageSizeOptions={['10', '20', '50']}
                          onShowSizeChange={this.onShowSizeChange}
                          onChange={this.changePage}
                        />
                      </div>
                    ) : ''
                }

              </Card>
            </div>
          </Col>
        </Row>

        <Spin
          className="wp spin-center zi1060"
          size="large"
          spinning={this.props.viewListSpinning}
        />
      </div>
    )
  }
}


export default TeachersManagement;