import React, { Component} from 'react';
import PropTypes from 'prop-types';
import {Modal, Form, Input, message, Checkbox, Button, Select, AutoComplete,Cascader, Upload, Icon,Radio, DatePicker } from 'antd';
import BreadcrumbCustom from '../../BreadcrumbCustom';
import './NewStudentsManagement.less';
import UploadImg from '../components/UploadImg';
import moment from 'moment';
import {addStudentFormSubmit, editStudentsInfo, getPhoneDetail} from '@/actions/userManagement/NewStudentsManagementAction'
const FormItem = Form.Item;
const Option = Select.Option;
const AutoCompleteOption = AutoComplete.Option;
const RadioGroup = Radio.Group;
const classRoomOptions = [
  {
    value: 'zhejiang',
    label: 'Zhejiang',
    children: [{
      value: 'hangzhou',
      label: 'Hangzhou',
      children: [{
        value: 'xihu',
        label: 'West Lake',
      }],
    }],
  },
  {
    value: 'jiangsu',
    label: 'Jiangsu',
    children: [{
      value: 'nanjing',
      label: 'Nanjing',
      children: [{
        value: 'zhonghuamen',
        label: 'Zhong Hua Men',
      }],
    }],
  }
];

class NewStudentsManagement extends Component {
  state= {
    confirmDirty: false,
    autoCompleteResult: [],
    editData:null, //通过路由获取的需要改变的值
    birthDate:'',
    fileList:[],// 上传人脸
    noRegistered: false, // 判断是否在籍
  };

  componentWillMount() {
    console.log('sdj NewStudentsManagement', this.props.location.state);
    const editData = this.props.location.state;
    if(editData && editData.hasOwnProperty('name')){
      this.setState({
        editData: {...editData}
      })
    }

  }

  componentDidMount() {
    //  如果为编辑模式
    if(this.state.editData && this.state.editData.hasOwnProperty('name')){
      const {name, number, phone, status, className, board, detailId, gender, sex, birthday, classId, gradeId, photoData} = this.state.editData
      console.log('sdj editData', this.state.editData);
      // 请求获取phone的值
      getPhoneDetail(detailId, this.props.form.setFieldsValue);
      this.props.form.setFieldsValue(
        {
          name: name,
          number: number,
          birthday: birthday?moment(birthday,'YYYY-MM-DD'): moment(),
          registered: status=='1'?'registered':'noRegistered',
          sex: sex==1?'male': sex==2?'female':'unknown',
          classGrade:[gradeId?gradeId.toString():'',classId?classId.toString():''],
          board: board
        }
      );

      this.setState({
        fileList:[
          {
            uid: -1,
            name: 'a.png',
            status: 'done',
            url: photoData,
          },
        ],
        noRegistered: status=='1'? false: true
      },()=>{
        this.props.form.setFieldsValue(
          {
            imageIds: this.state.fileList
          })
      })
    }
  }


  getBirth = (date, dateString)=>{
    console.log(date, dateString);

  }

  normFile = (e) => {
    console.log('Upload event:', e);
    if (Array.isArray(e)) {
      return e;
    }
    return e && e.fileList;
  }

  // 班级修改
  onClassRoomChange = (value)=> {
    console.log('sdj onClassRoomChange', value)
  }

  cancle = () => {
    this.props.history.push('/app/userManagement/studentsManagement');
  }

  handleSubmit = (e) => {
    e.preventDefault();
    const {form, location} = this.props;
    form.setFieldsValue({ imageIds: this.state.fileList });
    form.validateFieldsAndScroll((err, values) => {

      if (!err) {
        console.log('Received values of form: ', values);
        const birthday = values.birthday.format('YYYY-MM-DD');
        console.log('birthday', birthday);
        //  发起请求进行数据处理
        if(location.state.hasOwnProperty('name')){
          editStudentsInfo({
            ...values,
            id: location.state.detailId,
            realName: values.name,
            birthday,
            sex: values.sex ==='male'? '男': '女',
            pStatus: values.registered === 'registered'? 1:2,
            zhxyId: values.registered === 'registered'?values.classGrade[values.classGrade.length -1]: 0,
            pType: 1,
            bankType: 1,
            board: values.registered === 'registered'?values.board: 0,//住校
            pId: location.state.detailId
          }, this.props.history);
        }else{
          addStudentFormSubmit({
            ...values,
            realName: values.name,
            birthday,
            sex: values.sex ==='male'? '男': '女',
            pStatus: values.registered === 'registered'? 1:2,
            zhxyId: values.registered === 'registered'? values.classGrade[values.classGrade.length -1]: 0,
            bankType: 1,
            pType: 1,
            board: values.registered === 'registered'?values.board: 0,//住校
          }, this.props.history);
        }
      }
    });
  }
  changeRegist = e => {
    console.log('sdj changeRegist', e.target.value);
    if(e.target.value === 'noRegistered'){
      this.setState({
        noRegistered: true
      })
    }else{
      this.setState({
        noRegistered: false
      })
    }
  }
  // uploadFace
  handleUpload = info => {
    if (!(info && info.file)) return;
    console.log('uploadStatus', info);
    switch (info.file.status) {
      case 'error':
        this.setState({
          fileList: []
        });
        break;
      case 'done':
        // info.file.response字段存放上传图片之后的接口返回信息
        if (info.file.response) {
          const res = info.file.response;
          let filelists = this.state.fileList;
          if (res && res.errCode == 0) {
            if (res.data.faces == 1) {
              // 正确上传
              this.setState({
                fileList: info.fileList
              });
            } else if (res.data.faces == -1) {
              message.error('您上传的图片可能是系统红名单人员，被限制上传操作！');
              // 匹配到红名单人员
              this.setState({
                fileList: filelists.splice(0, [filelists.length - 1]),
                // tipModalVisible: true,
                // tipModalMsg: '您上传的图片可能是系统红名单人员，被限制上传操作！'
              });
            } else {
              // 上传图片不合规
              message.error('上传图片没有人脸或人脸多于一张，请重新上传');

              this.setState({
                fileList: filelists.splice(0, [filelists.length - 1]),
                // tipModalVisible: true,
                // tipModalMsg: '上传图片没有人脸或人脸多于一张，请重新上传'
              });
            }
          } else if (res.errCode == 1001) {
            // 接口报错异常处理
            message.error('数据异常')
            this.setState({
              fileList: [],
              // tipModalVisible: true,
              // tipModalMsg: '数据异常'
            });
          }
        }
        break;
      case 'uploading':
      case 'removed':
      default:
        this.setState({
          fileList: info.fileList
        });
    }
    this.props.form.setFieldsValue({ imageIds: info.fileList });
  };

  render() {

    const { getFieldDecorator } = this.props.form;
    const { autoCompleteResult } = this.state;

    const formItemLayout = {
      labelCol: {
        xs: { span: 12 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0,
        },
        sm: {
          span: 16,
          offset: 8,
        },
      },
    };
    const headerStyle={
      fontSize: 25,
      borderBottom:'1px solid #ccc',
      marginBottom: 15
    };
    // 班级信息
    const gradeData = ['Zhejiang', 'Jiangsu'];
    const classRoomData = {
      Zhejiang: ['Hangzhou', 'Ningbo', 'Wenzhou'],
      Jiangsu: ['Nanjing', 'Suzhou', 'Zhenjiang'],
    };
    // console.log('sdj classGrade', this.props.classGrade);
    const newClassGrade = this.props.classGrade.map((val, index) => {
      if(val.children && val.children.length){
        return val;
      }else{
        return {
          ...val,
          disabled: true,
        }
      }
    })
    return (
      <div>
        <BreadcrumbCustom first="用户管理" second="学生档案"/>
        <header style={headerStyle}>基本信息</header>
        <Form
          className="new-student-form"
          onSubmit={this.handleSubmit}
        >

          <FormItem
            {...formItemLayout}
            label="姓名"
          >
            {getFieldDecorator('name', {
              rules: [
              //   {
              //   type: 'email', message: 'The input is not valid E-mail!','Please input your E-mail!'
              // },
                {required: true, message: '请输入姓名!'},
                {max: 100, message: '长度超过限制!'}
              ],
            })(
              <Input />
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="学号"
          >
            {getFieldDecorator('number', {
              rules: [
                // {
                //   type: 'email', message: 'The input is not valid E-mail!',
                // },
                {pattern: /^[0-9]*$/, message: '学号必须为数字!'},
                { required: true, message: '请输入学号!',},
                {max: 100, message: '长度超过限制!'}
              ],
            })(
              <Input />
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="性别"
          >
            {getFieldDecorator('sex', {
              rules: [
                // {
                //   type: 'email', message: 'The input is not valid E-mail!',
                // },
                {
                  required: true, message: '请选择性别!',
                }],
            })(
              <RadioGroup>
                <Radio value="male">男</Radio>
                <Radio value="female">女</Radio>
                {/*<Radio value="unknown">未知</Radio>*/}
              </RadioGroup>
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="出生日期"
          >
            {getFieldDecorator('birthday', {
              rules: [
                {
                  required: true, message: '请选择出生日期',
                }],
            })(
              <DatePicker
                // defaultValue={moment('2015/01/01')}
                onChange={this.getBirth}
                placeholder={"请选择出生日期"}
              />
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="在籍状态"
          >
            {getFieldDecorator('registered',{
              rules:[
                { required: true, message: '请选择在籍状态!'},
              ]
            })(
              <RadioGroup
                onChange={this.changeRegist}
              >
                <Radio value="registered">在籍</Radio>
                <Radio value="noRegistered">不在籍</Radio>
              </RadioGroup>
            )}
          </FormItem>
          <FormItem
            {...formItemLayout}
            label="寄宿类型"
          >
            {getFieldDecorator('board',{
              rules:[
                { required: !this.state.noRegistered, message: '请选择寄宿类型!'},
              ]
            })(
              <RadioGroup>
                <Radio disabled={this.state.noRegistered} value={2}>走读</Radio>
                <Radio disabled={this.state.noRegistered} value={1}>住校</Radio>
              </RadioGroup>
            )}
          </FormItem>
          <FormItem
            {...formItemLayout}
            label="班级"
          >
            {getFieldDecorator('classGrade', {
              rules: [
                // { required: true, message: 'Please select your favourite colors!', type: 'array' },
                { required: !this.state.noRegistered, message: '请选择班级!'},
              ],
            })(
              <Cascader
                options={newClassGrade}
                onChange={this.onClassRoomChange}
                filedNames={{value: 'id'}}  //自定义字段名
                placeholder="请选择班级"
                disabled={this.state.noRegistered}
              />
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="联系电话"
          >
            {getFieldDecorator('phone', {
              rules: [
                // {pattern: /^[1][3,4,5,7,8][0-9]{9}$/, message: '电话号码校验不正确!'},
                {pattern: /^[0-9]*$/, message: '电话号码必须为数字!'},
                {required: true, message: '请输入联系电话!'},
                {max: 100, message: '长度超过限制!'}
              ],
            })(
              <Input />
            )}
          </FormItem>


          <FormItem
            {...formItemLayout}
            label="上传人脸"
          >
            <div className="dropbox">
              {getFieldDecorator('imageIds', {
                rules:[{
                  required: true,message:'请上传头像照片'
                }]
              })(
                <div>
                  <UploadImg
                    handleUpload={this.handleUpload}
                    fileList={this.state.fileList}
                    disabled={
                      this.props.location.state.hasOwnProperty('name')?true:false
                    }
                    limitImgLen = {1}
                  />
                  {
                    this.props.location.state.hasOwnProperty('name')?'修改暂时不支持上传的图片':''
                  }
                </div>
              )}
            </div>
          </FormItem>

          <FormItem
            wrapperCol={{ span: 24, offset: 12 }}
          >
            <Button className="new-student-button" type="primary" htmlType="submit">
              {
                this.props.location.state.hasOwnProperty('name')?'修改':'新建'
              }
            </Button>
            <Button className="new-student-button" onClick={this.cancle}>取消</Button>
          </FormItem>

        </Form>
      </div>
    )
  }
}

export default  Form.create()(NewStudentsManagement)