import PropTypes from 'prop-types';

import React , {Component} from 'react';
import {Table, Button, Pagination ,Card, Spin} from 'antd';
import BreadcrumbCustom from '../../BreadcrumbCustom';
import moment from 'moment';
import MonthPickerRange from '../components/MonthPickerRange';
import {getMothEnd} from '@/utils/common';
const marginStle={
  marginBottom: 15,
  marginRight: 15
}

class Record extends Component {
  state ={
    startTime: moment(),
    endTime: moment(),
    page: 1,
    pageSize: 10,

  }

  componentWillMount() {
    console.log('sdj NewStudentsManagement', this.props);
    const editData = this.props.location.state;
    if(editData && editData.hasOwnProperty('name')){
      this.setState({
        editData: {...editData}
      },()=>{
        this.getStudentRecordList({...this.state}, true);
      })
    }

  }

  changeState= (name, value) => {
    this.setState({
      [name]:value
    })
  }

  //搜索人员出入校信息
  searchRecord = () => {
    const { startTime, endTime, page, pageSize } = this.state;
    //2018-05-16 14:27:55
    this.getStudentRecordList({
      ...this.state,
    });
  }

  // getMothEnd = endTime => {
  //   let endArr= endTime.split('-');
  //   if(endArr.length!==2) return console.log('格式不规范')
  //   const endMoth = endArr[1];
  //   let endtime = '';
  //   switch(endMoth){
  //     case '01':
  //     case '03':
  //     case '05':
  //     case '07':
  //     case '08':
  //     case '10':
  //     case '12':
  //       endtime = '-31 00:00:00';
  //       break;
  //     case '02':
  //       endtime = '-28 00:00:00';
  //       break;
  //     case '04':
  //     case '06':
  //     case '09':
  //     case '11':
  //       endtime='-30 00:00:00';
  //       break;
  //     default:
  //       break;
  //   }
  //   return endTime+endtime;
  // }

  //  搜索的方法
  getStudentRecordList =({startTime, endTime, page, pageSize}, isFirst = false) => {
    const studentId = this.props.location.state.detailId;
    const newStart = startTime.format('YYYY-MM') + '-01 00:00:00';
    const newEnd = getMothEnd(endTime.format('YYYY-MM'));
    console.log('sdj searchRecord',newStart, newEnd);
    this.props.getStudentRecord({
      startTime: startTime&& startTime!==''?  newStart:'',
      endTime: endTime&& endTime!==''? newEnd:'',
      page,
      pageSize,
      studentId
    }, isFirst);
  }

  // 分页
  changePage = (page, pageSize)=> {
    console.log('sdj',page)
    this.setState({
      page: page
    },() => {
      this.getStudentRecordList({...this.state});
    });
  }

  // 调整每页的数量
  onShowSizeChange = (current, pageSize)=> {
    console.log('sdj', current, pageSize);
    this.setState({
      page:1,
      pageSize: pageSize,
    }, () => {
      this.getStudentRecordList({...this.state});
    });
  }
  // 时间戳转为 YYYY-MM-DD HH:mm:ss
  transformTime = time => {
    if(time && time !==''){
      return moment(new Date(time)).format('YYYY-MM-DD HH:mm:ss');
    }else{
      return '- -'
    }
  }
  //返回
  returnRecord = () => {
    this.props.history.push('/app/userManagement/studentsManagement')
  }

  changeStateRes = (record,state, type =1)=> {
    let name =''
    if(state ==0){
      name = type ===1? '未抓拍到该学生入校':'未抓拍到该学生离校';
    }
    else if(state == 1){
      name = type ===1? `${record.aCameraName?record.aCameraName:''}摄像头抓拍到入校`:
        `${record.lCameraName?record.lCameraName:''}摄像头抓拍到离校`;
    }
    else if(state == 2){
      name = type ===1? `${record.teacherName?record.teacherName:''}老师修改了入校记录`:
        `${record.teacherName?record.teacherName:''}老师修改了离校记录`;
    }
    else if(state == 3){
      name = '请假'
    }else{
      name = '未知'
    }
    return name;
  }

  render() {
    // console.log('sdj, this.props.location', this.props.location);
    const columns=[
      {title:'日期',dataIndex:'calendar',key:'name'},
      {title:'入校时间',dataIndex:'',key:'num',render :(text, record)=>(
        <span>
          {this.transformTime(record.aTime)}
        </span>
      )},
      {title:'判断依据',dataIndex:'',key:'className',render :(text, record)=>(
        <span>
          {this.changeStateRes(record, record.aState, 1)}
        </span>
      )},
      {title:'离校时间',dataIndex:'',key:'mobile',render :(text, record)=>(
      <span>
          {this.transformTime(record.lTime)}
        </span>
    )},
      {title:'判断依据',dataIndex:'',key:'face',render :(text, record)=>(
        <span>
          {this.changeStateRes(record, record.lState, 2)}
        </span>
      )},
    ];
    const {startTime, endTime, page, pageSize} = this.state;
    return (
      <div>
        <BreadcrumbCustom first="用户管理" second={this.props.location.state.name+'的出入校门记录'} />
        <nav style={{margin:"10px 0"}}>
          <MonthPickerRange
            startTime={startTime}
            endTime={endTime}
            onChange ={this.changeState}
          />
          <Button  type="primary" onClick={this.searchRecord}>搜索</Button>
          <Button style={{float: "right", marginRight: 15}}onClick={this.returnRecord} className={"search-button"}>
            返回
          </Button>
        </nav>
        <Card className={"record-container"} bordered={false}>
          <Table
            columns={columns}
            dataSource={this.props.studentRecordList}
            bordered
            pagination={false}
          />
          <Pagination
            className="center footer"
            total={this.props.total}
            defaultCurrent={1}
            showSizeChanger
            showQuickJumper
            current={page}
            pageSize={pageSize}
            defaultPageSize={10}
            pageSizeOptions={['10','20','50']}
            onShowSizeChange={this.onShowSizeChange}
            onChange={this.changePage}
          />
        </Card>
        <Spin
          className="wp spin-center zi1060"
          size="large"
          spinning={this.props.spining}
        />
      </div>
    )
  }
}

export default Record;