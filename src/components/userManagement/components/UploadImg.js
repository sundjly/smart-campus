import React , {Component} from 'react';
import { Upload, Icon, Modal } from 'antd';
import intl from 'react-intl-universal';
import './UploadImg.less'

class PicturesWall extends Component {
  state = {
    previewVisible: false,
    previewImage: '',
    // fileList: [
      // {
      //   uid: -1,
      //   name: 'xxx.png',
      //   status: 'done',
      //   url: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png',
      // },
    // ],
  };

  handleCancel = () => this.setState({ previewVisible: false })

  handlePreview = (file) => {
    this.setState({
      previewImage: file.url || file.thumbUrl,
      previewVisible: true,
    });
  }

  // handleChange = ({ fileList }) => this.setState({ fileList })

  render() {
    let token = '';
    let userInfo =
      localStorage.getItem('userInfo') ||
      window.sessionStorage.getItem('sessionUserInfo');
    if (userInfo) {
      token = JSON.parse(userInfo).access_token;
    }
    const { previewVisible, previewImage } = this.state;
    const { fileList, handleUpload,disabled, limitImgLen} = this.props;
    const uploadButton = (
      <div>
        <Icon type="plus" />
        <div className="ant-upload-text">上传人脸</div>
      </div>
    );
    return (
      <div className="clearfix">
        <Upload
          action={
            window.$IF.env.apiBaseURL + '/api/intellif/image/upload/true?type=1'
          }
          headers={{
            Authorization: 'Bearer ' + token
          }}
          listType="picture-card"
          fileList={fileList}
          onPreview={this.handlePreview}
          onChange={handleUpload}
          disabled={disabled}
          className={disabled? 'upload-img':''}
        >
          {fileList && fileList.length >= limitImgLen ? null : uploadButton}
        </Upload>
        <Modal
          visible={previewVisible}
          footer={null}
          onCancel={this.handleCancel}
          disabled={disabled}
        >
          <img alt="example" style={{ width: '100%' }} src={previewImage} />
        </Modal>
      </div>
    );
  }
}

export default PicturesWall;