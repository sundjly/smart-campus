import React, { Component } from "react";
import PropTypes from "prop-types";
import { Modal, Input, Button, Row, Col, message, Progress} from "antd";
import download_templateZip from "@/assets/download/school_template.zip";
import File from "@/utils/File";

const propTypes = {

};

class MultiRegister extends Component {
  state = {
    file: "",
    filePath: "",
    fileName: "",
    fileSize: "",
    showFileError: false,
    upLoadState: false,
    cacheKey:0,//  取消导入时所需的key
    // showUrl: null
  };

  upLoadFile = null;

  chooseFile = () => {
    this.upLoadFile.click();
  };

  componentWillReceiveProps(nextProps) {

  }

  onFileChange = () => {
    const file = this.upLoadFile.files[0];
    const path = this.upLoadFile.value;
    if (!file) {
      return;
    }
    let size = File.getHumanFileSize(file.size);
    this.setState({
      file: file,
      filePath: path,
      fileSize: size
    });
    const fileInfo = this.getFileInfo(path);
    if (fileInfo.type.toLowerCase() !== "zip") {
      this.setState({
        upLoadState: false,
        showFileError: true
      });
    } else {
      this.setState({
        upLoadState: true,
        fileName: fileInfo.name,
        showFileError: false
      });
    }
  };

  getFileInfo = path => {
    let pos1 = path.lastIndexOf("\\");
    let pos2 = path.lastIndexOf(".");
    return {
      name: path.substring(pos1 + 1),
      type: path.substring(pos2 + 1)
    };
  };

  handleOk = () => {
    const { submitTemplate_dispatch } = this.props;
    if (this.state.upLoadState && this.state.filePath) {
      let formData = new FormData();
      formData.append("file", this.state.file);
      let key = Math.ceil(Math.random() * 10000);
      this.setState({
        cacheKey: key
      })
      console.log(formData, key);
      submitTemplate_dispatch(formData, key);
    } else if (!this.state.filePath || !this.state.upLoadState) {
      message.error("上传文件不正确!");
    }
  };

  handleCancel = () => {
    const { changeModalUpload } = this.props;
    changeModalUpload({
      multiRegisterVisible: false,
      multiRegisterLoading: false
    });
  };

  showErrorDetail = errorInfo => {
    let errorContent =[];
    if(errorInfo && errorInfo instanceof Object){
      Object.keys(errorInfo).forEach(key => {
        const singleError = errorInfo[key];
        errorContent.push(
          <p className="modal-error-detail">列表第<span>{key}</span>行出现错误：
            {
              singleError.length && singleError.map((val, index) => {
                return (
                  <span>{val.message}</span>
                )
              })
            }
          </p>
        )
      })
    }else if(errorInfo && typeof errorInfo === 'string'){
      errorContent.push(
        <p
          className="file_type_error"
          style={{display: "block"}}
        >
          您的上传有误，你可以下载<a
          href={this.props.errorInfo}
        >
          错误日志
        </a>来修改错误
        </p>
      )
    }
    return errorContent;
  }
  // 操作批量导入弹出框  operateModal_dispatch,
  closeProgressModal = () => {
    const { operateModal_dispatch,cancelUpLoadModal } = this.props;
    cancelUpLoadModal(this.state.cacheKey);
    operateModal_dispatch({
      multiRegisterProgressVisible: false,
    });
  }

  render() {
    const { multiRegisterVisible, multiRegisterLoading, errorInfo, multiRegisterProgressVisible, multiRegisterProgress, progressError} = this.props;
    const { percent, successNum, failedNum, } = multiRegisterProgress;

    return (
      <div>
        <Modal
          visible={multiRegisterProgressVisible}
          title="导入进度"
          onCancel={this.closeProgressModal}
          maskClosable={false}
          footer={null}
        >
          <div className="mulitple-register-progress" style={{padding: '0 40px'}}>
            <Progress
              percent={percent}
            />
            <div className="mulitple-register-result" style={{marginTop: '20px'}}>
              导入成功<span style={{ color: '#52c41a' }}>{successNum}</span>条，失败<span style={{ color: '#f5222d' }}>{failedNum}</span>条
            </div>
            <div style={{color: 'red'}}>
              <p>{progressError['1']?`出现错误：图片不存在人脸共${progressError['1']}条`:''}</p>
              <p>{progressError['2']?`出现错误：图片人脸数大于1共${progressError['2']}条`:''}</p>
              <p>{progressError['3']?`出现错误：图片在红名单内共${progressError['3']}条`:''}</p>
              <p>{progressError['4']?`出现错误：图片处理引擎异常共${progressError['4']}条`:''}</p>
              <p>{progressError['5']?`出现错误：班级不存在共${progressError['5']}条`:''}</p>
              {
                this.props.progressErrorInfo && this.props.progressErrorInfo !==''?(
                  <p
                    className="file_type_error"
                  >
                    上传完成，你可以下载<a
                    href={this.props.progressErrorInfo}
                  >
                    错误日志
                  </a>来查看其中出现的具体错误
                  </p>
                ):''
              }
            </div>

          </div>
        </Modal>

        <Modal
          visible={multiRegisterVisible}
          title="批量导入"
          onOk={this.handleOk}
          onCancel={this.handleCancel}
          maskClosable={false}
          footer={[
            <Button
              key="submit"
              type="primary"
              loading={multiRegisterLoading}
              onClick={this.handleOk}
            >
              开始上传
            </Button>,
            <Button key="back" onClick={this.handleCancel}>
              取消
            </Button>
          ]}
        >
          <div className="mulitple_monitor_wrap">
            <div className="prepare_wrap">
              <div className="prepare_title">准备工作</div>
              <div className="prepare_content">
                <Row>
                  <Col
                    span={23}
                    offset={1}
                    className="prepare_text"
                  >
                    1.点击
                    <a
                      id="downloadTpl"
                      href={download_templateZip}
                    >
                      下载Excel模板
                    </a>，按照模板的格式填写布控信息。
                  </Col>
                  <Col
                    span={23}
                    offset={1}
                    className="prepare_text"
                  >
                    2.把布控照片和Excel表格压缩成一个压缩包，上传压缩包。
                  </Col>
                </Row>
              </div>
            </div>
            <div className="import_wrap">
              <div className="import_content">
                <Row>
                  <Col
                    span={23}
                    offset={1}
                    className="import_text"
                    style={{ marginBottom: "15px" }}
                  >
                    选择准备好的压缩包 ：
                  </Col>
                </Row>
                <Row>
                  <Col
                    span={23}
                    offset={1}
                    className="import_select_file"
                    style={{ marginBottom: "20px" }}
                  >
                    <Input
                      id="upLoadFileName"
                      style={{
                        width: "300px",
                        marginRight: "20px"
                      }}
                      value={this.state.filePath}
                    />
                    <Button
                      id="getFile"
                      onClick={this.chooseFile.bind(this)}
                    >
                      浏览
                    </Button>
                    <input
                      id="upLoadFile"
                      ref={ele => (this.upLoadFile = ele)}
                      type="file"
                      style={{ display: "none" }}
                      onChange={this.onFileChange}
                    />
                  </Col>
                </Row>
                <Row>
                  <Col
                    span={23}
                    offset={1}
                    style={{ height: "20px", color: "#EE522C" }}
                  >
                    <p
                      className="file_type_error"
                      style={
                        this.state.showFileError
                          ? { display: "block" }
                          : { display: "none" }
                      }
                    >
                      上传文件不是Zip压缩包，请选择符合要求的文件
                    </p>
                  </Col>
                </Row>
                <Row>
                  <Col
                    span={23}
                    offset={1}
                    style={{
                      height: "auto",
                      color: "#EE522C",
                      overflow: 'scroll'
                    }}
                  >
                    {this.showErrorDetail(errorInfo)}
                  </Col>
                </Row>
              </div>
            </div>
          </div>
          <style>
            {`
           .modal-error-detail {
             color: #ccc;
           }
           .modal-error-detail span{
             color: #EE522C;
             margin-right: 5px;
            }
          `}
          </style>
        </Modal>
      </div>
    );
  }
}

MultiRegister.propTypes = propTypes;

export default MultiRegister;
