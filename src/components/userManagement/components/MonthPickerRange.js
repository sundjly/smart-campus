import PropTypes from 'prop-types';
import React ,{Component} from 'react';
import { DatePicker } from 'antd';
const {MonthPicker} = DatePicker;

class DateRange extends React.Component {
  state = {
    // startValue: null,
    // endValue: null,
    endOpen: false,
  };

  disabledStartDate = (startValue) => {
    // const endValue = this.state.endValue;
    const endValue = this.props.endTime;
    if (!startValue || !endValue) {
      return false;
    }
    return startValue.valueOf() > endValue.valueOf();
  }

  disabledEndDate = (endValue) => {
    const startValue = this.props.startTime;
    if (!endValue || !startValue) {
      return false;
    }
    return endValue.valueOf() <= startValue.valueOf();
  }

  // onChange = (field, value) => {
  //   this.setState({
  //     [field]: value,
  //   });
  // }

  onStartChange = (value) => {
    this.props.onChange('startTime', value);
  }

  onEndChange = (value) => {
    this.props.onChange('endTime', value);
  }

  handleStartOpenChange = (open) => {
    if (!open) {
      this.setState({ endOpen: true });
    }
  }

  handleEndOpenChange = (open) => {
    this.setState({ endOpen: open });
  }

  render() {
    const { endOpen } = this.state;
    const {startTime, endTime} = this.props;
    return (
      <div style={{display: 'inline-block', marginRight: 15}}>
        <MonthPicker
          disabledDate={this.disabledStartDate}
          value={startTime}
          placeholder="Start"
          onChange={this.onStartChange}
          onOpenChange={this.handleStartOpenChange}
          style={{marginRight: 15}}
        />
        <MonthPicker
          disabledDate={this.disabledEndDate}
          value={endTime}
          placeholder="End"
          onChange={this.onEndChange}
          open={endOpen}
          onOpenChange={this.handleEndOpenChange}
        />
      </div>
    );
  }
}

export default DateRange