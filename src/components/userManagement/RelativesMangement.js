import React, {Component} from 'react';
import {Row, Col, Card, Icon, Input, Select, Button, Table, Modal, Cascader, Pagination, Spin} from 'antd';
import BreadcrumbCustom from '../BreadcrumbCustom';
import ModalForm from '../systemAccount/ModalForm';
import UpLoadModal from './components/UpLoadModal';
import intl from 'react-intl-universal';
import {deleteStudentInfo, exportExcel} from '@/actions/userManagement/RelativesMangementAction';
import './RelativesMangement.less';

const confirm = Modal.confirm;
const Search = Input.Search;
const Option = Select.Option;
const ExampleFace = 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png';

const Relativedata = [
  {
    id: 1,
    name: 'name',
    className: '三年级8班',
    faceImg: ExampleFace,
    //  接口字段
    number: '11232',
    phone: '13660721158',
    pStatus: '1',//学生(1、在籍 2、不在籍)
    gender: '男',
  },
  {
    id: 2,
    name: 'name',
    className: '三年级2班',
    faceImg: ExampleFace,
    number: '11232',
    phone: '13660721158',
    pStatus: '2',//学生(1、在籍 2、不在籍)
    gender: '女'
  },
];


class RelativesMangement extends Component {
  state = {
    selectedRowKeys: [], // Check here to configure the default column
    page: 1,
    pageSize: 10,
    total: 20,
    spinning: false,
    searchName: '',//姓名或者学号
    classRoom: [], //【年级，班级】
    registered: ''// 在籍状态 all：全部  registered : 在籍   noRegistered：不在籍
  };

  componentWillMount() {
    // const {getClassGradeList} = this.props;
    // getClassGradeList();
    // this.getTaskList({...this.state}, true)
  }

  //改变信息的模板
  changeState = (name, value) => {
    this.setState({
      [name]: value
    })
  }

  searchName = (e) => {
    this.changeState('searchName', e.target.value)
  }

  addAccount = (record) => {
    this.props.history.push(
      {
        pathname: "/app/userManagement/newStudentsManagement",
        state: record
      }
    )
  }

  //  导入
  upLoad = () => {
    this.props.changeModalUpload({
      multiRegisterVisible: true,
      multiRegisterLoading: false,
    })
  }

  //  导出
  downLoad = () => {
    const {searchName, classRoom, registered, page, pageSize} = this.state;
    exportExcel(
      {
        queryStr: searchName,
        page,
        pageSize,
        pType: 1,
        status: registered === 'registered' ? '1' : registered === 'noRegistered' ? '2' : '',
        gradeId: classRoom.length > 1 ? classRoom[classRoom.length - 2] : '',
        classId: classRoom.length > 0 ? classRoom[classRoom.length - 1] : '',
      }
    )
  }

  onSelectChange = (selectedRowKeys) => {
    console.log('selectedRowKeys changed: ', selectedRowKeys);
    this.setState({selectedRowKeys});
  }

  //删除
  showConfirm = (key) => {
    confirm({
      title: '你确定删除这条记录吗?',
      content: '当你点击确认的时候,就会在1s后删除所有相关的记录',
      okText: '确认',
      cancelText: '取消',
      onOk: () => {
        return new Promise((resolve, reject) => {
          console.log('sdj deleteStudentInfo')
          setTimeout(resolve(this.props.deleteStudentInfo(key, this, this.state)), 1000);
        }).catch(() => console.log('Oops errors!'));
      },
      onCancel() {
      },
    });
  }

  //  操作记录
  recordHandle = (record, type = 1) => {
    console.log('sdj record', record)
    this.props.history.push({
      pathname: `/app/userManagement/studentsManagementRecord`,
      state: {...record, type}
    });
  }

  // 搜索查询
  searchButton = () => {
    const {searchName, classRoom, registered, page, pageSize} = this.state;
    console.log('sdj', searchName, classRoom, registered, page, pageSize);
    this.getTaskList({
      ...this.state,
      page: 1
    })
    // 重置搜索显示的page
    this.setState({
      page: 1
    })
  }

  //获取任务列表
  getTaskList = ({searchName, classRoom, registered, page, pageSize}, isFirst = false) => {
    this.props.getRelativesList({
      queryStr: searchName,
      page,
      pageSize,
      pType: 4
    }, isFirst)
  }

  // 分页
  changePage = (page, pageSize) => {
    console.log('sdj', page)
    this.setState({
      page: page
    }, () => {
      this.getTaskList({...this.state})
    });
    //  发请求
  }

  // 调整每页的数量
  onShowSizeChange = (current, pageSize) => {
    console.log('sdj', current, pageSize);
    this.setState({
      page: 1,
      pageSize: pageSize,
      spining: true,
    }, () => {
      this.getTaskList({...this.state})
    });
  }

  render() {
    const {selectedRowKeys, page, pageSize} = this.state;
    const searchStyle = {
      width: 200,
      marginRight: 20,
      marginBottom: 5
    };

    const tableHandle = {
      margin: '0 10px'
    };

    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
      hideDefaultSelections: true,
      selections: [{
        key: 'all-data',
        text: '选择全部',
        onSelect: () => {
          this.setState({
            selectedRowKeys: [...Array(46).keys()], // 0...45
          });
        },
      }, {
        key: 'odd',
        text: '选择奇数列',
        onSelect: (changableRowKeys) => {
          let newSelectedRowKeys = [];
          newSelectedRowKeys = changableRowKeys.filter((key, index) => {
            if (index % 2 !== 0) {
              return false;
            }
            return true;
          });
          this.setState({selectedRowKeys: newSelectedRowKeys});
        },
      }, {
        key: 'even',
        text: '选择偶数列',
        onSelect: (changableRowKeys) => {
          let newSelectedRowKeys = [];
          newSelectedRowKeys = changableRowKeys.filter((key, index) => {
            if (index % 2 !== 0) {
              return true;
            }
            return false;
          });
          this.setState({selectedRowKeys: newSelectedRowKeys});
        },
      }],
      onSelection: this.onSelection,
    };
    const tableData = this.props.relativesList && this.props.relativesList.length ? this.props.relativesList : Relativedata;
    const columns = [
      {
        title: '姓名', className: '', dataIndex: '', key: 'name', render: (text, record) => (
        <span className="sudents-col-name">{record.name}</span>
      )
      },
      {title: '关联学生姓名', className: '', dataIndex: 'number', key: 'num'},
      {
        title: '关联学生学号', className: '', dataIndex: '', key: 'className', render: (text, record) => (
        <span>{record.gradeName ? record.gradeName : '无'}/{record.className ? record.className : '无'}</span>
      )
      },
      {title: '亲属关系', className: '', dataIndex: 'phone', key: 'mobile'},
      {
        title: '联系电话', className: '', dataIndex: '', key: 'face', render: (text, record) => (
        <span>
          {
            record && record.photoData ? <img style={{width: 80, height: 80}} src={record.photoData} alt="人脸照片"/> : ''
          }
        </span>
      )
      },
      {
        title: '人像照片', className: '', dataIndex: '', key: 'roleIds', render: (text, record) =>
        (
          <span>
          {
            record.status == '1' ? '在籍' : record.status == '2' ? '不在籍' : '未知'
          }
        </span>
        )
      },
      {
        title: '操作', className: '', dataIndex: '', key: 'x', render: (text, record) => (
        <span>
          <Button style={tableHandle} title="编辑" onClick={this.addAccount.bind(this, record)}>
            <Icon type="edit"/>
          </Button>
          <Button style={tableHandle} title="删除" onClick={this.showConfirm.bind(this, record.detailId, 'delete')}>
            <Icon type="delete"/>
          </Button>
           <Button style={tableHandle} onClick={this.recordHandle.bind(this, record, 'arrivalSchool')} title="到校">
            访问记录
          </Button>
        </span>
      )
      },
    ];
    const {multiRegisterVisible, progressError, multiRegisterLoading, progressErrorInfo, cancelUpLoadModal, multiRegisterProgress, multiRegisterProgressVisible, errorInfo, changeModalUpload, submitTemplate_dispatch, operateModal_dispatch} = this.props;
    const UpLoadModalProps = {
      multiRegisterVisible,
      multiRegisterLoading,
      errorInfo,// 错误提示信息
      changeModalUpload,
      submitTemplate_dispatch,
      operateModal_dispatch,
      multiRegisterProgress,
      multiRegisterProgressVisible,
      cancelUpLoadModal,
      progressError,
      progressErrorInfo,
    };
    const clientHeight = document.body.clientHeight;
    const tableHeight = (clientHeight - 370);
    console.log('sdj clientHeight', tableHeight)
    return (
      <div className="gutter-example">
        <BreadcrumbCustom first="用户管理" second="亲属档案"/>
        <Row gutter={16}>
          <Col className="gutter-row" md={24}>
            <div className="gutter-box">
              <Input
                placeholder="请输入亲属姓名或者学生姓名"
                onChange={this.searchName}
                style={searchStyle}
              />
              <Button onClick={this.searchButton} type="primary"><Icon type="search"/>查询</Button>
              <Button onClick={this.upLoad}><Icon type="upload"/>导入</Button>
              <Button onClick={this.downLoad}><Icon type="download"/>导出</Button>
              {/*<Button><Icon type="download" />下载模板</Button>*/}
              <UpLoadModal {...UpLoadModalProps} />
            </div>
          </Col>
        </Row>

        <Row gutter={16}>
          <Col className="gutter-row" md={24}>
            <div className="gutter-box">
              <Card bordered={false}>
                <Button
                  onClick={this.addAccount.bind(this, {})}
                  style={{marginBottom: 15}}
                >
                  <Icon type="plus"/>新增
                </Button>

                {
                  !this.props.studentsList
                    // && this.props.studentsList.length
                    ?
                    (
                      <div className="student-management-content">
                        <Table
                          className="students-tab "
                          rowSelection={rowSelection}
                          columns={columns}
                          dataSource={tableData}
                          pagination={false}
                          scroll={{y: tableHeight}}
                        />
                        <Pagination
                          className="footer center "
                          total={this.props.total}
                          defaultCurrent={1}
                          showSizeChanger
                          showQuickJumper
                          current={page}
                          pageSize={pageSize}
                          defaultPageSize={10}
                          pageSizeOptions={['10', '20', '50']}
                          onShowSizeChange={this.onShowSizeChange}
                          onChange={this.changePage}
                        />
                      </div>
                    ) : ''
                }

              </Card>
            </div>
          </Col>
        </Row>

        <Spin
          className="wp spin-center zi1060"
          size="large"
          spinning={this.props.viewListSpinning}
        />
      </div>
    )
  }
}


export default RelativesMangement;