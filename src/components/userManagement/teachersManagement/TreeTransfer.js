import PropTypes from 'prop-types'
import React, {Component} from 'react';
import {Tree, Checkbox, Button, Icon, message} from 'antd';
import './TreeTransfer.less';
import {formatTree} from './TreeTransferAction';

const TreeNode = Tree.TreeNode;
const treeData = [
  {
    title: '0-0',
    key: '0-0',
    children: [{
      title: '0-0-0',
      key: '0-0-0',
      children: [
        {title: '0-0-0-0', key: '0-0-0-0'},
        {title: '0-0-0-1', key: '0-0-0-1'},
        {title: '0-0-0-2', key: '0-0-0-2'},
      ],
    }, {
      title: '0-0-1',
      key: '0-0-1',
      children: [
        {title: '0-0-1-0', key: '0-0-1-0'},
        {title: '0-0-1-1', key: '0-0-1-1'},
        {title: '0-0-1-2', key: '0-0-1-2'},
      ],
    }, {
      title: '0-0-2',
      key: '0-0-2',
    }],
  },
  {
    title: '0-1',
    key: '0-1',
    children: [
      {title: '0-1-0-0', key: '0-1-0-0'},
      {title: '0-1-0-1', key: '0-1-0-1'},
      {title: '0-1-0-2', key: '0-1-0-2'},
    ],
  }, {
    title: '0-2',
    key: '0-2',
  }];
const CheckboxGroup = Checkbox.Group;
const ButtonGroup = Button.Group;

export default class TreeTransfer extends Component {
  state = {
    expandedKeys: [],
    autoExpandParent: true,
    checkedKeys: [],
    selectedKeys: [],
    studentsTreeData: [],// 树形结构
    idMapName: {},// 根据id查找name
    clearCheck: [],// 右边选中的列表
 //用于判断右侧学生列表
    cacheStudentIds:[]
  }

  componentWillMount() {
    const {studentsTree, studentIds} = this.props;
    // console.log('sdj formatTree studentsTree',studentsTree)
    const data = formatTree(studentsTree);
    const {studentsTreeData, idMapName} = data;
    // console.log('sdj formatTree', studentsTreeData, idMapName);
    this.setState({
      studentsTreeData,
      idMapName
    });
  }

  componentDidMount() {

  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.cacheStudentIds !== this.props.cacheStudentIds && nextProps.cacheStudentIds.length){
      this.setState({
        cacheStudentIds: nextProps.cacheStudentIds
      })
    }
  }

  onExpand = (expandedKeys) => {
    console.log('onExpand', arguments);
    // if not set autoExpandParent to false, if children expanded, parent can not collapse.
    // or, you can remove all expanded children keys.
    this.setState({
      expandedKeys,
      autoExpandParent: false,
    });
  }

  // 点击复选框
  onCheck = (checkedKeys) => {
    const newCheck = checkedKeys.length ? checkedKeys.filter((val, index) => {
      return val.indexOf('grade') == -1 && val.indexOf('class') == -1;
    }) : [];
    console.log('onCheck', newCheck);
    this.setState({
      clearCheck: [],
      checkedKeys,
      cacheStudentIds: [],
    });
    this.props.onStateModal('studentIds', newCheck)
  };

  onSelect = (selectedKeys, info) => {
    console.log('onSelect', info);
    this.setState({selectedKeys});
  };

  renderTreeNodes = (data) => {
    return data.map((item) => {
      if (item.children) {
        return (
          <TreeNode title={item.title} key={item.key} dataRef={item}>
            {this.renderTreeNodes(item.children)}
          </TreeNode>
        );
      }
      return <TreeNode {...item} />;
    });
  }

  // 清除已选择
  onTreeBox = checkedValues => {
    console.log('checked = ', checkedValues);
    this.setState({
      clearCheck: checkedValues
    })
  }

  // 创建多选列表
  getTreeOptions = (options = [], idMapName = {}) => {
    let newOptions = [];
    options.length && options.forEach((val, index) => {
      if (idMapName.hasOwnProperty(val)) {
        newOptions.push({
          label: idMapName[val],
          value: val
        })
      }
    })
    return newOptions
  }

  rightCheck = () => {
    const {clearCheck, checkedKeys, cacheStudentIds} = this.state;
    const {studentIds} = this.props;
    const newCheckedKeys = cacheStudentIds.length ? cacheStudentIds : checkedKeys;

    if (clearCheck.length) {
      const newCheck = newCheckedKeys.filter(val => {
        return clearCheck.indexOf(val) == -1;
      })
      this.props.onStateModal('studentIds', newCheck);
      console.log('sdj rightCheck', newCheck);
      this.setState({
        clearCheck: [],
        cacheStudentIds: newCheck
      })
    } else {
      message.error('选择后才能清除选中');
    }
  }

  render() {
    const {studentsTreeData, idMapName, checkedKeys, cacheStudentIds} = this.state;
    const {studentIds} = this.props;
    const newCheckedKeys = cacheStudentIds.length ? cacheStudentIds : studentIds;
    const treeOptions = this.getTreeOptions(newCheckedKeys, idMapName);
    return (
      <div>
        <div className="transfer-content">
          <div className='transfer-left'>
            <p>待选</p>
            <Tree
              checkable
              onExpand={this.onExpand}
              expandedKeys={this.state.expandedKeys}
              autoExpandParent={this.state.autoExpandParent}
              onCheck={this.onCheck}
              checkedKeys={this.state.checkedKeys}
              onSelect={this.onSelect}
              selectedKeys={this.state.selectedKeys}
            >
              {this.renderTreeNodes(studentsTreeData)}
            </Tree>
          </div>
          <div className="transfer-button">
            {/*<ButtonGroup>*/}
            <Button
              type="primary"
              onClick={this.rightCheck}
            >
              <Icon type="left"/>
            </Button>
            <br/>
            <Button
              type="primary"
              onClick={() => {
                message.success('已经添加在右侧了！')
              }}
            >
              <Icon type="right"/>
            </Button>
            {/*</ButtonGroup>*/}
          </div>
          <div className="transfer-right">
            <p>已选</p>
            <CheckboxGroup options={treeOptions} value={this.state.clearCheck} onChange={this.onTreeBox}/>
          </div>
        </div>

      </div>
    )
  }
}