// 格式化数组  并且建立相应的表（通过id找到name）
export const formatTree = (data) => {
  let studentsTreeData = [];
  let idMapName = {};
  if (Array.isArray(data) && data.length) {
    studentsTreeData = data.map((grade, index) => {
      let singleGrade = {
        title: grade.name ? grade.name : grade.label,
        key: 'grade' + grade.id
      };
      const childClass = grade.children;
      if (childClass && childClass.length) {
        const siGrade = childClass.map((siClass, index) => {

          let singleClass = {
            title: siClass.name ? siClass.name : siClass.label,
            key: 'class' + siClass.id
          }
          const studentsList = siClass.nextList;
          if (studentsList && studentsList.length) {
            const clStudent = studentsList.map(siStudent => {
              idMapName[siStudent.id] = siStudent.name ? siStudent.name : siStudent.label;
              return {
                title: siStudent.name ? siStudent.name : siStudent.label,
                key: siStudent.id
              }
            });
            singleClass.children = clStudent;
          }

          return singleClass;
        });
        singleGrade.children = siGrade;
      }
      return singleGrade;
    });
  }
  return {
    studentsTreeData,
    idMapName
  };
}
