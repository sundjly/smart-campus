import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {
  Modal,
  Form,
  Input,
  message,
  Checkbox,
  Button,
  Select,
  AutoComplete,
  Upload,
  Icon,
  Radio,
  DatePicker
} from 'antd';
import BreadcrumbCustom from '../../BreadcrumbCustom';
import UploadImg from '../components/UploadImg';
import moment from 'moment';
import {
  editTeacherForm,
  getEditInfo,
} from '@/actions/userManagement/NewTeachersManagementAction'
import TreeTransfer from './TreeTransfer'

const FormItem = Form.Item;
const Option = Select.Option;
const AutoCompleteOption = AutoComplete.Option;
const RadioGroup = Radio.Group;
const CheckboxGroup = Checkbox.Group;
const classRoomOptions = [
  {
    value: 'zhejiang',
    label: 'Zhejiang',
    children: [{
      value: 'hangzhou',
      label: 'Hangzhou',
      children: [{
        value: 'xihu',
        label: 'West Lake',
      }],
    }],
  },
  {
    value: 'jiangsu',
    label: 'Jiangsu',
    children: [{
      value: 'nanjing',
      label: 'Nanjing',
      children: [{
        value: 'zhonghuamen',
        label: 'Zhong Hua Men',
      }],
    }],
  }
];
const Checkboxoptions = [
  {label: '学生管理权限', value: 'student'},
  {label: '访客审核权限', value: 'visitor'},
  {label: '归寝管理权限', value: 'dormitory'},
];

class NewTeachersManagement extends Component {
  state = {
    confirmDirty: false,
    autoCompleteResult: [],
    teacherId: null, //通过路由获取的需要改变的值
    birthDate: '',
    fileList: [],// 上传人脸
    noRegistered: false, // 判断是否在籍
    permissionStr: [],// 老师权限分配
    studentIds: [],// 学生ID
    cacheStudentIds:[],//
  };

  componentWillMount() {
    console.log('sdj NewTeachersManagement js', this.props.location.state);
    const teacherId = this.props.location.state;

    if (teacherId && JSON.stringify(teacherId) !== '{}') {
      this.setState({
        teacherId
      })
    }
  }

  componentDidMount() {
    //  如果为编辑模式
    if (this.state.teacherId) {
      getEditInfo(this.state.teacherId).then(res => {
        console.log('sdj getEditInfo', res)
        if (!res.errCode && res.data && res.data.zhxyPersonVo && res.data.studentList) {
          const data = res.data;
          const {name, number, phone, photoData, birthday, sex, ptype, status, permissionStr} = data.zhxyPersonVo;
          const students = data.studentList;
          this.props.form.setFieldsValue(
            {
              realName: name,
              number: number,
              birthday: birthday ? moment(birthday, 'YYYY-MM-DD') : moment(),
              sex: sex == 1 ? 'male' : sex == 2 ? 'female' : 'unknown',
              description: ptype == 2 ? '教师' : ptype == 3 ? '职工' : '',
              // pType: values.description == '教师' ? 2 : values.description == '职工' ? 3 : '',
              pStatus: status ? status + '' : '',
              phone: phone ? phone : '',
              permissionStr: permissionStr ? permissionStr : []
            }
          );
          const studentIds = students.length && students.map(val => {
            if(val.sid){
              return val.sid
            }
          })
          console.log('sdj studentIds', studentIds)
          this.setState({
            fileList: [
              {
                uid: -1,
                name: 'a.png',
                status: 'done',
                url: photoData,
              },
            ],
            cacheStudentIds: studentIds
          }, () => {
            this.props.form.setFieldsValue(
              {
                imageIds: this.state.fileList
              })
          });
        }
      })
    }
  }

  //获取生日
  getBirth = (date, dateString) => {
    console.log(date, dateString);
  }

  //图片上传处理
  normFile = (e) => {
    console.log('Upload event:', e);
    if (Array.isArray(e)) {
      return e;
    }
    return e && e.fileList;
  }

  // 班级修改
  // onClassRoomChange = (value) => {
  //   console.log('sdj onClassRoomChange', value)
  // }

  //跳转
  cancle = () => {
    this.props.history.push('/app/userManagement/teacherManagement');
  }

  onChangeCheckbox = checkedValues => {
    this.setState({
      permissionStr: checkedValues
    })
  }

  onStateModal = (name, val) => {
    this.setState({
      [name]: val
    })
  }
  // 提交修改  （新建）
  handleSubmit = (e) => {
    e.preventDefault();
    const {form, location, addTeachersFormSubmit} = this.props;
    form.setFieldsValue({imageIds: this.state.fileList});
    form.validateFieldsAndScroll((err, values) => {

      if (!err) {
        console.log('Received values of form: ', values);
        const birthday = values.birthday.format('YYYY-MM-DD');
        console.log('birthday', birthday);
        //  发起请求进行数据处理
        if (location.state) {
          editTeacherForm({
            ...values,
            id: location.state,
            sex: values.sex == 'male' ? '男' : values.sex == 'female' ? '女' : '',
            birthday,
            pType: values.description == '教师' ? 2 : values.description == '职工' ? 3 : '',
            bankType: 1,
          }, this.props.history);
        } else {
          addTeachersFormSubmit({
            ...values,
            sex: values.sex == 'male' ? '男' : values.sex == 'female' ? '女' : '',
            birthday,
            bankType: 1,
            pType: values.description == '教师' ? 2 : values.description == '职工' ? 3 : '',
          }, this.props.history);
        }
      }
    });
  }

  changeRegist = e => {
    console.log('sdj changeRegist', e.target.value);
    if (e.target.value === 'noRegistered') {
      this.setState({
        noRegistered: true
      })
    } else {
      this.setState({
        noRegistered: false
      })
    }
  }
  // uploadFace
  handleUpload = info => {
    if (!(info && info.file)) return;
    console.log('uploadStatus', info);
    switch (info.file.status) {
      case 'error':
        this.setState({
          fileList: []
        });
        break;
      case 'done':
        // info.file.response字段存放上传图片之后的接口返回信息
        if (info.file.response) {
          const res = info.file.response;
          let filelists = this.state.fileList;
          if (res && res.errCode == 0) {
            if (res.data.faces == 1) {
              // 正确上传
              this.setState({
                fileList: info.fileList
              });
            } else if (res.data.faces == -1) {
              message.error('您上传的图片可能是系统红名单人员，被限制上传操作！');
              // 匹配到红名单人员
              this.setState({
                fileList: filelists.splice(0, [filelists.length - 1]),
                // tipModalVisible: true,
                // tipModalMsg: '您上传的图片可能是系统红名单人员，被限制上传操作！'
              });
            } else {
              // 上传图片不合规
              message.error('上传图片没有人脸或人脸多于一张，请重新上传');

              this.setState({
                fileList: filelists.splice(0, [filelists.length - 1]),
                // tipModalVisible: true,
                // tipModalMsg: '上传图片没有人脸或人脸多于一张，请重新上传'
              });
            }
          } else if (res.errCode == 1001) {
            // 接口报错异常处理
            message.error('数据异常')
            this.setState({
              fileList: [],
              // tipModalVisible: true,
              // tipModalMsg: '数据异常'
            });
          }
        }
        break;
      case 'uploading':
      case 'removed':
      default:
        this.setState({
          fileList: info.fileList
        });
    }
    this.props.form.setFieldsValue({imageIds: info.fileList});
  };
  // 上传老师关联学生
  pushStudents = () => {
    const {studentIds} = this.state;
    const {pushStudent, history, location} = this.props;
    let teacherId = this.props.teacherId || location.state;
    console.log('studentIds', studentIds, teacherId)
    if (teacherId !== '' && teacherId && studentIds.length) {

      const newIDs = studentIds.filter((val, index) => {
        return val.indexOf('grade') == -1 && val.indexOf('class') == -1;
      })
      pushStudent({
        studentIds: newIDs,
        teacherId
      }, history)
    } else {
      message.error('添加老师的学生失败！')
    }
  }

  render() {

    const {getFieldDecorator} = this.props.form;
    const {autoCompleteResult} = this.state;

    const formItemLayout = {
      labelCol: {
        xs: {span: 12},
        sm: {span: 8},
      },
      wrapperCol: {
        xs: {span: 24},
        sm: {span: 16},
      },
    };
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0,
        },
        sm: {
          span: 16,
          offset: 8,
        },
      },
    };
    const headerStyle = {
      fontSize: 25,
      borderBottom: '1px solid #ccc',
      marginBottom: 15
    };
    // 班级信息
    const gradeData = ['Zhejiang', 'Jiangsu'];
    const classRoomData = {
      Zhejiang: ['Hangzhou', 'Ningbo', 'Wenzhou'],
      Jiangsu: ['Nanjing', 'Suzhou', 'Zhenjiang'],
    };
    // console.log('sdj classGrade', this.props.classGrade);

    // const newClassGrade = this.props.classGrade.map((val, index) => {
    //   if(val.children && val.children.length){
    //     return val;
    //   }else{
    //     return {
    //       ...val,
    //       disabled: true,
    //     }
    //   }
    // })

    return (
      <div>
        <BreadcrumbCustom first="用户管理" second="教师档案"/>
        <header style={headerStyle}>第一步：基本信息</header>
        <Form
          className="new-student-form"
          onSubmit={this.handleSubmit}
        >

          <FormItem
            {...formItemLayout}
            label="姓名"
          >
            {getFieldDecorator('realName', {
              rules: [
                //   {
                //   type: 'email', message: 'The input is not valid E-mail!','Please input your E-mail!'
                // },
                {required: true, message: '请输入姓名!'},
                {max: 100, message: '长度超过限制!'}
              ],
            })(
              <Input/>
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="工号"
          >
            {getFieldDecorator('number', {
              rules: [
                // {
                //   type: 'email', message: 'The input is not valid E-mail!',
                // },
                {pattern: /^[0-9]*$/, message: '工号必须为数字!'},
                {required: true, message: '请输入工号!',},
                {max: 100, message: '长度超过限制!'}
              ],
            })(
              <Input/>
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="性别"
          >
            {getFieldDecorator('sex', {
              rules: [
                // {
                //   type: 'email', message: 'The input is not valid E-mail!',
                // },
                {
                  required: true, message: '请选择性别!',
                }],
            })(
              <RadioGroup>
                <Radio value="male">男</Radio>
                <Radio value="female">女</Radio>
                {/*<Radio value="unknown">未知</Radio>*/}
              </RadioGroup>
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="出生日期"
          >
            {getFieldDecorator('birthday', {
              rules: [
                {
                  required: true, message: '请选择出生日期',
                }],
            })(
              <DatePicker
                // defaultValue={moment('2015/01/01')}
                onChange={this.getBirth}
                placeholder={"请选择出生日期"}
              />
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="类别"
          >
            {getFieldDecorator('description', {
              rules: [
                // {
                //   type: 'email', message: 'The input is not valid E-mail!',
                // },
                {
                  required: true, message: '请选择类别!',
                }],
            })(
              <RadioGroup>
                <Radio value="教师">教师</Radio>
                <Radio value="职工">职工</Radio>
                {/*<Radio value="unknown">未知</Radio>*/}
              </RadioGroup>
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="在职状态"
          >
            {getFieldDecorator('pStatus', {
              rules: [
                {required: true, message: '请选择在籍状态!'},
              ]
            })(
              <RadioGroup
                onChange={this.changeRegist}
              >
                <Radio value="1">在职</Radio>
                <Radio value="2">离职</Radio>
              </RadioGroup>
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="联系电话"
          >
            {getFieldDecorator('phone', {
              rules: [
                // {pattern: /^[1][3,4,5,7,8][0-9]{9}$/, message: '电话号码校验不正确!'},
                {pattern: /^[0-9]*$/, message: '电话号码必须为数字!'},
                {required: true, message: '请输入联系电话!'},
                {max: 100, message: '长度超过限制!'}
              ],
            })(
              <Input/>
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="上传人脸"
          >
            <div className="dropbox">
              {getFieldDecorator('imageIds', {
                rules: [{
                  required: true, message: '请上传头像照片'
                }]
              })(
                <div>
                  <UploadImg
                    handleUpload={this.handleUpload}
                    fileList={this.state.fileList}
                    disabled={
                      this.props.location.state.hasOwnProperty('name') ? true : false
                    }
                    limitImgLen={1}
                  />
                  {
                    this.props.location.state.hasOwnProperty('name') ? '修改暂时不支持上传的图片' : ''
                  }
                </div>
              )}
            </div>
          </FormItem>
          <header style={headerStyle}>第二步：业务权限</header>
          <FormItem
            {...formItemLayout}
            label="业务权限"
          >
            {getFieldDecorator('permissionStr', {
              rules: [
                {required: true, message: '请业务权限!'},
              ]
            })(
              //
              <CheckboxGroup options={Checkboxoptions} onChange={this.onChangeCheckbox}/>
            )}
          </FormItem>

          <FormItem
            wrapperCol={{span: 24, offset: 12}}
          >
            <Button className="new-student-button" type="primary" htmlType="submit">
              {
                this.props.location.state.hasOwnProperty('name') ? '修改' : '新建'
              }
            </Button>
          </FormItem>
          <header style={headerStyle}>第三步：管理对象</header>
          <TreeTransfer
            studentsTree={this.props.studentsTree}
            onStateModal={this.onStateModal}
            studentIds={this.state.studentIds}
            cacheStudentIds={this.state.cacheStudentIds}
          />
          <FormItem
            wrapperCol={{span: 24, offset: 12}}
          >
            <Button type="primary" onClick={this.pushStudents}>保存</Button>
            <Button className="new-student-button" onClick={this.cancle}>取消</Button>
          </FormItem>
        </Form>
      </div>
    )
  }
}

export default Form.create()(NewTeachersManagement)