import React, {Component} from 'react';
import {Card, Spin, message} from 'antd';
import request from '@/utils/request';
import './PhoneStudentsRecord.less';
import moment from 'moment';
import {PhotoSwipe} from 'react-photoswipe';
import 'react-photoswipe/lib/photoswipe.css';
import $ from 'jquery';
const ExampleFace = 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png';

class PhoneStudentsRecord extends Component {

  state={
    resData: [],
    page:1,
    pageSize:10,
    bigImg:[
      {
        src: '',
        w: 1200,
        h: 900,
        title: ''
      },
    ],
    isOpen: false
  }

  componentWillMount() {
    this.gerRequestList(this.props.location.state);
  }
  componentDidMount() {
    $('.ant-layout-content').addClass('ant-layout-content-phone');
    $('.ant-layout-header').hide();
  }
  componentWillUnmount () {
    $('.ant-layout-content').removeClass('ant-layout-content-phone');
    $('.ant-layout-header').show();
  }
  gerRequestList = (studentId) => {
    const startTime = moment().subtract(14,'days').format('YYYY-MM-DD HH:MM:SS');
    const endTime = moment().format('YYYY-MM-DD HH:MM:SS');
    return request({
      url:'/api/intellif/zhxy/student/attendance/record',
      method:'post',
      data:{
        studentId,
        startTime,
        endTime
      }
    }).then(res => {
      if(res.errCode ===0){
        console.log('sdj', res)
        this.setState({
          resData: [...this.state.resData,...res.data.detail]
        })
      }else{
        message.error('数据获取失败')
      }
    })
  }
  //点击查看大图  请求接口

  getBigRequest = (imageId, realName)=> {
    return request({
      url: '/api/intellif/image/'+imageId,
      method: 'get',
      data: {}
    });
  }

  getAllList = data => {
     const {calendar, realName, photoData, imageId, aState, lState, lTime, aTime, } = data;
     let contentHtml = [];
     // if(aState == 0 && lState == 0){
     //   contentHtml= this.getList(calendar, realName, photoData, aTime, aState, imageId);
     //   // contentHtml += this.getList(calendar, realName, photoData, lTime, lState);
     // }else {
     contentHtml.push(this.getList(calendar, realName, photoData, aTime, aState, imageId, 'arrive'));
     contentHtml.push(this.getList(calendar, realName, photoData, lTime, lState, imageId, 'leave'));
      return contentHtml;
  }

  // getList = (data) => {
  getList =(calendar, realName, photoData, aTime, cState, imageId, type) => {
    //calendar "2018-05-11"
    // const {calendar, realName, aTime, photoData, cState, lTime} = data;
    let calendarArr = calendar? calendar.split('-'):['','','暂无'];
    let time = aTime?moment(new Date(aTime)).format('YYYY-MM-DD HH:mm:ss'):'';
    // if(cState == 1){
    //   time = aTime?moment(new Date(aTime)).format('YYYY-MM-DD HH:mm:ss'):'';
    // }else if(cState == 2){
    //   time = lTime?moment(new Date(lTime)).format('YYYY-MM-DD HH:mm:ss'):'';
    // }
    let hour = '没有时间';
    if(time !== ''){
      hour = time.split(' ')[1];
    }
    const stateName = this.switchState(cState, type);
    return (
      <div className="students-record">
        <div className="students-left">
          <span>{calendarArr[calendarArr.length - 1]}</span>
          <span>{calendarArr[calendarArr.length - 2]}月</span>
        </div>
        <div  className="students-right">
          <img src={photoData} alt={''} onClick={this.clickBig.bind(this, {photoData, realName, imageId})}/>
          <div>
            <div className="student-info">
              <span>{this.limitNameLenth(realName)}{stateName}</span>
              {
                cState !== 3 && cState !==0 ?(
                  <span>{hour}</span>
                ):(<span></span>)
              }
            </div>
            <p>您的孩子{this.limitNameLenth(realName)}今天{stateName}</p>
          </div>
        </div>
      </div>
    )
  }
  // 根据state获取状态
  switchState = (cState, type='leave') => {
    let stateName = '';
    if(cState !==0 && !cState) return;
    switch(cState) {
      case 0 :
        if(type=='leave'){
          stateName = '未抓拍到离校';
        }else{
          stateName = '未抓拍到到校';
        }
        break;
      case 1 :
        if(type=='leave'){
          stateName = '离校';
        }else{
          stateName = '到校';
        }
        break;
      case 2 :
        if(type=='leave'){
          stateName = '离校';
        }else{
          stateName = '到校';
        }
        break;
      case 3 :
        stateName = '请假';
        break;
      default :
        stateName='没有信息';
        break;
    }
    return stateName;
  }

  closeBigImg = ()=>{
    this.setState({
      isOpen: false
    })
  }

  limitNameLenth = name => {
    return name? name.length>5?name.substr(0, 5)+'...': name : ''
  }

  clickBig = ({photoData, realName, imageId}) => {
    // const {photoData, realName, imageId} = data;
    if(imageId && imageId > 0){
      // 有抓拍图片才显示大图
      this.getBigRequest(imageId, realName).then(res => {
        if(!res.errCode && res.data && res.data.uri){
          this.setState({
            bigImg:[
              {
                src: res.data.uri,
                w: 1200,
                h: 900,
                title: realName
              },
            ],
            isOpen: true
          });
        }else{
          this.setState({
            bigImg:[
              {
                src: photoData,
                w: 1200,
                h: 900,
                title: realName
              },
            ],
            isOpen: true
          });
        }
      })
    }else{
      this.setState({
        bigImg:[
          {
            src: photoData,
            w: 1200,
            h: 900,
            title: realName
          },
        ],
        isOpen: true
      });
    }

  }
  render (){
    // console.log('sdj', this.state.resData)
    const resData = this.state.resData;
    const showData = resData.length?
      resData.map((val,index) => {
        return this.getAllList(val);
      }) : (
        <div className="center">没有数据信息</div>
      );
    const options ={};
    return (
        <div className="phone-container">
          {showData}
          <PhotoSwipe
            isOpen={this.state.isOpen}
            items={this.state.bigImg}
            onClose={this.closeBigImg}
            options={options}
          />
        </div>
    )
  }
}

export default PhoneStudentsRecord;

{/*<div class="students-record">*/}
{/*<div className="students-left">*/}
{/*<span>21</span>*/}
{/*<span>11月</span>*/}
{/*</div>*/}
{/*<div  className="students-right">*/}
{/*<img src={ExampleFace} alt={''}/>*/}
{/*<div>*/}
{/*<div className="student-info">*/}
{/*<span>王晓丽入园</span>*/}
{/*<span>5:23</span>*/}
{/*</div>*/}
{/*<p>你的孩子已于今天8:30分进入校园</p>*/}
{/*</div>*/}
{/*</div>*/}
{/*</div>*/}