import React, { Component } from 'react';
import { Form, Button, Select, Input } from "antd";
import intl from "react-intl-universal";

const FormItem = Form.Item;
const Option = Select.Option;

class AddMarkComponent extends Component {

    componentDidMount() {
        const { getFacilitiesList_dispatch, } = this.props;
        //获取设备列表
        getFacilitiesList_dispatch({
            page: 1,
            pageSize: 100,
        });
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.setFieldsValueObj !== nextProps.setFieldsValueObj) {
            nextProps.setFieldsValueObj && nextProps.form.setFieldsValue(
                {
                    address: nextProps.setFieldsValueObj.address,
                    cameraId: nextProps.setFieldsValueObj.cameraId
                }
            )
        }

    }

    handleSubmit = (e) => {
        const { form, setFieldsValueObj } = this.props;
        e.preventDefault();
        form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                values.position = setFieldsValueObj.position ? setFieldsValueObj.position : "";
                values.index = typeof setFieldsValueObj.index === "number" ? setFieldsValueObj.index : "";
                this.props.handleSaveClick(values);
            }
        });

    }

    render() {
        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {
            labelCol: { span: 8 },
            wrapperCol: { span: 14, offset: 2 }
        };
        return (
            <Form onSubmit={this.handleSubmit} style={{ margin: "10px" }}>
                <FormItem label={"点位名称"} {...formItemLayout} key={1}>
                    {getFieldDecorator("address", {
                        rules: [
                            { required: true, message: "点位名称名称为必填项！" },
                            {
                                max: 30, message: "长度不超过30个字符!",
                            }
                        ]
                    })(<Input type="text" />)}
                </FormItem>
                <FormItem label={"摄像头"} {...formItemLayout} key={2}>
                    {getFieldDecorator("cameraId", {
                        rules: [
                            { required: true, message: "摄像头为必选项！" }
                        ]
                    })(
                        <Select
                            placeholder="请选择设备类型"
                        >
                            {
                                this.props.facilitiesList.map(item =>
                                    <Option
                                        value={item.key}
                                        key={item.key}
                                    >
                                        {item.name}
                                    </Option>)
                            }
                        </Select>
                    )}
                </FormItem>

                <FormItem >
                    {
                        this.props.setFieldsValueObj
                        &&
                        this.props.setFieldsValueObj.cameraId
                        &&
                        <Button
                            onClick={this.props.handleDeleteClick.bind(this, this.props.setFieldsValueObj.index)}
                            className="modalFormBtn"
                            size="small"
                        >
                            删除
                        </Button>
                    }
                    <Button
                        type="primary"
                        size="small"
                        htmlType="submit"
                        className="modalFormBtn"
                    >
                        确定
                    </Button>
                    <Button
                        type="primary"
                        size="small"
                        className="modalFormBtn"
                        onClick={this.props.handleCancelClick}
                    >
                        取消
                    </Button>
                </FormItem>
            </Form>
        );
    }

}
const AddMarkForm = Form.create()(AddMarkComponent);
export default AddMarkForm;
