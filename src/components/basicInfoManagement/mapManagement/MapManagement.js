
import React, { Component } from 'react';
import {
     Icon, Row, Col, Button, Input, Spin, Pagination
} from 'antd';
import "./MapManagement.css";
import BreadcrumbCustom from '../../BreadcrumbCustom.js';
const Search = Input.Search;

class MapManagement extends Component {
    state = {
        page: 1,
        pageSize: 10,
        name: ""
    }

    componentDidMount() {
        const { getMapList_dispatch } = this.props;
        //获取地图列表
        getMapList_dispatch({
            page: this.state.page,
            pageSize: this.state.pageSize,
            name: this.state.name
        })
    }


    //点击地图进行编辑
    handleClickMap = (maoInfo, e) => {
        const { history } = this.props;
        history.push("/app/basicInfoManagement/addOrEditMap", { ...maoInfo });
    }

    //删除地图
    handleClickRemoveMap = (mapId, e) => {
        const { deleteMap_dispatch, getMapList_dispatch } = this.props;
        deleteMap_dispatch(mapId).then((code) => {
            setTimeout(() => {
                if (code === 0) {
                    getMapList_dispatch({
                        page: this.state.page,
                        pageSize: this.state.pageSize,
                        name: this.state.name
                    });
                }
            }, 500);
        });
    }

    //新增地图
    handleClickPlus = (e) => {
        const { history } = this.props;
        history.push("/app/basicInfoManagement/addOrEditMap");
    }

    render() {
        const { total, mapList, spinning } = this.props;
        return (
            <div>
                <BreadcrumbCustom first="基础信息管理" second="地图管理" />
                <Row gutter={16}>
                    <Col className="gutter-row" md={24}>
                        <div className="gutter-box">
                            <Button
                                onClick={this.handleClickPlus}
                                type="primary"
                                style={{}}
                            >
                                <Icon type="plus" />
                                新增
                            </Button>
                        </div>
                    </Col>
                </Row>
                <Spin
                    size="large"
                    spinning={spinning}
                >
                    {
                        mapList.length > 0 && mapList.map((item) => {
                            return (
                                <div className={"imgBoxCard"} style={{ marginTop: "10px", display: "inline-block", marginRight: "10px" }} key={item.mapId}>
                                    <Icon
                                        type="close"
                                        className="upload_map_remove"
                                        onClick={this.handleClickRemoveMap.bind(this, item.mapId)}
                                    />
                                    <img
                                        alt="map"
                                        className={"imgBox"}
                                        src={item.mapUrl}
                                        onClick={this.handleClickMap.bind(this, item)}
                                    />
                                    <div className={"imgBoxName"}>{item.mapName}</div>
                                </div>
                            );
                        })
                    }
                </Spin>
                <Pagination
                    total={total}
                    onChange={this.onChange}
                    onShowSizeChange={this.onShowSizeChange}
                    hideOnSinglePage
                />
            </div>

        );
    }
}

export default MapManagement;
