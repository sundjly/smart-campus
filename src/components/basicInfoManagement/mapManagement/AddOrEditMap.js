import React, { Component } from 'react';
import { Row, Col, Modal, Input, Select, Button, Spin, Icon, message } from 'antd';
import AddMarkForm from './AddMarkForm.js';
import $ from 'jquery';
import "./AddOrEditMap.css";
import BreadcrumbCustom from '../../BreadcrumbCustom.js';

const Option = Select.Option;


class AddOrEditMap extends Component {
    constructor(props) {
        super(props);
        const { location } = props;
        this.state = {
            mapName: location.state ? location.state.mapName : "",
            mapId: location.state ? location.state.mapId : "",
            suitType: location.state ? location.state.suitType : "",
            mapUrl: location.state ? location.state.mapUrl : "",
            relativeInfoList: location.state ? location.state.relativeInfoList : [],
            setFieldsValueObj: null,
            recommendationVisible: false
        }
        //获取容器对body的偏移量
        this.getContainerOffset = (obj) => {
            var x = 0, y = 0;
            x += obj.offsetLeft;
            y += obj.offsetTop;
            return { x: x, y: y };
        }
        //获取摄像机点位对容器的偏移量
        this.getCameraOffset = (oEvent) => {
            var container = $("#AddOrEditMap_container").get(0);
            var offset = this.getContainerOffset(container);
            var x = ((oEvent.clientX - offset.x - 15) / 600).toFixed(8) * 100 + "%";
            var y = ((oEvent.clientY - offset.y - 15) / 600).toFixed(8) * 100 + "%";
            return { x: x, y: y };
        }
        //绑定右键地图事件
        this.bindRightClickEvent = () => {
            $("#AddOrEditMap_map").on("contextmenu", (oEvent) => {
                if (!this.state.mapUrl) {
                    return;
                }
                oEvent.preventDefault();
                var offset = this.getCameraOffset(oEvent);
                this.showInfoModal(oEvent.clientX, oEvent.clientY);
                //清空表单初始值
                this.setState({
                    setFieldsValueObj: {
                        address: "",
                        cameraId: "",
                        position: `${offset.x},${offset.y}`
                    }
                });
            })
        }
        //显示点位信息弹框
        this.showInfoModal = (x, y) => {
            $("#addMarkBox").css({
                display: "block",
                left: x + "px",
                top: y + "px",
            });
        }
        //隐藏点位信息弹框
        this.hideInfoModal = () => {
            $("#addMarkBox").css({
                display: "none",
            });
        }
        //添加单个点位
        this.addMark = (p, x, y, i, mark) => {
            var div = document.createElement("div");
            div.id = "mark" + i;
            div.className = "mark";
            div.style.left = x;
            div.style.top = y;
            $(div).appendTo(p);
            $(div).on("click", (oEvent) => {
                this.showInfoModal(oEvent.clientX, oEvent.clientY);
                //写入表单初始值
                this.setState({
                    setFieldsValueObj: {
                        address: mark.address,
                        cameraId: mark.cameraId,
                        index: i
                    }
                });
            });
            $(div).on("contextmenu", (oEvent) => {
                oEvent.preventDefault();
            });
        }
        //渲染所有点位
        this.loadMark = (relativeInfoList) => {
            var container = $("#AddOrEditMap_container");
            for (var i = 0; i < relativeInfoList.length; i++) {
                var postionArr = relativeInfoList[i].position.split(",");
                var x = postionArr[0];
                var y = postionArr[1];
                this.addMark(container, x, y, i, relativeInfoList[i]);
            }
        }
    }

    componentDidMount() {
        const { relativeInfoList } = this.state;
        this.loadMark(relativeInfoList)
        this.bindRightClickEvent();
        ;
    }

    uploadBtn = null;

    //改变地图名字
    handleMapName = (e) => {
        this.setState({ mapName: e.target.value });
    }

    //改变地图适配终端
    handleTerminalChange = (value) => {
        this.setState({ suitType: value });
    }

    //一键清除所有点位
    handleClickClear = () => {
        this.setState((preState, props) => {
            preState.relativeInfoList.map((item, index) => {
                $("#mark" + index).remove();
            });
            let currentState = { ...preState };
            currentState.relativeInfoList = [];
            return { relativeInfoList: currentState.relativeInfoList };
        });
    }

    /*更新或者上传地图start*/
    handleClickUpload = () => {
        this.setState({
            recommendationVisible: true
        });
    }
    handleRecommendationOk = () => {
        this.setState({
            recommendationVisible: false
        });
        //弹出文件上传窗口
        const evt = new MouseEvent("click", {
            bubbles: true,
            view: window
        });
        this.uploadBtn.dispatchEvent(evt);
    }
    handleUploadMapChange = (e) => {
        const { uploadMap_dispatch } = this.props;
        e.preventDefault();
        e.stopPropagation();

        const fileList = e.target.files || e.dataTransfer.files;
        let formData = new FormData();
        formData.append('file', fileList[0]);
        uploadMap_dispatch(formData).then((mapUrl) => {
            if (mapUrl) {
                this.setState({
                    mapUrl: mapUrl
                })
            }
            this.handleClickClear();
        });
        //修复不能上传同一张地图的bug
        e.target.value = "";
    }
    handleRecommendationCancel = () => {
        this.setState({
            recommendationVisible: false
        });
    }
    /*更新或者上传地图end*/

    //保存地图按钮
    handleSaveBtn = () => {
        if (!this.state.mapName) {
            return message.error("请先填写地图名称！");
        } else if (typeof this.state.suitType !== "number") {
            return message.error("请先选择适配终端！");
        } else if (!this.state.mapUrl) {
            return message.error("请先上传地图！");
        }
        const { addOrEditMap_dispatch, history } = this.props;
        let requestData = {
            mapName: this.state.mapName,
            suitType: this.state.suitType,
            mapUrl: this.state.mapUrl,
            relativeInfoList: this.state.relativeInfoList,
        };
        if (this.state.mapId) {
            requestData.mapId = this.state.mapId;
        }
        addOrEditMap_dispatch(requestData).then((code) => {
            if (code === 0) {
                history.push("/app/basicInfoManagement/mapManagement");
            }
        });
    }

    //退出按钮
    handleCancelBtn = () => {
        const { history } = this.props;
        history.push("/app/basicInfoManagement/mapManagement");
    }

    //保存点位
    handleSaveClick = (values) => {
        if (typeof values.index === "number") {
            //修改现有点位信息
            this.setState((preState, props) => {
                //删除现有点位
                preState.relativeInfoList.map((item, index) => {
                    $("#mark" + index).remove();
                });
                let currentState = { ...preState };
                currentState.relativeInfoList.find((ele, i, arr) => {
                    if (i === values.index) {
                        let exchangeObj = {}
                        exchangeObj.address = values.address;
                        exchangeObj.cameraId = values.cameraId
                        exchangeObj.position = arr[i].position;
                        arr[i] = exchangeObj;
                        return true;
                    } else {
                        return false;
                    }
                });
                //渲染新点位
                this.loadMark(currentState.relativeInfoList);
                return {
                    relativeInfoList: currentState.relativeInfoList,
                    setFieldsValueObj: {
                        address: values.address,
                        cameraId: values.cameraId,
                        index: values.index
                    }
                };
            })
        } else if (values.position) {
            //新增点位
            this.setState((preState, props) => {
                //删除现有点位
                preState.relativeInfoList.map((item, index) => {
                    $("#mark" + index).remove();
                });
                let currentState = { ...preState };
                //渲染新点位
                currentState.relativeInfoList.push(values);
                this.loadMark(currentState.relativeInfoList);
                return { relativeInfoList: currentState.relativeInfoList };
            })
        }
        this.hideInfoModal();
    }

    //删除点位
    handleDeleteClick = (index) => {
        typeof index === "number" && this.setState((preState, props) => {
            preState.relativeInfoList.find((ele, i) => {
                if (index === i) {
                    $("#mark" + i).remove();
                    return true;
                } else {
                    return false;
                }

            });
            let currentState = { ...preState };
            currentState.relativeInfoList.splice(index, 1);
            return { relativeInfoList: currentState.relativeInfoList };
        });
        this.hideInfoModal();
    }

    //取消点位
    handleCancelClick = () => {
        this.hideInfoModal();
    }

    render() {
        const searchStyle = {
            width: 200,
            marginRight: 20,
            marginBottom: 5
        };
        const { mapName, mapUrl, suitType, mapId } = this.state;
        return (
            <div style={{ background: "rgb(255, 255, 255)" }}>
                <BreadcrumbCustom first="基础信息管理" second="地图编辑/新增" />
                <Row gutter={16}>
                    <Col className="gutter-row" md={24}>
                        <div className="gutter-box">
                            <Input
                                placeholder="请输入地图名称"
                                onChange={this.handleMapName}
                                style={searchStyle}
                                defaultValue={mapName}
                            />
                            <Select
                                placeholder="请选择适用终端"
                                onChange={this.handleTerminalChange}
                                style={searchStyle}
                                defaultValue={suitType}
                            >
                                <Option
                                    key={0}
                                    value={0}
                                >
                                    {"PC端"}
                                </Option>
                                <Option
                                    key={1}
                                    value={1}
                                >
                                    {"移动端"}
                                </Option>
                            </Select>

                            <Button onClick={this.handleClickClear} type="primary">
                                <Icon type="delete" />
                                一键清除
                            </Button>

                            <Button onClick={this.handleClickUpload} type="primary">
                                <Icon type="upload" />
                                {mapId ? "更新图片" : "上传图片"}
                            </Button>

                            <Button onClick={this.handleSaveBtn} type="primary">
                                <Icon type="save" />
                                保存
                            </Button>

                            <Button onClick={this.handleCancelBtn} type="primary">
                                <Icon type="step-backward" />
                                退出
                            </Button>
                        </div>
                    </Col>
                </Row>
                <div id="AddOrEditMap_container">
                    <img id="AddOrEditMap_map" src={this.state.mapUrl} alt="请上传图片" />
                </div>
                <input
                    type={"file"}
                    onChange={this.handleUploadMapChange}
                    ref={(ele) => (this.uploadBtn = ele)}
                    style={{ display: "none" }}
                />
                <div id={"addMarkBox"} style={{ display: "none" }}>
                    <AddMarkForm
                        handleSaveClick={this.handleSaveClick}
                        handleCancelClick={this.handleCancelClick}
                        handleDeleteClick={this.handleDeleteClick}
                        setFieldsValueObj={this.state.setFieldsValueObj}
                        {...this.props}
                    />
                </div>
                <Modal
                    title="地图上传建议"
                    visible={this.state.recommendationVisible}
                    onOk={this.handleRecommendationOk}
                    onCancel={this.handleRecommendationCancel}
                >
                    <p>为保证地图显示的精确度，请上传长宽比为1:1的地图</p>
                </Modal>
            </div>
        );
    }
}

export default AddOrEditMap;
