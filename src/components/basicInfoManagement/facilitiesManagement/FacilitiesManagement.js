import React, { Component } from 'react';
import { Row, Col, Card, Input, Select, Button, Table, Spin, Icon } from 'antd';
import BreadcrumbCustom from '../../BreadcrumbCustom';
import intl from 'react-intl-universal';
import CustimizeModal from '../../common/CustimizeModal';

const Option = Select.Option;

class FacilitiesManagement extends Component {
    state = {
        page: 1,
        pageSize: 10,
        queryStr: "",//设备类型名
        cameraTypeId: "",//设备类型id
        online: "",//在线状态
    };

    componentDidMount() {
        const { getFacilitiesList_dispatch, getFacilitiesTypeList_dispatch } = this.props;
        //获取设备列表
        getFacilitiesList_dispatch({
            page: this.state.page,
            pageSize: this.state.pageSize,
            queryStr: this.state.queryStr,
            cameraTypeId: this.state.cameraTypeId,
            online: this.state.online,
        });
        //获取设备类型列表
        getFacilitiesTypeList_dispatch({
            page: 1,
            pageSize: 100,
        })

    }
    //改变名称
    handleSearchName = (e) => {
        this.setState({
            queryStr: e.target.value
        })
    }
    //改变设备类型
    handleFacilityTypeChange = (value, option) => {
        this.setState({
            cameraTypeId: option.props.id
        })
    }
    //改变在线状态
    handleOnlineStatusChange = (value) => {
        this.setState({
            online: Number(value)
        })
    }

    //查询
    searchButton = () => {
        const {
            page,
            pageSize,
            queryStr,
            cameraTypeId,
            online
        } = this.state;
        const { getFacilitiesList_dispatch } = this.props;
        getFacilitiesList_dispatch({
            page,
            pageSize,
            queryStr,
            cameraTypeId,
            online
        });

    }

    render() {
        const searchStyle = {
            width: 200,
            marginRight: 20,
            marginBottom: 5
        };

        const columns = [
            { title: '设备名称', dataIndex: 'name', key: 'name' },
            { title: '设备ID', dataIndex: 'cameraId', key: 'cameraId' },
            { title: '设备类型', dataIndex: 'cameraType', key: 'cameraType' },
            { title: '在线状态', dataIndex: 'online', key: 'online' },
            {
                title: '操作', dataIndex: 'operation', key: 'operation', render: (text, record) => (
                    <span>
                        <CustimizeModal
                            operationRequest={this.props.modifyFacilities_dispatch}
                            renderRequest={this.props.getFacilitiesList_dispatch}
                            type={"edit"}
                            btnName={null}
                            formItems={
                                [
                                    {
                                        labelName: "设备名称",
                                        property: "name",
                                        options: {
                                            rules: [
                                                { required: true, message: "设备名称为必填项！" },
                                                {
                                                    max: 20, message: "长度不超过20个字符!",
                                                }
                                            ],
                                            initialValue: record.name
                                        },
                                        content: <Input type="text" />
                                    },
                                    {
                                        labelName: "设备ID",
                                        property: "cameraId",
                                        options: {
                                            rules: [
                                                { required: true, message: "设备ID名称为必填项！" },
                                                {
                                                    max: 30, message: "长度不超过30个字符!",
                                                }
                                            ],
                                            initialValue: record.cameraId
                                        },
                                        content: <Input type="text" />
                                    },
                                    {
                                        labelName: "设备类型",
                                        property: "cameraTypeId",
                                        options: {
                                            rules: [
                                                { required: true, message: "设备类型为必选项！" },
                                            ]
                                        },
                                        content: (
                                            <Select
                                                placeholder="请选择设备类型"
                                            >
                                                {
                                                    this.props.facilitiesTypeList.map(item =>
                                                        <Option
                                                            value={item.key}
                                                            key={item.key}
                                                        >
                                                            {item.name}
                                                        </Option>)
                                                }
                                            </Select>
                                        )
                                    },
                                    {
                                        labelName: "地理位置信息",
                                        property: "geoString",
                                        options: {
                                            rules: [
                                                {
                                                    max: 30, message: "长度不超过30个字符!",
                                                }
                                            ],
                                            initialValue: record.geoString
                                        },
                                        content: <Input type="text" />
                                    },
                                    {
                                        labelName: "详细地址",
                                        property: "addr",
                                        options: {
                                            rules: [
                                                {
                                                    max: 40, message: "长度不超过40个字符!",
                                                }
                                            ],
                                            initialValue: record.addr
                                        },
                                        content: <Input type="text" />
                                    }
                                ]
                            }
                            operationRequestParams={{ id: record.id }}
                            renderRequestParams={{
                                page: this.state.page,
                                pageSize: this.state.pageSize,
                                queryStr: this.state.queryStr,
                                cameraTypeId: this.state.cameraTypeId,
                                online: this.state.online,
                            }}
                            setFieldsValueObj={{ cameraTypeId: record.cameraTypeId }}
                            title={"编辑设备"}
                            discription={""}
                        />
                        <CustimizeModal
                            operationRequest={this.props.deleteFacilities_dispatch}
                            renderRequest={this.props.getFacilitiesList_dispatch}
                            type={"delete"}
                            btnName={null}
                            formItems={
                                []
                            }
                            operationRequestParams={{ id: record.key }}
                            renderRequestParams={{
                                page: this.state.page,
                                pageSize: this.state.pageSize,
                                queryStr: this.state.queryStr,
                                cameraTypeId: this.state.cameraTypeId,
                                online: this.state.online,
                            }}
                            title={"删除设备"}
                            discription={"确认删除么？"}
                        />
                    </span>
                )
            }
        ];

        return (
            <div className="gutter-example">
                <BreadcrumbCustom first="基础信息管理" second="设备管理" />
                <Row gutter={16} className="gutter-row">
                    <Col md={24}>
                        <div className="gutter-box" style={{ marginLeft: "34px" }}>
                            <Input
                                placeholder="请输入设备名称或ID"
                                onChange={this.handleSearchName}
                                style={searchStyle}
                            />
                            <Select
                                placeholder="请选择设备类型"
                                onChange={this.handleFacilityTypeChange}
                                style={searchStyle}
                            >
                                {
                                    this.props.facilitiesTypeList.map(item =>
                                        <Option
                                            key={item.name}
                                            id={item.key}
                                        >
                                            {item.name}
                                        </Option>)
                                }
                            </Select>

                            <Select
                                showSearch
                                style={searchStyle}
                                placeholder="请选择在线状态"
                                onChange={this.handleOnlineStatusChange}
                            >
                                <Option value="4">异常</Option>
                                <Option value="5">在线</Option>
                                <Option value="6">断线</Option>
                            </Select>

                            <Button onClick={this.searchButton} type="primary"><Icon type="search" />查询</Button>
                        </div>
                    </Col>
                </Row>
                <Row gutter={16}>
                    <Col className="gutter-row" md={24}>
                        <div className="gutter-box">
                            <Card bordered={false}>
                                <CustimizeModal
                                    styleObj={{
                                        display: "inline-block",
                                        marginRight: "10px",
                                        marginBottom: "10px"
                                    }}
                                    operationRequest={this.props.addFacilities_dispatch}
                                    renderRequest={this.props.getFacilitiesList_dispatch}
                                    type={"plus"}
                                    btnName={"新增设备"}
                                    formItems={
                                        [
                                            {
                                                labelName: "设备名称",
                                                property: "name",
                                                options: {
                                                    rules: [
                                                        { required: true, message: "设备名称为必填项！" },
                                                        {
                                                            max: 20, message: "长度不超过20个字符!",
                                                        }
                                                    ]
                                                },
                                                content: <Input type="text" />
                                            },
                                            {
                                                labelName: "设备ID",
                                                property: "cameraId",
                                                options: {
                                                    rules: [
                                                        { required: true, message: "设备ID名称为必填项！" },
                                                        {
                                                            max: 30, message: "长度不超过30个字符!",
                                                        }
                                                    ]
                                                },
                                                content: <Input type="text" />
                                            },
                                            {
                                                labelName: "设备类型",
                                                property: "cameraTypeId",
                                                options: {
                                                    rules: [
                                                        { required: true, message: "设备类型为必选项！" }
                                                    ]
                                                },
                                                content: (
                                                    <Select
                                                        placeholder="请选择设备类型"
                                                    >
                                                        {
                                                            this.props.facilitiesTypeList.map(item =>
                                                                <Option
                                                                    key={item.key}
                                                                    value={item.key}
                                                                >
                                                                    {item.name}
                                                                </Option>)
                                                        }
                                                    </Select>
                                                )
                                            },
                                            {
                                                labelName: "摄像头ip",
                                                property: "uri",
                                                options: {
                                                    rules: [
                                                        {
                                                            required: true,
                                                            message: "摄像头ip为必填项！"
                                                        },
                                                        {
                                                            max: 15,
                                                            message: "长度不超过15个字符!",
                                                        }
                                                    ]
                                                },
                                                content: <Input type="text" />
                                            },
                                            {
                                                labelName: "端口",
                                                property: "port",
                                                options: {
                                                    rules: [
                                                        {
                                                            required: true,
                                                            message: "端口为必填项！"
                                                        },
                                                        {
                                                            max: 15,
                                                            message: "长度不超过15个字符!",
                                                        }
                                                    ]
                                                },
                                                content: <Input type="text" />
                                            },
                                            {
                                                labelName: "地理位置信息",
                                                property: "geoString",
                                                options: {
                                                    rules: [
                                                        {
                                                            max: 30, message: "长度不超过30个字符!",
                                                        }
                                                    ]
                                                },
                                                content: <Input type="text" />
                                            },
                                            {
                                                labelName: "详细地址",
                                                property: "addr",
                                                options: {
                                                    rules: [
                                                        {
                                                            max: 40, message: "长度不超过40个字符!",
                                                        }
                                                    ]
                                                },
                                                content: <Input type="text" />
                                            }
                                        ]
                                    }
                                    operationRequestParams={{}}
                                    renderRequestParams={{
                                        page: this.state.page,
                                        pageSize: this.state.pageSize,
                                        queryStr: this.state.queryStr,
                                        cameraTypeId: this.state.cameraTypeId,
                                        online: this.state.online,
                                    }}
                                    title={"新增设备"}
                                    discription={""}
                                />
                                <Spin
                                    size="large"
                                    spinning={this.props.spinning}
                                >
                                    <Table
                                        bordered
                                        columns={columns}
                                        dataSource={this.props.facilitiesList}
                                    />
                                </Spin>
                            </Card>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }
}

export default FacilitiesManagement;
