import React, { Component } from "react";
import { Table, Input, Card, Spin } from "antd";
import CustimizeModal from "../../common/CustimizeModal";
import BreadcrumbCustom from '../../BreadcrumbCustom';
import intl from "react-intl-universal";

class FacilitiesTypeManagement extends Component {

    state = {
        page: 1,
        pageSize: 10
    }
    componentDidMount() {
        const { getFacilitiesTypeList_dispatch } = this.props;
        const requestData = {
            page: this.state.page,
            pageSize: this.state.pageSize
        }
        getFacilitiesTypeList_dispatch(requestData);
    }


    renderOperation = (text, record) => (
        <div className="modal-button">
            <CustimizeModal
                operationRequest={this.props.modifyFacilitiesType_dispatch}
                renderRequest={this.props.getFacilitiesTypeList_dispatch}
                type={"edit"}
                btnName={null}
                formItems={[
                    {
                        labelName: "设备类型名称",
                        property: "name",
                        options: {
                            rules: [
                                { required: true, message: "设备类型名称为必填项！" },
                                {
                                    max: 15, message: "长度不超过15个字符!",
                                }
                            ],
                            initialValue: record.name
                        },
                        content: <Input type="text" />
                    },
                    {
                        labelName: "告警阈值",
                        property: "threshold",
                        options: {
                            rules: [
                                { required: true, message: "告警阈值为必填项！" },
                                {
                                    pattern: /^[8-9]{1}[0-9]{1}$|^100$/,
                                    message: '请填写80-100的整数！'
                                }
                            ],
                            initialValue: record.threshold
                        },
                        content: <Input type="text" />
                    }
                ]}
                operationRequestParams={{ id: record.id }}
                renderRequestParams={{
                    page: this.state.page,
                    pageSize: this.state.pageSize
                }}
                title={"编辑设备类型"}
                discription={""}
            />
            <CustimizeModal
                operationRequest={this.props.deleteFacilitiesType_dispatch}
                renderRequest={this.props.getFacilitiesTypeList_dispatch}
                type={"delete"}
                btnName={null}
                formItems={[]}
                operationRequestParams={{ id: record.id }}
                renderRequestParams={{
                    page: this.state.page,
                    pageSize: this.state.pageSize
                }}
                title={"删除设备类型"}
                discription={"确认删除吗？"}
            />
        </div>
    )

    render() {
        const columns = [
            {
                title: "类型名称",
                dataIndex: "name",
                key: "name"
            },
            {
                title: "阈值",
                dataIndex: "threshold",
                key: "threshold",
                width: "25%",
            },
            {
                title: "操作",
                dataIndex: "operation",
                key: "operation",
                width: "15%",
                render: (text, record) => (
                    this.renderOperation(text, record)
                )
            }
        ];
        return (
            <div>
                <BreadcrumbCustom first="基本信息管理" second="设备类型管理" />
                <Card>
                    <CustimizeModal
                        styleObj={{
                            display: "inline-block",
                            marginRight: "10px",
                            marginBottom: "10px"
                        }}
                        operationRequest={this.props.addFacilitiesType_dispatch}
                        renderRequest={this.props.getFacilitiesTypeList_dispatch}
                        type={"plus"}
                        btnName={"新增设备类型"}
                        formItems={[
                            {
                                labelName: "设备类型名称",
                                property: "name",
                                options: {
                                    rules: [
                                        { required: true, message: "设备类型名称为必填项！" },
                                        {
                                            max: 15, message: "长度不超过15个字符!",
                                        }
                                    ],
                                    initialValue: ""
                                },
                                content: <Input type="text" />
                            },
                            {
                                labelName: "告警阈值",
                                property: "threshold",
                                options: {
                                    rules: [
                                        { required: true, message: "告警阈值为必填项！" },
                                        {
                                            pattern: /^[8-9]{1}[0-9]{1}$|^100$/,
                                            message: '请填写80-100的整数！'
                                        }
                                    ],
                                    initialValue: ""
                                },
                                content: <Input type="text" />
                            }
                        ]}
                        operationRequestParams={{ parentId: 0 }}
                        renderRequestParams={{
                            page: this.state.page,
                            pageSize: this.state.pageSize
                        }}
                        title={"新增设备类型"}
                        discription={""}
                    />
                    <Spin
                        size="large"
                        spinning={this.props.spinning}
                    >
                        <Table
                            bordered
                            columns={columns}
                            dataSource={this.props.facilitiesTypeList}
                        />
                    </Spin>
                </Card>
            </div>
        );
    }
}



export default FacilitiesTypeManagement;
