import React, { Component } from "react";
import { Table, Input, Card, Spin } from "antd";
import BreadcrumbCustom from '../../BreadcrumbCustom';
import intl from "react-intl-universal";
import CustimizeModal from '../../common/CustimizeModal';


class ClassesManagement extends Component {

    componentDidMount() {
        const { getGradeTreeList_dispatch } = this.props;
        getGradeTreeList_dispatch();
    }


    renderOperation = (text, record) => (
        <div>
            {
                record.type === 2
                    ?
                    <div className="grade_modal-button">
                        <CustimizeModal
                            operationRequest={this.props.addGradeOrClass_dispatch}
                            renderRequest={this.props.getGradeTreeList_dispatch}
                            type={"plus"}
                            btnName={null}
                            formItems={[
                                {
                                    labelName: "班级名称",
                                    property: "name",
                                    options: {
                                        rules: [{ required: true, message: "名称格式不符合规范!", max: 15 }],
                                        initialValue: ""
                                    },
                                    content: <Input type="text" />
                                }
                            ]}
                            operationRequestParams={{ parentId: record.key, type: 3 }}
                            title={"新增班级"}
                            discription={""}
                        />
                        <CustimizeModal
                            operationRequest={this.props.editGradeOrClass_dispatch}
                            renderRequest={this.props.getGradeTreeList_dispatch}
                            type={"edit"}
                            btnName={null}
                            formItems={
                                [
                                    {
                                        labelName: "年级名称",
                                        property: "name",
                                        options: {
                                            rules: [{ required: true, message: "名称格式不符合规范!", max: 15 }],
                                            initialValue: record.name
                                        },
                                        content: <Input type="text" />
                                    },
                                    {
                                        labelName: "跨级时间",
                                        property: "crossLevelTime",
                                        options: {
                                            rules: [
                                                { required: true, message: "名字格式不符合规范!", max: 5 },
                                                {
                                                    pattern: /^\d{2}-\d{2}$/,
                                                    message: '日期格式为MM-DD(例如：9月9日应写为09-09)'
                                                }
                                            ],
                                            initialValue: record.crossLevelTime
                                        },
                                        content: <Input type="text" />
                                    },
                                    {
                                        labelName: "下一级名称",
                                        property: "nextLevelName",
                                        options: {
                                            rules: [{ required: true, message: "名字格式不符合规范!", max: 15 }],
                                            initialValue: record.nextLevelName
                                        },
                                        content: <Input type="text" />
                                    }
                                ]
                            }
                            operationRequestParams={{ id: record.key, type: 2, parentId: record.parentId }}
                            title={"编辑年级"}
                            discription={""}
                        />
                        <CustimizeModal
                            operationRequest={this.props.deleteGradeOrClass_dispatch}
                            renderRequest={this.props.getGradeTreeList_dispatch}
                            type={"delete"}
                            btnName={null}
                            formItems={
                                []
                            }
                            operationRequestParams={{ id: record.key }}
                            title={"删除年级"}
                            discription={"确认删除么？"}
                        />
                    </div>
                    :
                    <div className="class_modal-button">
                        <CustimizeModal
                            operationRequest={this.props.editGradeOrClass_dispatch}
                            renderRequest={this.props.getGradeTreeList_dispatch}
                            type={"edit"}
                            btnName={null}
                            formItems={
                                [
                                    {
                                        labelName: "班级名称",
                                        property: "name",
                                        options: {
                                            rules: [{ required: true, message: "名称格式不符合规范!", max: 15 }],
                                            initialValue: record.name
                                        },
                                        content: <Input type="text" />
                                    }
                                ]
                            }
                            operationRequestParams={{ id: record.key, type: 3, parentId: record.parentId }}
                            title={"编辑班级"}
                            discription={
                                <div className="form-content-tile">
                                    <span>所在年级: </span>
                                    <span>{record.parentName}</span>
                                </div>
                            }
                        />
                        <CustimizeModal
                            operationRequest={this.props.deleteGradeOrClass_dispatch}
                            renderRequest={this.props.getGradeTreeList_dispatch}
                            type={"delete"}
                            btnName={null}
                            formItems={
                                []
                            }
                            operationRequestParams={{ id: record.key }}
                            title={"删除班级"}
                            discription={"确认删除么？"}
                        />
                    </div>
            }
        </div>

    )

    render() {
        const columns = [
            {
                title: "名称",
                dataIndex: "name",
                key: "name"
            },
            {
                title: "操作",
                dataIndex: "operation",
                key: "operation",
                width: "25%",
                render: (text, record) => (
                    this.renderOperation(text, record)
                )
            }
        ];
        return (
            <div>
                <BreadcrumbCustom first="基本信息管理" second="班级管理" />
                <Card>
                    <CustimizeModal
                        styleObj={{
                            display: "inline-block",
                            marginRight: "10px",
                            marginBottom: "10px"
                        }}
                        operationRequest={this.props.addGradeOrClass_dispatch}
                        renderRequest={this.props.getGradeTreeList_dispatch}
                        type={"plus"}
                        btnName={"新增年级"}
                        formItems={[
                            {
                                labelName: "年级名称",
                                property: "name",
                                options: {
                                    rules: [{ required: true, message: "名称格式不符合规范!", max: 15 }]
                                },
                                content: <Input type="text" />
                            },
                            {
                                labelName: "跨级时间",
                                property: "crossLevelTime",
                                options: {
                                    rules: [
                                        { required: true, message: "名字格式不符合规范!", max: 5 },
                                        {
                                            pattern: /^\d{2}-\d{2}$/,
                                            message: '日期格式为MM-DD(例如：9月9日应写为09-09)'
                                        }
                                    ]
                                },
                                content: <Input type="text" />
                            },
                            {
                                labelName: "下一级名称",
                                property: "nextLevelName",
                                options: {
                                    rules: [{ required: true, message: "名字格式不符合规范!", max: 15 }]
                                },
                                content: <Input type="text" />
                            }
                        ]}
                        operationRequestParams={{ parentId: 0, type: 2 }}
                        title={"新增年级"}
                        discription={""}
                    />
                    <Spin
                        size="large"
                        spinning={this.props.spinning}
                    >
                        <Table
                            columns={columns}
                            bordered
                            dataSource={this.props.gradeTreeList}
                            scroll={{ x: '100%', y: 550 }}
                            pagination={false}
                        />
                    </Spin>
                </Card>
            </div>
        );
    }
}



export default ClassesManagement;
