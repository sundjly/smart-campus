import React from 'react';
import BreadcrumbCustom from '../BreadcrumbCustom';
import { Card, Form, Row, Col, Checkbox, Input } from 'antd';
import UitestTp from './systemModular/UitestTP';
import { api } from '../../constants/API';
import request from '@/utils/request';

class MessageTemplates extends React.Component {
    state = {
        tplList: [],
    };

    componentDidMount() {
        console.log(this.props);
        request({
            url: api.getMesgTplList,
            method: 'POST',
            data: { page: 1, pageSize: 10 }
        }).then(
            res => {
                let tplList = [];
                console.log("getMesgTplList: ", res);
                if (res && res.data) {
                    let tplItem;
                    for (let i = 0; i < res.data.length; i++) {
                        tplItem = res.data[i];
                        if(!tplItem.name || tplItem.name==null){
                            continue;
                        }
                        tplList.push(<UitestTp form={this.props.form} key={tplItem.id} lister={["短信", "微信"]} tplItem={tplItem}/>)
                    }
                }
                this.setState({ tplList: tplList });
            });
    }

    render() {

        return (
            <div>
                <BreadcrumbCustom first="系统设置" second="消息模板" />
                {this.state.tplList}
            </div>
        )
    };
};
const MessageTemplate = Form.create()(MessageTemplates);

export default MessageTemplate;