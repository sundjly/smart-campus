import React, { Component } from 'react';
import { Card, Row, Col, Checkbox, Radio, Input, Button, Form,message } from 'antd';
import { api } from '../../../constants/API';
import request from '@/utils/request';

const { TextArea } = Input;
const RadioGroup = Radio.Group;
const FormItem = Form.Item;
const success = (txt) => {
    message.success(txt, 2);
};

const error = (txt) => {
    message.error(txt, 2);
};

class UitestTp extends Component {

    constructor(props) {
        super(props);
        this.state = {
            tplItem: props.tplItem,
        }
        console.log(props);
    }

    typeOptions = [
        { label: '微信', value: 1 },
        { label: '短信', value: 2 }
    ];

    onChange = (e) => {
        console.log(`checked = ${e.target.checked}`);
    }

    typeChange = (e) => {
        this.setState({
            tplItem: {
                ...this.state.tplItem,
                type: e.target.value
            }
        });
    }

    contentChange = (e) => {
        this.setState({
            tplItem: {
                ...this.state.tplItem,
                content: e.target.value
            }
        });
    }

    save = () => {
        this.setState({ loading: true });
        // console.log("MsgTpl - data: ", this.state.tplItem);
        let updateData = {
            id:this.state.tplItem.id,
            name: this.state.tplItem.name,
            type: this.state.tplItem.type,
            content: this.state.tplItem.content
        }
        request({
            url: api.updateMesgTpl,
            method: "PUT",
            data: updateData,
        }).then(res => {
            console.log(res);
            if (res.errCode === 0) {
                this.setState({ loading: false });
                success("保存成功")
            } else {
                error(res.data)
            }
        });
    }

    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <div>
                <Row gutter={16}>
                    <Col className="gutter-row" md={14}>
                        <div className="gutter-box">
                            <Card title={this.state.tplItem.name} bordered={false}>
                                <Row span={24} className="TP-title">
                                    <Col offset={1} span={2}>推送方式</Col>
                                    <RadioGroup options={this.typeOptions} onChange={this.typeChange} defaultValue={this.state.tplItem.type} value={this.state.tplItem.type} />
                                </Row>
                                <Row className="TP-body" span={24}>
                                    <Col offset={1} span={2}>推送说明</Col>
                                    <Col span={10}>
                                        <TextArea ref="tplContent" rows={10} defaultValue={this.state.tplItem.content} value={this.state.tplItem.content} onChange={this.contentChange} /></Col>
                                    <Col className="Preservation" offset={1} span={2}><Button type="primary" onClick={this.save}>保存</Button></Col>
                                </Row>
                            </Card>
                        </div>
                    </Col>
                </Row>
                <style>{
                    `
                     .TP-title{
                         padding-bottom:25px;
                     }
                     .TP-body [class^="ant-col-"] {
                         height:190px;
                     }
                     .Preservation{
                         display:flex;
                         flex-direction:column-reverse;
                     }`
                }</style>
            </div>
        )
    };
};
export default UitestTp;