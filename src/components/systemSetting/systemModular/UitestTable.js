import React, { Component } from 'react';
import { Table } from 'antd';

Date.prototype.format = function(format){
    var o = {
        "M+" : this.getMonth()+1, //month
        "d+" : this.getDate(), //day
        "h+" : this.getHours(), //hour
        "m+" : this.getMinutes(), //minute
        "s+" : this.getSeconds(), //second
        "q+" : Math.floor((this.getMonth()+3)/3), //quarter
        "S" : this.getMilliseconds() //millisecond
    }
    if(/(y+)/.test(format)) {
        format = format.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));
    }
    for(var k in o) {
        if(new RegExp("("+ k +")").test(format)) {
            format = format.replace(RegExp.$1, RegExp.$1.length==1 ? o[k] : ("00"+ o[k]).substr((""+ o[k]).length));
        }
    }
    return format;   
} 
class UitestTable extends Component {
  state = {
    data : []
  };
  render() {
    const columns = [{
      title: '账户名',
      dataIndex: 'userId',
      align: 'center'
    }, {
      title: '姓名',
      className: 'column-money',
      dataIndex: 'userName',
      align: 'center'
    }, {
      title: '角色',
      dataIndex: 'organization',
      align: 'center'
    }, {
      title: '操作平台',
      className: 'column-money',
      dataIndex: 'terminalId',
      align: 'center'
    }, {
      title: '操作时间',
      className: 'column-money',
      dataIndex: 'insertTime',
      render:(text)=>(
        new Date(text).format("yyyy-MM-dd hh:mm:ss")
      ),
      align: 'center'
    }, {
      title: '操作描述',
      className: 'column-money',
      dataIndex: 'operateName',
      align: 'center'
    },];


    return (


      <Table style={{ background: '#fff' }}
        pagination={false}
        columns={columns}
        dataSource={this.props.data}
        bordered
      />

    );
  }
};

export default UitestTable;