import React,{Component} from 'react';
import moment from 'moment';
import 'moment/locale/zh-cn';
import {Row, Col, Icon, Input, Select, Button,DatePicker,Pagination } from 'antd';
import BreadcrumbCustom from '../BreadcrumbCustom';
import UitestTable from './systemModular/UitestTable';
import request from '../../utils/request';
import { api } from '../../constants/API';

moment.locale('zh-cn');
const Option = Select.Option;

Date.prototype.format = function(format){
    var o = {
        "M+" : this.getMonth()+1, //month
        "d+" : this.getDate(), //day
        "h+" : this.getHours(), //hour
        "m+" : this.getMinutes(), //minute
        "s+" : this.getSeconds(), //second
        "q+" : Math.floor((this.getMonth()+3)/3), //quarter
        "S" : this.getMilliseconds() //millisecond
    }
    if(/(y+)/.test(format)) {
        format = format.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));
    }
    for(var k in o) {
        
        if(new RegExp("("+ k +")").test(format)) {
            format = format.replace(RegExp.$1, RegExp.$1.length==1 ? o[k] : ("00"+ o[k]).substr((""+ o[k]).length));
        }

    }
    return format;   
}

class OperationLog extends Component{
  state = {
      startValue: null,
      endValue: null,
      endOpen: false,
      data:[],
      total:0,
      page:1,
      pageSize:10,
      roleList:[],
      operateName:"",
      userName:"",
    };
  componentDidMount(){
    this.getLogList(1,10)
    this.getRoleList()
  }
  handleChange = (value) => {
    console.log(`selected ${value}`);
  }
  handleFocus = () => {

  }
  handleBlur = () => {

  }

  addAccount = () => {

  }
  getRoleList = () => {
      request({
        url:api.roleList,
        method:'post',
        data:{page:1,pageSize:10000}
      }).then(res => {
        console.log("wl getRoleList",res);
        if(res.errCode===0&&res.data&&res.data.length>0){
          this.setState({roleList:res.data});
        }
      })
  }

  //查询日志。  
  getLogList = () => {
    const startTime = this.state.startValue?new Date(this.state.startValue).format("yyyy-MM-dd hh:mm:ss"):this.state.startValue
    const endTime = this.state.endValue?new Date(this.state.endValue).format("yyyy-MM-dd hh:mm:ss"):this.state.endValue
    request({
      url:api.logList,
      method:'POST',
      data:{
        page:this.state.page,
        pageSize:this.state.pageSize,
        userId:this.state.userId,
        // userName:this.state.userName,
        operateName:this.state.operateName,
        organization:this.state.organization,
        terminalId:this.state.terminalId,
        startTime:startTime,
        endTime:endTime,
      }
    }).then(res => {
      console.log("wl getLogList",res);
      if(res.errCode===0&&res.data){
        this.setState({data:res.data,total:res.total});
      }
    })
  }

  disabledStartDate = (startValue) => {
    const endValue = this.state.endValue;
    if (!startValue || !endValue) {
      return false;
    }
    return startValue.valueOf() > endValue.valueOf();
  }

  disabledEndDate = (endValue) => {
    const startValue = this.state.startValue;
    if (!endValue || !startValue) {
      return false;
    }
    return endValue.valueOf() <= startValue.valueOf();
  }
  pageChange = (page,pageSize) => {
    this.setState({
      page:page,
      pageSize:pageSize
    },() => {
      this.getLogList()
    })
  }
  onChange = (field, value) => {
    this.setState({
      [field]: value,
    });
  }

  onStartChange = (value) => {
    this.onChange('startValue', value);
  }

  onEndChange = (value) => {
    this.onChange('endValue', value);
  }

  handleStartOpenChange = (open) => {
    if (!open) {
      this.setState({ endOpen: true });
    }
  }

  handleEndOpenChange = (open) => {
    this.setState({ endOpen: open });
  }
  onSelectChange = (value) =>{
    console.log("onSelectChange",value)
    this.setState({terminalId: value});
  }
  // userNameChange = (e) => {
  //   console.log("userNameChange", e.target.value);
  //   this.setState({ userName: e.target.value });
  // }
  userIdChange = (e) => {
    this.setState({ userId: e.target.value });
  }
  operateNameChange = (e) => {
    this.setState({ operateName: e.target.value });
  }
  render(){
      const inputStyle = {
          width: 140,
          marginRight: 20
        };
      const { startValue, endValue, endOpen } = this.state;
      const options = this.state.roleList.map( d => <Option key={d.id} value={d.id}>{d.name}</Option>)
      return(
          <div>
                <BreadcrumbCustom first="系统设置" second="操作日志" />
                <Row gutter={24}>
                  <Col className="gutter-row" md={24}>
                      <div className="gutter-box">
                      <Input
                          onChange={this.userIdChange}
                          placeholder="请输入帐号名"                           
                          style={inputStyle}
                      />
                      <Input
                          onChange={this.operateNameChange}
                          placeholder="请输入操作关键字"
                          style={inputStyle}
                      />
                      <Select 
                        placeholder="角色"
                        style={{ width: 120,marginRight:20 }} onChange={this.onSelectChange}>
                        <Option value="">全部</Option>
                        {options}
                      </Select>
                      <DatePicker
                          disabledDate={this.disabledStartDate}
                          showTime
                          format="YYYY-MM-DD HH:mm:ss"
                          value={startValue}
                          placeholder="选择开始时间"
                          onChange={this.onStartChange}
                          onOpenChange={this.handleStartOpenChange}
                      />
                      <span style={{paddingLeft:7,paddingRight:7}}>至</span>
                          <DatePicker style={{marginRight:20}}
                          disabledDate={this.disabledEndDate}
                          showTime
                          format="YYYY-MM-DD HH:mm:ss"
                          value={endValue}
                          placeholder="选择结束时间"
                          onChange={this.onEndChange}
                          open={endOpen}
                          onOpenChange={this.handleEndOpenChange}
                          />
                      <Button onClick={this.getLogList}><Icon type="search" />查询</Button>
                      </div>
                  </Col>
                </Row>
                <Row gutter={24} style={{margin: '12px 0'}}>
                  <UitestTable  data={this.state.data}/>
                </Row>
                <Row type="flex" justify="end">
                  <Pagination showSizeChanger hideOnSinglePage current={this.state.page} pageSize={this.state.pageSize} onChange={this.pageChange} onShowSizeChange={this.pageChange} defaultCurrent={1} total={this.state.total} />
                </Row>     
          </div>
      )
  };
};

export default OperationLog;