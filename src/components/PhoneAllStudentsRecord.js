import React, {Component} from 'react';
import {Spin, message, Menu, Icon, Input, Button, Modal} from 'antd';
import request from '@/utils/request';
import './PhoneAllStudentsRecord.less';
import moment from 'moment';
// import Buttons from "./systemAccount/RoleManagement";
import {api} from '../constants/API';
import PropTypes from 'prop-types';
import $ from 'jquery';
const ExampleFace = 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png';
const Search = Input.Search;
class PhoneStudentsRecord extends Component {

  state={
    resData: [],
    page:1,
    pageSize:50,
    current: '0',
    visible: false,
    status: {
      absent: 0,
      arrive: 0,
      leave: 0,
      timeOff: 0
    },
    changeStudentId:{
      id: 0,
      teacherId : 0
    },
    realName: '',// 搜索存储的名字
    visibleLoading: false,
  }

  componentWillMount() {
    this.gerRequestList();
  }
  componentDidMount(){
    $('.ant-layout-content').addClass('ant-layout-content-phone');
    $('.ant-layout-header').hide();
  }
  componentWillUnmount () {
    $('.ant-layout-content').removeClass('ant-layout-content-phone');
    $('.ant-layout-header').show();
  }

  handleClick = (e) => {
    console.log('click ', e);
    this.setState({
      current: e.key,
    });
    this.gerRequestList(e.key);
  }

  changeSearchName= val => {
    console.log('sdj',val)
    const realName = val?val.trim():'';
    this.setState({
      realName: realName,
    })
    this.gerRequestList(this.state.current,1,50,realName);
  }

  gerRequestList = (cState = '0', page=1, pageSize=50,realName = this.state.realName) => {
    let data={
      cState,
      page,
      pageSize,
      realName
    };
    // if(cState ==='all'){
    //   data={
    //     page,
    //     pageSize,
    //     realName
    //   }
    // }else{
    //   data = {
    //     cState,
    //     page,
    //     pageSize
    //   }
    // };

    this.setState({
      visibleLoading: true
    })
    return request({
      url:'/api/intellif/zhxy/student/attendance/record',
      method:'post',
      data:{
        ...data
      }
    }).then(res => {
      if(res.errCode ===0){
        console.log('sdj', res)
        this.setState({
          // resData: [...this.state.resData,...res.data.detail],
          resData: [...res.data.detail],
          status: {
            absent: res.data.total[0],
            arrive: res.data.total[1],
            leave: res.data.total[2],
            timeOff: res.data.total[3]
          }
        })
      }else{
        message.error('数据获取失败')
      }
      this.setState({
        visibleLoading: false
      })
    })
  }

  changeStatus = id => {
    console.log('sdj', id)
  }

  handleCancel = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  }

  getList = ({calendar, realName, aTime,photoData, pId, teacherId, className, updated}) => {
    //calendar "2018-05-11"
    let calendarArr = calendar? calendar.split('-'):['','','暂无'];
    let time = aTime?moment(new Date(aTime)).format('YYYY-MM-DD HH:mm:ss'):''
    let hour = '没有时间';
    if(time !== ''){
      hour = time.split(' ')[1];
    }
    let updateTime = updated?moment(new Date(updated)).format('MM-DD HH:mm:ss'):''
    let buttonHandle = '';
    switch(this.state.current){
      case '0': buttonHandle='未到校';
        break;
      case '1': buttonHandle='已到校';
        break;
      case '2': buttonHandle='已离校';
        break;
      case '3': buttonHandle='请假';
        break;
      default:buttonHandle='weizhi';
        break;
    }

    return (
      <div onClick={this.changePhone.bind(this, pId, teacherId)}>
        <section className={"single-info"}>
          <img src={photoData} alt={""} />
          <div className={"app-content-name"}>
            <p>{realName}</p>
            <p>{updateTime}</p>
          </div>
          <div className={"app-handle"}>
            <Button
              data-key="handle"
              // onClick={this.changeStatus.bind(this, pId)}
            >
              {buttonHandle}<Icon data-key="handle" type="edit" />
            </Button>
          </div>
        </section>
        <span className={"white-space-2"}></span>
      </div>
    )
  }

  changePhone = (id,teacherId, e) =>{
    console.log('sdj changePhone', e , id)
    if(e.target['type'] && e.target['type']=='button'){
      this.setState({
        visible: true,
        changeStudentId: {
          id,
          teacherId
        }
      })
    }else{
      this.props.history.push({
        pathname: `/app/service/map`,
        state:id
      });
    }
  }

  //改变请假模式
  changeModalStatus = e => {
    console.log('sdj changeModalStatus',e.target['innerHTML']);
    let cState='0';
    switch (e.target['innerHTML']){
      case '未到校':
        cState = '0';
        break;
      case '已到校':
        cState = '1';
        break;
      case '已离校':
        cState = '2';
        break;
      case '请假':
        cState = '3';
        break;
      default:
        break;
    }
    const studentId = this.state.changeStudentId.id;
    return request({
      url:'/api/intellif/zhxy/student/attendance/update',
      method:'put',
      data:{
        cState,
        studentId,
        teacherId: this.state.changeStudentId.teacherId
      }
    }).then(res => {
      if(res.errCode ===0){
        this.setState({
          visible: false,
        });
      }else{
        message.error('出现错误，错误原因为：'+ res.data)
      }
      this.gerRequestList(this.state.current)
    })
  }

  render (){
    console.log('sdj', this.state.resData)
    const resData = this.state.resData;
    const showData = resData.length?
      resData.map((val,index) => {
        return this.getList({...val});
      }) : (
        <div className="center app-center">没有数据信息</div>
      );
    return (
      <div className="app-container">
        <header className="app-header">
          <p>全校出勤概览</p>
          <Search
            className="app-search"
            placeholder="搜索学生"
            onSearch={this.changeSearchName}
            enterButton
          />
        </header>
        <Menu
          onClick={this.handleClick}
          selectedKeys={[this.state.current]}
          mode="horizontal"
        >
          <Menu.Item key='0'>未到校({this.state.status.absent})</Menu.Item>
          <Menu.Item key='1' >已到校({this.state.status.arrive})</Menu.Item>
          <Menu.Item key='2' >已离校({this.state.status.leave})</Menu.Item>
          <Menu.Item key='3' >请假({this.state.status.timeOff})</Menu.Item>
        </Menu>
        <span className={"white-space-11"}></span>
        <div className={"app-content"}>

          {showData}

          {/*<div>*/}
            {/*<section className={"single-info"}>*/}
              {/*<img src={ExampleFace} alt={""} />*/}
              {/*<div className={"app-content-name"}>*/}
                {/*<p>阿兰</p>*/}
                {/*<p>美术课</p>*/}
              {/*</div>*/}
              {/*<div className={"app-handle"}>*/}
                {/*<Button>*/}
                  {/*未到校<Icon type="edit" />*/}
                {/*</Button>*/}
              {/*</div>*/}
            {/*</section>*/}
            {/*<span className={"white-space-2"}></span>*/}
          {/*</div>*/}

        </div>
        <Modal
          title={'更改到校记录'}
          style={{ top: '25%' }}
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
          footer={null}
          className={"app-modal"}
        >
          <div
            className={"app-modal-content"}
            onClick={this.changeModalStatus}
          >
            <p data-key="0" className={this.state.current == '0'? 'modal-checked-active':''}>未到校</p>
            <p data-key="1" className={this.state.current == '1'? 'modal-checked-active':''}>已到校</p>
            <p data-key="2"className={this.state.current == '2'? 'modal-checked-active':''}>已离校</p>
            <p data-key="3" className={this.state.current == '3'? 'modal-checked-active':''}>请假</p>
          </div>
        </Modal>
        <Spin
          className="center spin-center"
          indicator={<Icon type="loading" style={{ fontSize: 45 }} spin />}
          spinning={this.state.visibleLoading}
        />
      </div>
    )
  }
}

export default PhoneStudentsRecord;