import React, { Component } from "react";
import {
  Row,
  Col,
  Icon,
  Input,
  Select,
  Button,
  DatePicker,
  List,
  Pagination,
  Spin,
  Popconfirm
} from "antd";
import BreadcrumbCustom from "../../BreadcrumbCustom";
import CreateBackRoom from "./CreateBackRoom";
import BackRoomDetail from "./BackRoomDetail";

import "../MeetingManagement.less";

const Option = Select.Option;

const testData = [
  {
    created: 1526020851000,
    updated: 1526020851000,
    id: 18,
    name: "寝室1",
    meetTime: 1525968000000,
    timeRange: "2018-05-11 18:48:00~2018-05-11 18:55:00",
    adminIds: "1,2",
    cameraIds: "3,4",
    state: 0,
    personIds: "5,6",
    allCount: 2,
    arrivedCount: null,
    noArrivedCount: 2,
    holidayCount: null,
    isMeetAdmin: 1
  }
];

const meetingListHeader = (
  <div className="meeting-list-header">
    {/* <Row>
      <Col span={5}>寝室名称</Col>
      <Col span={5}>寝室地点</Col>
      <Col span={4}>寝室时间</Col>
      <Col span={3}>寝室状态</Col>
      <Col span={2}>缺会人数</Col>
      <Col span={5}>操作</Col>
    </Row> */}
  </div>
);

class BackRoomManagement extends Component {
  state = {
    activeModal: "cancel",
    activePage: "list",
    listLoading: false,
    isHandleModalShow: false
  };

  getBackRoomState = state => {
    switch (state) {
      case 0:
        return "未开始";
      case 1:
        return "已取消";
      case 2:
        return "进行中";
      case 3:
        return "已结束";
      default:
        return "";
    }
  };

  editBackRoom = item => {
    console.log("edit meeting", item);
    this.setActivePage("create");
  };

  cancelBackRoom = item => {
    console.log("cancel meeeting", item);
  };

  deleteBackRoom = item => {
    console.log("delete meeting", item);
  };

  setActivePage = activePage => {
    console.log(activePage);

    this.setState({
      activePage
    });
  };

  // 操作按钮
  renderHandleButtons = item => {
    switch (item.state) {
      case 0:
        return (
          <div>
            <Button size="small" onClick={this.editBackRoom.bind(this, item)}>
              编辑
            </Button>
            <Popconfirm
              title="确定取消此寝室吗？"
              onConfirm={this.cancelBackRoom}
              okText="确定"
              cancelText="取消"
            >
              <Button
                type="primary"
                size="small"
                onClick={this.setActivePage.bind(this, "detail")}
              >
                查看归寝记录
              </Button>
            </Popconfirm>
            <Popconfirm
              title="确定要删除此寝室吗？"
              onConfirm={this.deleteBackRoom}
              okText="确定"
              cancelText="取消"
            >
              <Button type="danger" size="small">
                删除
              </Button>
            </Popconfirm>
          </div>
        );
      case 1:
        return (
          <div>
            <Popconfirm
              title="确定要删除此寝室吗？"
              onConfirm={this.deleteBackRoom}
              okText="确定"
              cancelText="取消"
            >
              <Button type="danger" size="small">
                删除
              </Button>
            </Popconfirm>
          </div>
        );
      case 2:
        return (
          <div>
            <Popconfirm
              title="确定取消此寝室吗？"
              onConfirm={this.cancelBackRoom}
              okText="确定"
              cancelText="取消"
            >
              <Button
                type="primary"
                size="small"
                onClick={this.cancelBackRoom.bind(this, item)}
              >
                终止
              </Button>
            </Popconfirm>
          </div>
        );
      case 3:
        return "";
      default:
        return "";
    }
  };

  render() {
    const {
      listLoading,
      isHandleModalShow,
      activeModal,
      activePage
    } = this.state;

    return (
      <div className="meeting-manage-wrap">
        <div
          className="meeting-list-page"
          style={{ display: activePage === "list" ? "block" : "none" }}
        >
          <BreadcrumbCustom first="业务管理" second="归寝管理" />
          <Row className="meeting-manage-header">
            <Button
              type="primary"
              onClick={this.setActivePage.bind(this, "create")}
              style={{ marginLeft: "10px" }}
            >
              <Icon type="plus" />新增归寝地点
            </Button>
          </Row>
          <Row className="meeting-manage-content">
            <div className="meeting-list">
              <List
                // header={meetingListHeader}
                bordered
                dataSource={testData}
                renderItem={item => (
                  <List.Item>
                    <Row
                      type="flex"
                      align="middle"
                      justify="start"
                      className="meeting-item"
                    >
                      <Col span={5}>{item.name}</Col>
                      <Col span={4}>{`${item.timeRange.split("~")[0]}~${
                        item.timeRange.split("~")[1].split(" ")[1]
                      }`}</Col>
                      <Col span={3}>{this.getBackRoomState(item.state)}</Col>
                      <Col span={2}>
                        <a onClick={this.setActivePage.bind(this, "detail")}>
                          {item.noArrivedCount}
                        </a>
                      </Col>
                      <Col span={5}>{this.renderHandleButtons(item)}</Col>
                    </Row>
                  </List.Item>
                )}
              />
            </div>
            <Spin className="loading-center zi1060 " spinning={listLoading} />
          </Row>
        </div>
        <div
          className="meeting-create-page"
          style={{ display: activePage === "create" ? "block" : "none" }}
        >
          <BreadcrumbCustom
            first="业务管理"
            second="归寝管理"
            third="新增/修改"
          />
          <CreateBackRoom {...this.props} setActivePage={this.setActivePage} />
        </div>
        <div
          className="meeting-detail-page"
          style={{ display: activePage === "detail" ? "block" : "none" }}
        >
          <BreadcrumbCustom
            first="业务管理"
            second="归寝管理"
            third="归寝记录"
          />
          <BackRoomDetail {...this.props} setActivePage={this.setActivePage} />
        </div>
      </div>
    );
  }
}

export default BackRoomManagement;
