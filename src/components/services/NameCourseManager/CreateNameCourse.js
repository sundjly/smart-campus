import React, { Component } from "react";
import moment from "moment";
import {
  Card,
  Form,
  Select,
  Row,
  Col,
  Button,
  message,
  Spin,
  Icon,
  Input,
  Checkbox,
  Transfer
} from "antd";
import BreadcrumbCustom from "../../BreadcrumbCustom";
import intl from "react-intl-universal";
import CommonModal from "../../common/CommonModal";
import CheckCourseHistory from "./CheckCourseHistory";
import TimeFilter from "../TimeFilter";

import "../InOutSchool.less";

const FormItem = Form.Item;
const Option = Select.Option;

class CreatNameCourse extends Component {
  state = {
    dataSource: [
      {
        key: 1,
        name: "张老师"
      },
      {
        key: 2,
        name: "李三元_1001"
      },
      {
        key: 3,
        name: "王老师"
      },
      {
        key: 4,
        name: "刘老师"
      },
      {
        key: 5,
        name: "赵老师"
      },
      {
        key: 6,
        name: "陈老师"
      },
      {
        key: 7,
        name: "李三圆_1002"
      }
    ],
    spinning: false,
    isHistoryShow: false,
    isDateModalShow: false,
    isCameraModalShow: false,
    deletedDateKey: "",
    deletedCameraId: {
      isInDirection: "",
      id: ""
    },
    newSchollDate: {
      startDate: moment(new Date().getTime() - 1 * 24 * 3600000).format(
        "YYYY-MM-DD"
      ),
      endDate: moment().format("YYYY-MM-DD")
    },
    inCameraId: "",
    outCameraId: "",
    submitLoading: false
  };

  componentWillMount() {
    const { getInOutSchoolOption } = this.props;
    this.setState(
      {
        spinning: true
      },
      () => {
        getInOutSchoolOption().then(() => {
          this.setState({
            spinning: false
          });
        });
      }
    );
  }

  componentDidMount() {
    const { schoolDate, inCameraIds, outCameraIds } = this.props;
    this.props.form.setFields({
      schoolDate: {
        value: schoolDate
      },
      cameraIn: {
        value: inCameraIds
      },
      cameraOut: {
        value: outCameraIds
      }
    });
  }

  handleSubmit(e) {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((errors, values) => {
      console.log("submit option: ", errors, values);
      const {
        setInOutSchoolOption,
        optionId,
        schoolDate,
        inCameraIds,
        outCameraIds
      } = this.props;
      if (errors) {
        return;
      } else {
        this.setState(
          {
            submitLoading: true
          },
          () => {
            setInOutSchoolOption({
              urlParams: {
                id: optionId
              },
              timeRange: schoolDate.join(","),
              aCameraIds: inCameraIds.join(","),
              lCameraIds: outCameraIds.join(",")
            }).then(() => {
              this.setState({
                submitLoading: false
              });
              message.success("提交成功");
            });
          }
        );
      }
    });
  }

  onDeleteSchoolDate = key => {
    console.log("delete school date", key);
    this.setState(
      {
        deletedDateKey: key
      },
      () => {
        this.setState({
          isDateModalShow: true
        });
      }
    );
  };

  onDeleteInOutCamera = (id, isInDirection) => {
    console.log("delete in out camera", id);
    this.setState(
      {
        deletedCameraId: {
          isInDirection,
          id
        }
      },
      () => {
        this.setState({
          isCameraModalShow: true
        });
      }
    );
  };

  onAddSchoolDate = () => {
    const { newSchollDate } = this.state;
    const { schoolDate, updateInOutSchoolOption } = this.props;
    const { startDate, endDate } = newSchollDate;
    if (startDate && endDate) {
      schoolDate.push(`${startDate}~${endDate}`);
      updateInOutSchoolOption({
        schoolDate
      });
    } else {
      message.error(
        intl.get("NOT_CHOOSE_VALUE", {
          value: intl.get("SCHOOL_DATE").toLocaleLowerCase()
        })
      );
    }
  };

  onAddCamera = isInDirection => {
    console.log("add camera", isInDirection);
    const { inCameraId, outCameraId } = this.state;
    const { updateInOutSchoolOption, inCameraIds, outCameraIds } = this.props;
    if (isInDirection) {
      if (inCameraId) {
        inCameraIds.push(inCameraId);
        updateInOutSchoolOption({
          inCameraIds
        });
        this.setState({
          inCameraId: ""
        });
      } else {
        message.error(
          intl.get("NOT_CHOOSE_VALUE", {
            value: intl.get("CAMERA").toLocaleLowerCase()
          })
        );
      }
    } else {
      if (outCameraId) {
        outCameraIds.push(outCameraId);
        updateInOutSchoolOption({
          outCameraIds
        });
        this.setState({
          outCameraId: ""
        });
      } else {
        message.error(
          intl.get("NOT_CHOOSE_VALUE", {
            value: intl.get("CAMERA").toLocaleLowerCase()
          })
        );
      }
    }
  };

  onSelectCamera = (isInDirection, value) => {
    console.log("select camera", value, isInDirection);
    if (isInDirection) {
      this.setState({
        inCameraId: value
      });
    } else {
      this.setState({
        outCameraId: value
      });
    }
  };

  onStartChange = (date, dateString) => {
    console.log("onStartChange", date, dateString);
    const { newSchollDate } = this.state;
    this.setState({
      newSchollDate: Object.assign(newSchollDate, {
        startDate: dateString
      })
    });
  };

  onEndChange = (date, dateString) => {
    console.log("onEndChange", date, dateString);
    const { newSchollDate } = this.state;
    this.setState({
      newSchollDate: Object.assign(newSchollDate, {
        endDate: dateString
      })
    });
  };

  // 开始日期弹窗"确定"回调
  onStartOk = date => {
    console.log("onStartOk", date._i);
  };
  //结束日期弹窗"确定"回调
  onEndOk = date => {
    console.log("onEndOk", date._i);
  };

  onChangeIgnoreHoliday = (key, e) => {
    console.log("change Checkbox", e.target.checked, key);
    const { dateData, newSchollDate } = this.state;
    if (key < dateData.length) {
      this.setState({
        dateData: dateData.map((v, i) => {
          if (i === key) {
            v.ignoreHoliday = e.target.checked;
            return v;
          } else {
            return v;
          }
        })
      });
    } else {
      this.setState({
        newSchollDate: Object.assign(newSchollDate, {
          ignoreHoliday: e.target.checked
        })
      });
    }
  };

  setHistoryShow = isHistoryShow => {
    this.setState({
      isHistoryShow
    });
  };

  renderSchoolDate = dateData => {
    const { newSchollDate } = this.state;
    let template = [];
    const len = dateData.length + 1;
    for (let i = 0; i < len; i++) {
      if (i < len - 1) {
        let startTime = dateData[i].split("~")[0];
        let endTime = dateData[i].split("~")[1];
        template.push(
          <Row className="school-date-row" key={i}>
            <Col span={16}>
              <div className="school-date-item inline-block">{startTime}</div>
              <div className="inline-block" style={{ margin: "0 20px" }}>
                {intl.get("TO")}
              </div>
              <div className="school-date-item inline-block">{endTime}</div>
            </Col>
            <Col span={2}>
              {/* <Checkbox checked={dateData[i].ignoreHoliday} onChange={this.onChangeIgnoreHoliday.bind(this, i)}>{intl.get('AUTO_IGNORE_HOLLIDAY')}</Checkbox> */}
              <Button
                type="danger"
                size="small"
                onClick={this.onDeleteSchoolDate.bind(this, i)}
              >
                <Icon type="minus" />
              </Button>
            </Col>
            <Col span={6}>
              <Checkbox onChange={this.onChangeIgnoreHoliday.bind(this, i)}>{intl.get('AUTO_IGNORE_HOLLIDAY')}</Checkbox>
            </Col>
          </Row>
        );
      } else {
        template.push(
          <Row className="school-date-row" key={i}>
            <Col span={16}>
              <TimeFilter
                startDate={newSchollDate.startDate}
                endDate={newSchollDate.endDate}
                onStartChange={this.onStartChange}
                onEndChange={this.onEndChange}
                onStartOk={this.onStartOk}
                onEndOk={this.onEndOk}
                format={"YYYY-MM-DD HH:mm:ss"}                
                showTime={false}
              />
              {/* <DatePicker locale={locale} onChange={this.onStartChange} />
              <span style={{ margin: '0 20px' }}>{intl.get('TO')}</span>
              <DatePicker locale={locale} onChange={this.onEndChange} /> */}
            </Col>
            <Col span={2}>
       
              <Button
                type="primary"
                size="small"
                onClick={this.onAddSchoolDate.bind(this, "new")}
              >
                <Icon type="plus" />
              </Button>
            </Col>
            <Col span={6}>
              <Checkbox onChange={this.onChangeIgnoreHoliday.bind(this, i)}>{intl.get('AUTO_IGNORE_HOLLIDAY')}</Checkbox>
            </Col>
          </Row>
        );
      }
    }
    return template;
  };

  renderCamera = (cameraData, isInDirection) => {
    const { cameras } = this.props;
    let template = [];
    const len = cameraData.length + 1;
    for (let i = 0; i < len; i++) {
      if (i < len - 1 && Object.keys(cameras).length > 0) {
        template.push(
          <Row key={i}>
            <Col span={16}>
              {cameras[cameraData[i]]
                ? cameras[cameraData[i]].name
                : "此摄像头不存在"}
            </Col>
            <Col span={8}>
              <Button
                type="danger"
                size="small"
                onClick={this.onDeleteInOutCamera.bind(
                  this,
                  cameraData[i],
                  isInDirection
                )}
              >
                <Icon type="minus" />
              </Button>
            </Col>
          </Row>
        );
      } else {
        let options = [];
        for (let id in cameras) {
          if (cameraData.indexOf(id) === -1) {
            options.push(
              <Option key={id} value={id}>
                {cameras[id].name}
              </Option>
            );
          }
        }
        template.push(
          <Row key={i}>
            <Col span={16}>
              <Select
                defaultValue="none"
                style={{ width: 160 }}
                onChange={this.onSelectCamera.bind(this, isInDirection)}
              >
                <Option key={"none"} value="none" style={{ display: "none" }}>
                  {intl.get("CHOOSE_VALUE", { value: intl.get("CAMERA") })}
                </Option>
                {options}
              </Select>
            </Col>
            <Col span={8}>
              <Button
                type="primary"
                size="small"
                onClick={this.onAddCamera.bind(this, isInDirection)}
              >
                <Icon type="plus" />
              </Button>
            </Col>
          </Row>
        );
      }
    }
    return template;
  };

  closeCreatePage = () => {
    this.props.setActivePage("list");
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const {
      updateInOutSchoolOption,
      schoolDate,
      inCameraIds,
      outCameraIds
    } = this.props;
    const {
      isDateModalShow,
      isCameraModalShow,
      isHistoryShow,
      spinning,
      deletedDateKey,
      submitLoading
    } = this.state;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 4 }
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 20 }
      }
    };
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0
        },
        sm: {
          span: 8,
          offset: 8
        }
      }
    };
    // 删除到校时间段对话框配置
    const deleteSchoolDateModal = {
      // title: `${intl.get('DELETE')} ${intl.get('SCHOOL_DATE')}`,
      visible: isDateModalShow,
      okCallback: () => {
        updateInOutSchoolOption({
          schoolDate: schoolDate.filter((v, i) => {
            return i != deletedDateKey;
          })
        });
        this.setState({
          isDateModalShow: false
        });
      },
      cancelCallback: () => {
        this.setState({
          isDateModalShow: false
        });
      },
      wrapClassName: "delete-schoolDate inout-modal",
      okText: intl.get("OKTEXT"),
      cancelText: intl.get("CANCELTEXT"),
      maskClosable: false,
      content: (
        <div className="description-wrap">
          {`${intl.get("QUERY_DELETE_VALUE", {
            value: intl.get("SCHOOL_DATE").toLocaleLowerCase()
          })}`}
        </div>
      )
    };
    // 删除进出摄像头对话框配置
    const deleteInOutCameraModal = {
      // title: `${intl.get('DELETE')} ${intl.get('CAMERA')}`,
      visible: isCameraModalShow,
      okCallback: () => {
        const { deletedCameraId } = this.state;
        const { inCameraIds, outCameraIds } = this.props;
        if (deletedCameraId.isInDirection) {
          updateInOutSchoolOption({
            inCameraIds: inCameraIds.filter((v, i) => {
              return v != deletedCameraId.id;
            })
          });
        } else {
          updateInOutSchoolOption({
            outCameraIds: outCameraIds.filter((v, i) => {
              return v != deletedCameraId.id;
            })
          });
        }
        this.setState({
          isCameraModalShow: false
        });
      },
      cancelCallback: () => {
        this.setState({
          isCameraModalShow: false
        });
      },
      wrapClassName: "delete-camera inout-modal",
      okText: intl.get("OKTEXT"),
      cancelText: intl.get("CANCELTEXT"),
      maskClosable: false,
      content: (
        <div className="description-wrap">
          {`${intl.get("QUERY_DELETE_VALUE", {
            value: intl.get("CAMERA").toLocaleLowerCase()
          })}`}
        </div>
      )
    };
    return <div>
        <CommonModal modalOption={deleteSchoolDateModal} />
        <CommonModal modalOption={deleteInOutCameraModal} />
        <Row style={{ maxWidth: "800px", margin: "40px auto", display: isHistoryShow ? "none" : "block" }}>
          <Spin className="loading-center zi1060" spinning={spinning} />
          <Card title={intl.get("BASIC_SETTING")} bordered={false}>
            <Icon type="close" className="meeting-create-close-btn" onClick={this.closeCreatePage} />
            <Form>
              <FormItem {...formItemLayout} label={"课堂名称"}>
                {getFieldDecorator("meetingName", {
                  rules: [
                    {
                      required: true,
                      message: "课堂名称不能为空"
                    }
                  ]
                })(<Input placeholder="1-40位汉字长度" />)}
              </FormItem>
              <FormItem {...formItemLayout} label={"到课日期"}>
                {getFieldDecorator("schoolDate", {
                  rules: [
                    {
                      required: true,
                      message: intl.get("FORM_VALUE_REQUIRED", {
                        value: intl.get("SCHOOL_DATE")
                      })
                    }
                  ]
                })(<div>{this.renderSchoolDate(schoolDate)}</div>)}
              </FormItem>
              <FormItem {...formItemLayout} label={intl.get("CAMERA_IN")}>
                {getFieldDecorator("cameraIn", {
                  rules: [
                    {
                      required: true,
                      message: intl.get("FORM_VALUE_REQUIRED", {
                        value: intl.get("CAMERA_IN")
                      })
                    }
                  ]
                })(<div>{this.renderCamera(inCameraIds, true)}</div>)}
              </FormItem>
              <FormItem {...formItemLayout} label={intl.get("CAMERA_OUT")}>
                {getFieldDecorator("cameraOut", {
                  rules: [
                    {
                      required: true,
                      message: intl.get("FORM_VALUE_REQUIRED", {
                        value: intl.get("CAMERA_OUT")
                      })
                    }
                  ]
                })(<div>{this.renderCamera(outCameraIds, false)}</div>)}
              </FormItem>

              <h3 className="form-title">人员</h3>

              <FormItem>
                {getFieldDecorator("meetingPerson", {
                  rules: [
                    {
                      required: true,
                      message: "参会人员不能为空"
                    }
                  ]
                })(<Transfer className="select-teacher-transfer" dataSource={this.state.dataSource} showSearch titles={["待选人员", "参会人员"]} targetKeys={this.props.targetKeys // filterOption={this.filterOption}
                    } onChange={this.handleChange} render={item => item.name} searchPlaceholder="请输入工号或姓名" notFoundContent="暂无数据" lazy={false} listStyle={{ width: 220, height: 300 }} />)}
              </FormItem>
            </Form>
          </Card>
        </Row>

        <div className="meeting-submit-btns">
          <Button type="primary" htmlType="submit" onClick={this.handleSubmit.bind(this)}>
            保存
          </Button>
          <Button onClick={this.closeCreatePage}>取消</Button>
        </div>
        <CheckCourseHistory isHistoryShow={isHistoryShow} setHistoryShow={this.setHistoryShow} {...this.props} />
      </div>;
  }
}
const InOut = Form.create({
  mapPropsToFields: props => {
    return {
      schoolDate: Form.createFormField({
        value: props.schoolDate
      }),
      cameraIn: Form.createFormField({
        value: props.inCameraIds
      }),
      cameraOut: Form.createFormField({
        value: props.outCameraIds
      })
    };
  }
})(CreatNameCourse);
export default InOut;
