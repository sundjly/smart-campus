import React, { Component } from "react";
import moment from "moment";
import {
  Select,
  Row,
  Col,
  Button,
  message,
  Spin,
  Menu,
  Icon,
  Pagination,
  Card,
  Input,
  DatePicker
} from "antd";
// import intl from 'react-intl-universal';
import "../MeetingManagement.less";

class MeetingDetail extends Component {
  state = {
    faceList: [
      {
        created: 1526021138000,
        updated: 1526021138000,
        id: 1,
        meetId: 21,
        personId: 9,
        state: 2,
        photoData:
          "http://192.168.11.193/ifaas/api/uploads/2018-05-12-19-18-31-346_format_f.jpg",
        aTime: null,
        aCameraId: null
      },
      {
        created: 1526021138000,
        updated: 1526021138000,
        id: 2,
        meetId: 21,
        personId: 9,
        state: 2,
        photoData:
          "http://192.168.11.193/ifaas/api/uploads/2018-05-12-19-18-31-346_format_f.jpg",
        aTime: null,
        aCameraId: null
      },
      {
        created: 1526021138000,
        updated: 1526021138000,
        id: 3,
        meetId: 21,
        personId: 9,
        state: 2,
        photoData:
          "http://192.168.11.193/ifaas/api/uploads/2018-05-12-19-18-31-346_format_f.jpg",
        aTime: null,
        aCameraId: null
      },
      {
        created: 1526021138000,
        updated: 1526021138000,
        id: 4,
        meetId: 21,
        personId: 9,
        state: 2,
        photoData:
          "http://192.168.11.193/ifaas/api/uploads/2018-05-12-19-18-31-346_format_f.jpg",
        aTime: null,
        aCameraId: null
      },
      {
        created: 1526021138000,
        updated: 1526021138000,
        id: 5,
        meetId: 21,
        personId: 9,
        state: 2,
        photoData:
          "http://192.168.11.193/ifaas/api/uploads/2018-05-12-19-18-31-346_format_f.jpg",
        aTime: null,
        aCameraId: null
      }
    ],
    spinning: false,
    page: 1,
    activeTabKey: "0"
  };

  componentWillMount() {}

  componentDidMount() {}

  setPaddingTop = len => {
    const percent = 10 * (3 - Math.ceil(len / 10)) + "%";
    const px = 30 * (3 - Math.ceil(len / 10)) + "px";
    return `calc(${percent} + ${px})`;
  };

  onClickMenu = e => {
    console.log("click menu", e);
    this.setState(
      {
        activeTabKey: e.key,
        spinning: true
      },
      () => {}
    );
  };

  onCloseDetailPage = () => {
    this.props.setActivePage("list");
  };

  renderFaceList = data => {
    return data.map((v, i) => {
      return (
        <div className="meeting-face-item" key={`state-${v.state}-${i}`}>
          <div className="meeting-face-wrap">
            <img className="meeting-face-img" src={v.photoData} alt="Face" />
            <Icon
              type="setting"
              className="set-state-icon face-icon"
              // onClick={this.onClickSetState.bind(this, v.teacherId, v.pId)}
            />
            <Icon
              type="search"
              className="face-search-icon face-icon"
              // onClick={this.onClickSearchFace}
            />
          </div>
          <div className="meeting-face-btns">
            <span className="meeting-face-name ellipsis" title={"王老师"}>
              {"王老师"}
            </span>
            <span className="meeting-face-num ellipsis" title={v.personId}>
              {v.personId}
            </span>
          </div>
        </div>
      );
    });
  };

  render() {
    const { faceList, spinning, page } = this.state;
    const noData = <div className="meeting-face-list-noData">暂无数据</div>;
    return <div className="detail-content">
        <Card title={"基本信息"} bordered={false}>
          <Icon type="close" className="meeting-detail-close-btn" onClick={this.onCloseDetailPage} />
          <Row className="detail-info-wrap">
            <Col span={12}>
              <Row className="detail-info-item">
                <Col span={4} className="text-center">
                  课程名称
                </Col>
                <Col span={18} className="text-center">
                  <Input />
                </Col>
              </Row>
            </Col>
            <Col span={12}>
              <Row className="detail-info-item">
                <Col span={4} className="text-center">
                  应到人数
                </Col>
                <Col span={18} className="text-center">
                  <Input />
                </Col>
              </Row>
            </Col>
          </Row>
          <DatePicker allowClear={false} className="header-filter" placeholder="选择日期（精确到天）" showToday={false} />
          <Row className="face-list-wrap">
            <Menu defaultSelectedKeys={["0"]} mode="horizontal" onClick={this.onClickMenu.bind(this)}>
              <Menu.Item key="0">未到课（{0}）</Menu.Item>
              <Menu.Item key="1">请假（{0}）</Menu.Item>
              <Menu.Item key="2">已到课（{0}）</Menu.Item>
            </Menu>
            <div className="meeting-face-list">
              {faceList.length > 0 && this.renderFaceList(faceList)}
              {faceList.length === 0 && !spinning && noData}
              <Spin className="loading-center zi1060" spinning={spinning} />
              <Pagination className="meeting-face-list-pagination" style={{ opacity: faceList.length ? "1" : "0", paddingTop: this.setPaddingTop(faceList.length) }} defaultCurrent={1} total={faceList.length} current={page} defaultPageSize={30} onChange={this.onChangePage} />
            </div>
          </Row>
        </Card>
      </div>;
  }
}

export default MeetingDetail;
