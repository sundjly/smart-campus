import React, { Component } from "react";
import {
  Row,
  Col,
  Icon,
  Input,
  Select,
  Button,
  DatePicker,
  List,
  Pagination,
  Spin,
  Popconfirm,
  Table,
  Divider
} from "antd";
import BreadcrumbCustom from "../../BreadcrumbCustom";
import CreateBackRoom from "./CreateNameCourse";
import BackRoomDetail from "./CheckCourseHistory";

import moment from "moment";
import TimeFilter from "../TimeFilter";
import "../MeetingManagement.less";

class BackRoomManagement extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeModal: "cancel",
      activePage: "list",
      listLoading: false,
      isHandleModalShow: false,
      newSchollDate: {
        startDate: moment(new Date().getTime() - 1 * 24 * 3600000).format(
          "YYYY-MM-DD"
        ),
        endDate: moment().format("YYYY-MM-DD")
      },
      dataSource: [
        {
          key: "0",
          name: "Edward King 0",
          age: "32",
          address: "London, Park Lane no. 0"
        },
        {
          key: "1",
          name: "Edward King 1",
          age: "32",
          address: "London, Park Lane no. 1"
        }
      ],
      count: 2
    };
    this.columns = [
      { title: "课程", dataIndex: "1" },
      { title: "课程时间", dataIndex: "2" },
      { title: "所属班级", dataIndex: "3" },
      { title: "应到人数", dataIndex: "4" },
      { title: "已到记录", dataIndex: "5" },
      { title: "离开记录", dataIndex: "6" },
      ,
      { title: "未到记录", dataIndex: "7" },
      ,
      { title: "请假", dataIndex: "8" },
      {
        title: "操作",
        dataIndex: "9",
        render: (text, record) => {
          return (
            <span>
              <a
                href="javascript:;"
                onClick={this.setActivePage.bind(this, "detail")}
              >
                详情
              </a>
              <Divider type="vertical" />
              <a
                href="javascript:;"
                onClick={this.setActivePage.bind(this, "create")}
              >
                修改
              </a>
            </span>
          );
        }
      }
    ];
  }

  componentWillMount() {
    const { getCourseList } = this.props;
    getCourseList().then(() => {
     console.log(444);
    })
  }

  getBackRoomState = state => {
    switch (state) {
      case 0:
        return "未开始";
      case 1:
        return "已取消";
      case 2:
        return "进行中";
      case 3:
        return "已结束";
      default:
        return "";
    }
  };

  editBackRoom = item => {
    console.log("edit meeting", item);
    this.setActivePage("create");
  };

  cancelBackRoom = item => {
    console.log("cancel meeeting", item);
  };

  deleteBackRoom = item => {
    console.log("delete meeting", item);
  };

  setActivePage = activePage => {
    console.log(activePage);
    this.setState({
      activePage
    });
  };

  onStartChange = (date, dateString) => {
    console.log("onStartChange", date, dateString);
    const { newSchollDate } = this.state;
    this.setState({
      newSchollDate: Object.assign(newSchollDate, {
        startDate: dateString
      })
    });
  };

  onEndChange = (date, dateString) => {
    console.log("onEndChange", date, dateString);
    const { newSchollDate } = this.state;
    this.setState({
      newSchollDate: Object.assign(newSchollDate, {
        endDate: dateString
      })
    });
  };

  // 开始日期弹窗"确定"回调
  onStartOk = date => {
    console.log("onStartOk", date._i);
  };
  //结束日期弹窗"确定"回调
  onEndOk = date => {
    console.log("onEndOk", date._i);
  };

  render() {
    const {
      listLoading,
      isHandleModalShow,
      activeModal,
      activePage,
      dataSource
    } = this.state;
    const columns = this.columns;

    return (
      <div className="meeting-manage-wrap">
        <div
          className="meeting-list-page"
          style={{ display: activePage === "list" ? "block" : "none" }}
        >
          <BreadcrumbCustom first="业务管理" second="课堂点名管理" />
          <Row className="meeting-manage-header">
            <Input placeholder="请输入会议名称" className="header-filter" />
            <TimeFilter
              startDate={this.state.newSchollDate.startDate}
              endDate={this.state.newSchollDate.endDate}
              onStartChange={this.onStartChange}
              onEndChange={this.onEndChange}
              onStartOk={this.onStartOk}
              onEndOk={this.onEndOk}
              showTime={false}
            />
            <Button style={{ marginRight: "100px", marginLeft: "20px" }}>
              <Icon type="search" />查询
            </Button>
            <Button
              type="primary"
              onClick={this.setActivePage.bind(this, "create")}
            >
              <Icon type="plus" />新增课程
            </Button>
          </Row>
          <Row className="meeting-manage-content">
            <div className="meeting-list">
              <Table columns={columns} dataSource={dataSource} />
            </div>
            <Spin className="loading-center zi1060 " spinning={listLoading} />
          </Row>
        </div>
        <div
          className="meeting-create-page"
          style={{ display: activePage === "create" ? "block" : "none" }}
        >
          <BreadcrumbCustom
            first="业务管理"
            second="到课管理"
            third="新增/修改"
          />
          <CreateBackRoom {...this.props} setActivePage={this.setActivePage} />
        </div>
        <div
          className="meeting-detail-page"
          style={{ display: activePage === "detail" ? "block" : "none" }}
        >
          <BreadcrumbCustom
            first="业务管理"
            second="课堂管理"
            third="到课记录"
          />
          <BackRoomDetail {...this.props} setActivePage={this.setActivePage} />
        </div>
      </div>
    );
  }
}

export default BackRoomManagement;
