import React, { Component } from 'react'
import {
  Row,
  Col,
  Icon,
  Input,
  Select,
  Button,
  DatePicker,
  List,
  Pagination,
  Spin,
  Popconfirm,
  message
} from 'antd'
import BreadcrumbCustom from '../BreadcrumbCustom'
// import intl from "react-intl-universal";
import CreateMeeting from './CreateMeeting'
import MeetingDetail from './MeetingDetail'

import './MeetingManagement.less'

const Option = Select.Option

const meetingListHeader = (
  <div className='meeting-list-header'>
    <Row>
      <Col span={5}>会议名称</Col>
      <Col span={5}>会议地点</Col>
      <Col span={4}>会议时间</Col>
      <Col span={3}>会议状态</Col>
      <Col span={2}>缺会人数</Col>
      <Col span={5}>操作</Col>
    </Row>
  </div>
)
class MeetingManagement extends Component {
  constructor (props) {
    super(props)
    this.state = {
      activeModal: 'cancel',
      activePage: 'list',
      listLoading: false,
      isHandleModalShow: false,
      getListParams: {
        userId: 1,
        name: '',
        time: '',
        state: ''
      },
      page: 1,
      pageSize: 10,
      detailMeeting: {}
    }
  }

  componentWillMount () {
    const { getMeetingList } = this.props
    this.setState({
      listLoading: true
    }, () => {
      getMeetingList(this.buildGetMeetingListParams()).then(() => {
        this.setState({
          listLoading: false
        })
      })
    })
  }

  buildGetMeetingListParams () {
    const { getListParams, page, pageSize } = this.state
    const data = Object.assign(getListParams, {
      page,
      pageSize
    })
    return data
  }

  getMeetingState (state) {
    switch (state) {
      case 0:
        return '未开始'
      case 1:
        return '已取消'
      case 2:
        return '进行中'
      case 3:
        return '已结束'
      default:
        return ''
    }
  };

  editMeeting (item) {
    console.log('edit meeting', item)
    this.setActivePage('create')
  };

  cancelMeeting (item) {
    console.log('cancel meeeting', item)
    const { cancelMeeting, getMeetingList } = this.props
    this.setState({
      listLoading: true
    }, () => {
      cancelMeeting({
        urlParams: {
          cancel: item.id
        }
      }).then(res => {
        console.log('delete meeting res', res)
        if (res.data && !res.errCode) {
          getMeetingList(this.buildGetMeetingListParams())
            .then(() => {
              this.setState({
                listLoading: false
              })
            })
        } else {
          message.error('取消会议失败')
          this.setState({
            listLoading: false
          })
        }
      })
    })
  };

  deleteMeeting (item) {
    console.log('delete meeting', item)
    const { deleteMeeting, getMeetingList } = this.props
    this.setState({
      listLoading: true
    }, () => {
      deleteMeeting({
        urlParams: {
          delete: item.id
        }
      }).then(res => {
        console.log('delete meeting res', res)
        if (res.data && !res.errCode) {
          this.setState({
            page: 1
          }, () => {
            getMeetingList(this.buildGetMeetingListParams())
              .then(() => {
                this.setState({
                  listLoading: false
                })
              })
          })
        } else {
          message.error('删除会议失败')
          this.setState({
            listLoading: false
          })
        }
      })
    })
  };

  setActivePage (activePage) {
    this.setState({
      activePage
    })
  };

  onNameInputChange (e) {
    const { getListParams } = this.state
    this.setState({
      getListParams: Object.assign(getListParams, {
        name: e.target.value
      })
    })
  }

  onStateSelectChange (v) {
    const { getListParams } = this.state
    this.setState({
      getListParams: Object.assign(getListParams, {
        state: v === 'all' ? '' : v
      })
    })
  }

  onDateChange (date, dateString) {
    const { getListParams } = this.state
    this.setState({
      getListParams: Object.assign(getListParams, {
        time: dateString.split(' ')[0]
      })
    })
  }

  onSearchMeeting () {
    const { getMeetingList } = this.props
    this.setState({
      listLoading: true
    }, () => {
      this.setState({
        page: 1
      }, () => {
        getMeetingList(this.buildGetMeetingListParams()).then(() => {
          this.setState({
            listLoading: false
          })
        })
      })
    })
  }

  // pageNum
  onChangePage (page, pagesize) {
    console.log('change page', page)
    this.setState({
      page,
      listLoading: true
    }, () => {
      this.props.getMeetingList(this.buildGetMeetingListParams()).then(() => {
        this.setState({
          listLoading: false
        })
      })
    })
  };

  // pageSize
  onShowSizeChange (page, pageSize) {
    console.log('change showsize', pageSize)
    this.setState({
      page: 1,
      listLoading: true,
      pageSize
    }, () => {
      this.props.getMeetingList(this.buildGetMeetingListParams()).then(() => {
        this.setState({
          listLoading: false
        })
      })
    })
  };

  viewMeetingDetail (meeting) {
    const { getMeetingDetail } = this.props
    getMeetingDetail({
      meetId: meeting.id,
      state: 0
    }, 0).then(() => {
      this.setState({
        detailMeeting: meeting
      }, () => {
        this.setActivePage('detail')
      })
    })
  }

  renderHandleButtons (item) {
    switch (item.state) {
      case 0:
        return (
          <div>
            <Button size='small' onClick={this.editMeeting.bind(this, item)}>
              编辑
            </Button>
            <Popconfirm
              title='确定取消此会议吗？'
              onConfirm={this.cancelMeeting.bind(this, item)}
              okText='确定'
              cancelText='取消'
            >
              <Button type='primary' size='small'>
                取消
              </Button>
            </Popconfirm>
            <Popconfirm
              title='确定要删除此会议吗？'
              onConfirm={this.deleteMeeting.bind(this, item)}
              okText='确定'
              cancelText='取消'
            >
              <Button type='danger' size='small'>
                删除
              </Button>
            </Popconfirm>
          </div>
        )
      case 1:
        return (
          <div>
            <Popconfirm
              title='确定要删除此会议吗？'
              onConfirm={this.deleteMeeting.bind(this, item)}
              okText='确定'
              cancelText='取消'
            >
              <Button type='danger' size='small'>
                删除
              </Button>
            </Popconfirm>
          </div>
        )
      case 2:
        return (
          <div>
            <Popconfirm
              title='确定取消此会议吗？'
              onConfirm={this.cancelMeeting.bind(this, item)}
              okText='确定'
              cancelText='取消'
            >
              <Button type='primary' size='small'>
                终止
              </Button>
            </Popconfirm>
          </div>
        )
      case 3:
        return ''
      default:
        return ''
    }
  };

  render () {
    const { listLoading, activePage, page, pageSize, detailMeeting } = this.state
    const { meetingList, meetingTotal } = this.props
    return (
      <div className='meeting-manage-wrap'>
        <div
          className='meeting-list-page'
          style={{ display: activePage === 'list' ? 'block' : 'none' }}
        >
          <BreadcrumbCustom first='业务管理' second='会议管理' />
          <Row className='meeting-manage-header'>
            <Input placeholder='请输入会议名称' className='header-filter' onChange={this.onNameInputChange.bind(this)} />
            <Select showSearch placeholder='请选择会议状态' className='header-filter' onChange={this.onStateSelectChange.bind(this)}>
              <Option value='all'>全部</Option>
              <Option value='0'>未开始</Option>
              <Option value='2'>进行中</Option>
              <Option value='3'>已结束</Option>
              <Option value='1'>已取消</Option>
            </Select>
            <DatePicker
              allowClear
              className='header-filter'
              placeholder='请选择日期'
              showToday={false}
              onChange={this.onDateChange.bind(this)}
            />
            <Button onClick={this.onSearchMeeting.bind(this)}>
              <Icon type='search' />查询
            </Button>
            <Button style={{ float: 'right' }} type='primary' onClick={this.setActivePage.bind(this, 'create')}>
              <Icon type='plus' />新增
            </Button>
          </Row>
          <Row className='meeting-manage-content'>
            <div className='meeting-list'>
              <List
                header={meetingListHeader}
                bordered
                dataSource={meetingList}
                renderItem={item => (
                  <List.Item>
                    <Row type='flex' align='middle' justify='start' className='meeting-item'>
                      <Col span={5}>{item.name}</Col>
                      <Col span={5}>{item.address}</Col>
                      <Col span={4}>{`${item.timeRange.split('~')[0]}~${
                        item.timeRange.split('~')[1].split(' ')[1]
                      }`}</Col>
                      <Col span={3}>{this.getMeetingState(item.state)}</Col>
                      <Col span={2}>
                        {/* <a onClick={this.setActivePage.bind(this, 'detail')}> */}
                        <a onClick={this.viewMeetingDetail.bind(this, item)}>
                          {item.noArrivedCount}
                        </a>
                      </Col>
                      <Col span={5}>{this.renderHandleButtons(item)}</Col>
                    </Row>
                  </List.Item>
                )}
              />
            </div>
            <Pagination
              className='meeeting-list-pagination'
              current={page}
              pageSize={pageSize}
              total={meetingTotal}
              showSizeChanger
              pageSizeOptions={['10', '20', '50']}
              onChange={this.onChangePage.bind(this)}
              onShowSizeChange={this.onShowSizeChange.bind(this)}
            />
            <Spin className='loading-center zi1060 ' spinning={listLoading} />
          </Row>
        </div>
        <div
          className='meeting-create-page'
          style={{ display: activePage === 'create' ? 'block' : 'none' }}
        >
          <BreadcrumbCustom first='业务管理' second='会议管理' third='新增/修改' />
          <CreateMeeting {...this.props} setActivePage={this.setActivePage.bind(this)} />
        </div>
        <div
          className='meeting-detail-page'
          style={{ display: activePage === 'detail' ? 'block' : 'none' }}
        >
          <BreadcrumbCustom first='业务管理' second='会议管理' third='到会记录' />
          <MeetingDetail
            {...this.props}
            setActivePage={this.setActivePage.bind(this)}
            meetingDetail={detailMeeting}
          />
        </div>
      </div>
    )
  }
}

export default MeetingManagement
