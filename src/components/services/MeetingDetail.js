import React, { Component } from 'react'
import { Row, Col, Spin, Menu, Icon, Pagination, Card } from 'antd'
// import intl from 'react-intl-universal';

import './MeetingManagement.less'

class MeetingDetail extends Component {
  constructor (props) {
    super(props)
    this.state = {
      faceList: [],
      spinning: false,
      page: 1,
      activeTabKey: '0'
    }
  }

  componentWillMount () {}

  componentDidMount () {}

  setPaddingTop (len) {
    const percent = 10 * (3 - Math.ceil(len / 10)) + '%'
    const px = 30 * (3 - Math.ceil(len / 10)) + 'px'
    return `calc(${percent} + ${px})`
  };

  onClickMenu (e) {
    console.log('click menu', e)
    this.setState(
      {
        activeTabKey: e.key,
        spinning: true
      },
      () => {}
    )
  };

  onCloseDetailPage () {
    this.props.setActivePage('list')
  }

  renderFaceList (data) {
    return data.map((v, i) => {
      return (
        <div className='meeting-face-item' key={`state-${v.state}-${i}`}>
          <div className='meeting-face-wrap'>
            <img className='meeting-face-img' src={v.photoData} alt='Face' />
            <Icon
              type='setting'
              className='set-state-icon face-icon'
              // onClick={this.onClickSetState.bind(this, v.teacherId, v.pId)}
            />
            <Icon
              type='search'
              className='face-search-icon face-icon'
              // onClick={this.onClickSearchFace}
            />
          </div>
          <div className='meeting-face-btns'>
            <span className='meeting-face-name ellipsis' title={'王老师'}>
              {'王老师'}
            </span>
            <span className='meeting-face-num ellipsis' title={v.personId}>
              {v.personId}
            </span>
          </div>
        </div>
      )
    })
  };

  render () {
    const { spinning, page } = this.state
    const { setActivePage, meetingDetail, meetingPersonList } = this.props
    const noData = <div className='meeting-face-list-noData'>暂无数据</div>
    return (
      <div className='detail-content'>
        <Card title={'基本信息'} bordered={false}>
          <Icon
            type='close'
            className='meeting-detail-close-btn'
            onClick={setActivePage.bind(this, 'list')}
          />
          <Row className='detail-info-wrap'>
            <Col span={12}>
              <Row className='detail-info-item'>
                <Col span={4} className='text-right'>
                  会议名称
                </Col>
                <Col span={20} className='text-center'>
                  {meetingDetail.name ? meetingDetail.name : ''}
                </Col>
              </Row>
              <Row className='detail-info-item'>
                <Col span={4} className='text-right'>
                  会议地点
                </Col>
                <Col span={20} className='text-center'>
                  {meetingDetail.address ? meetingDetail.address : ''}
                </Col>
              </Row>
              <Row className='detail-info-item'>
                <Col span={4} className='text-right'>
                  会议时间
                </Col>
                <Col span={20} className='text-center'>
                  {meetingDetail.timeRange ? meetingDetail.timeRange : ''}
                </Col>
              </Row>
              <Row className='detail-info-item'>
                <Col span={4} className='text-right'>
                  会议管理员
                </Col>
                <Col span={20} className='text-center'>
                  {meetingDetail.adminIds ? meetingDetail.adminIds : ''}
                </Col>
              </Row>
            </Col>
            <Col span={12}>
              <Row className='detail-info-item'>
                <Col span={4} className='text-right'>
                  应到人数
                </Col>
                <Col span={20} className='text-center'>
                  {meetingDetail.allCount ? meetingDetail.allCount : 0}
                </Col>
              </Row>
              <Row className='detail-info-item'>
                <Col span={4} className='text-right'>
                  未到人数
                </Col>
                <Col span={20} className='text-center'>
                  {meetingDetail.noArrivedCount ? meetingDetail.noArrivedCount : 0}
                </Col>
              </Row>
              <Row className='detail-info-item'>
                <Col span={4} className='text-right'>
                  请假人数
                </Col>
                <Col span={20} className='text-center'>
                  {meetingDetail.holidayCount ? meetingDetail.holidayCount : 0}
                </Col>
              </Row>
              <Row className='detail-info-item'>
                <Col span={4} className='text-right'>
                  实到人数
                </Col>
                <Col span={20} className='text-center'>
                  {meetingDetail.arrivedCount ? meetingDetail.arrivedCount : 0}
                </Col>
              </Row>
            </Col>
          </Row>
          <Row className='face-list-wrap'>
            <Menu
              defaultSelectedKeys={['0']}
              mode='horizontal'
              onClick={this.onClickMenu.bind(this)}
            >
              <Menu.Item key='0'>未到（{0}）</Menu.Item>
              <Menu.Item key='1'>已到（{0}）</Menu.Item>
              <Menu.Item key='2'>请假（{0}）</Menu.Item>
            </Menu>
            <div className='meeting-face-list'>
              {meetingPersonList.length > 0 && this.rendermeetingPersonList(meetingPersonList)}
              {meetingPersonList.length === 0 && !spinning && noData}
              <Spin className='loading-center zi1060' spinning={spinning} />
              <Pagination
                className='meeting-face-list-pagination'
                style={{
                  opacity: meetingPersonList.length ? '1' : '0',
                  paddingTop: this.setPaddingTop(meetingPersonList.length)
                }}
                defaultCurrent={1}
                total={meetingPersonList.length}
                current={page}
                defaultPageSize={30}
                onChange={this.onChangePage}
              />
            </div>
          </Row>
        </Card>
      </div>
    )
  }
}

export default MeetingDetail
