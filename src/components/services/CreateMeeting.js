import React, { Component } from 'react'
import { Form, Select, Row, Button, Input, Transfer, Icon, Col, message, Popconfirm } from 'antd'
// import intl from 'react-intl-universal';
import TimeFilter from './TimeFilter'
// import MyTransfer from '../common/Transfer'

import './MeetingManagement.less'

const FormItem = Form.Item
const Option = Select.Option

class CreateMeetingForm extends Component {
  constructor (props) {
    super(props)
    // 设置 initial state
    this.state = {
      dataSource: [
        // {
        //   key: 1,
        //   name: '张老师'
        // },
        // {
        //   key: 2,
        //   name: '李三元_1001'
        // },
        // {
        //   key: 3,
        //   name: '王老师'
        // },
        // {
        //   key: 4,
        //   name: '刘老师'
        // },
        // {
        //   key: 5,
        //   name: '赵老师'
        // },
        // {
        //   key: 6,
        //   name: '陈老师'
        // },
        // {
        //   key: 7,
        //   name: '李三圆_1002'
        // }
      ],
      transferLeftData: [],
      transferRightData: [],
      adminName: '',
      selectedAdmin: '', // select已经选择待添加的会议管理员
      searchedAdmins: [], // 输入框搜索后待选的管理员
      selectedCameraId: ''
    }
  }

  componentWillMount () {
    const data = {
      classId: '',
      gradeId: '',
      pType: 1,
      page: 1,
      pageSize: 10000,
      queryStr: '',
      status: ''
    }
    this.props.searchTeacher(data)
      .then(res => {
        console.log('transfer search res', res)
        if (res && res.data && !res.errCode) {
          const dataSource = res.data.map((v, i) => {
            return {
              key: v.detailId,
              name: `${v.name}-${v.number}`
            }
          })
          // Todo获取全部学生信息渲染左侧选择框
          // Todo后续换成老师接口
          // 数据过多时过滤存在性能问题
          this.setState({
            dataSource
          })
        }
      })
  }

  componentDidMount () { }

  onStartChange (date, dateString) {
    console.log('onStartChange', date, dateString)
    const { updateDetail, meetingDetail } = this.props
    meetingDetail.rangeTime[0] = dateString
    updateDetail({ meetingDetail })
  };

  onEndChange (date, dateString) {
    console.log('onEndChange', date, dateString)
    const { updateDetail, meetingDetail } = this.props
    meetingDetail.rangeTime[1] = dateString
    updateDetail({ meetingDetail })
  };

  // 开始日期弹窗"确定"回调
  onStartOk (date) {
    console.log('onStartOk', date._i)
  };

  // 结束日期弹窗"确定"回调
  onEndOk (date) {
    console.log('onEndOk', date._i)
  };

  closeCreatePage () {
    this.props.setActivePage('list')
  };

  onTransferChange (targetKeys) {
    const { updateDetail, meetingDetail } = this.props
    const payload = Object.assign(meetingDetail, {
      targetKeys
    })
    updateDetail({ meetingDetail: payload })
  };

  handleSubmit (e) {
    e.preventDefault()
    this.props.form.validateFieldsAndScroll((errors, values) => {
      console.log('create submit : ', errors, values)
      if (errors) {
        return false
      } else {

      }
    })
  }

  onTransferSearchChange (direction, e) {
    // console.log('onTransferSearchChange', direction, e.target.value)
    // if (direction === 'left') {
    //   // Todo 此处先调用学生接口作为测试，后续接入教师搜索接口
    //   const data = {
    //     classId: '',
    //     gradeId: '',
    //     pType: 1,
    //     page: 1,
    //     pageSize: 20,
    //     queryStr: e.target.value,
    //     status: ''
    //   }
    //   this.props.searchTeacher(data)
    //     .then(res => {
    //       console.log('transfer search res', res)
    //       if (res && res.data && !res.errCode) {
    //         const transferLeftData = res.data.map((v, i) => {
    //           return {
    //             key: v.detailId,
    //             name: `${v.name}-${v.number}`
    //           }
    //         })
    //         this.setState({
    //           transferLeftData
    //         })
    //       }
    //     })
    // }
  }

  onSearchAdmin (value) {
    // console.log('onSearch admin', value)
    // Todo 此处先调用学生接口作为测试，后续接入教师搜索接口
    const data = {
      classId: '',
      gradeId: '',
      pType: 1,
      page: 1,
      pageSize: 20,
      queryStr: value,
      status: ''
    }
    this.props.searchTeacher(data)
      .then(res => {
        // console.log('搜索res', res)
        if (res && res.data && !res.errCode) {
          this.setState({
            searchedAdmins: res.data
          })
        }
      })
  }

  onSelectAdmin (value) {
    console.log('onSelect admin', value)
    const { searchedAdmins } = this.state
    const selectedAdmin = searchedAdmins.filter((v) => {
      return `${v.name}` === value.split('-')[0] && `${v.number}` === value.split('-')[1]
    })[0]
    this.setState({
      selectedAdmin
    })
  }

  addMeetingAdmin () {
    const { selectedAdmin } = this.state
    if (selectedAdmin) {
      const { updateDetail, meetingDetail } = this.props
      const admins = meetingDetail.admins
      admins.push(selectedAdmin.detailId)
      const payload = {
        meetingDetail: Object.assign(meetingDetail, {
          admins
        })
      }
      updateDetail(payload)
      this.setState({
        selectedAdmin: ''
      })
    } else {
      message.error('未选择会议管理员')
    }
  }

  deleteMeetingAdmin (adminId) {
    const { updateDetail, meetingDetail } = this.props
    const admins = meetingDetail.admins
    const payload = {
      meetingDetail: Object.assign(meetingDetail, {
        admins: admins.filter((v) => {
          return `${v}` !== `${adminId}`
        })
      })
    }
    updateDetail(payload)
  }

  addMeetingCamera () {
    const { selectedCameraId } = this.state
    if (selectedCameraId) {
      const { updateDetail, meetingDetail } = this.props
      const cameras = meetingDetail.cameras
      cameras.push(selectedCameraId)
      const payload = {
        meetingDetail: Object.assign(meetingDetail, {
          cameras
        })
      }
      updateDetail(payload)
      this.setState({
        selectedCameraId: ''
      })
    } else {
      message.error('未选择会议摄像头')
    }
  }

  deleteMeetingCamera (cameraId) {
    const { updateDetail, meetingDetail } = this.props
    const cameras = meetingDetail.cameras
    const payload = {
      meetingDetail: Object.assign(meetingDetail, {
        cameras: cameras.filter((v) => {
          return `${v}` !== `${cameraId}`
        })
      })
    }
    updateDetail(payload)
  }

  onSelectMeetingCamera (value) {
    console.log('select camera', value)
    this.setState({
      selectedCameraId: value
    })
  }

  onNameInputChange (e) {
    const { updateDetail, meetingDetail } = this.props
    meetingDetail.name = e.target.value
    updateDetail({ meetingDetail })
  }

  onAddressInputChange (e) {
    const { updateDetail, meetingDetail } = this.props
    meetingDetail.address = e.target.value
    updateDetail({ meetingDetail })
  }

  renderMeetingAdmins (admins) {
    let template = []
    const len = admins.length + 1
    for (let i = 0; i < len; i++) {
      if (i < len - 1) {
        template.push(
          <Row className='' key={i}>
            <Col span={12}>
              <div className=''>{`teacherId-${admins[i]}`}</div>
            </Col>
            <Col span={12}>
              <Popconfirm
                title='确定删除此管理员吗？'
                onConfirm={this.deleteMeetingAdmin.bind(this, admins[i])}
                cancelText='取消'
              >
                <Button type='danger' size='small'><Icon type='minus' /></Button>
              </Popconfirm>
            </Col>
          </Row>
        )
      } else {
        let options = []
        this.state.searchedAdmins.forEach((v) => {
          options.push(
            <Option key={`${v.name}-${v.number}`}>{`${v.name}-${v.number}`}</Option>
          )
        })
        template.push(
          <Row key={i}>
            <Col span={12}>
              <Select
                style={{ width: 160 }}
                mode='combobox'
                placeholder={'请输入姓名搜索'}
                defaultActiveFirstOption={false}
                showArrow={false}
                filterOption={false}
                onSearch={this.onSearchAdmin.bind(this)}
                onSelect={this.onSelectAdmin.bind(this)}
              // onChange={this.onChangeAdmin.bind(this)}
              >
                {options}
              </Select>
            </Col>
            <Col span={12}>
              <Button type='primary' size='small' onClick={this.addMeetingAdmin.bind(this)} ><Icon type='plus' /></Button>
            </Col>
          </Row>
        )
      }
    }
    return template
  }

  renderCamera (cameraData) {
    const { cameras } = this.props
    let template = []
    const len = cameraData.length + 1
    for (let i = 0; i < len; i++) {
      if (i < len - 1 && Object.keys(cameras).length > 0) {
        template.push(
          <Row key={i}>
            <Col span={12}>
              {cameras[cameraData[i]] ? cameras[cameraData[i]].name : '此摄像头不存在'}
            </Col>
            <Col span={12}>
              <Popconfirm
                title='确定删除此摄像头吗？'
                onConfirm={this.deleteMeetingCamera.bind(this, cameraData[i])}
                okText='确定'
                cancelText='取消'
              >
                <Button type='danger' size='small' ><Icon type='minus' /></Button>
              </Popconfirm>
            </Col>
          </Row>
        )
      } else {
        let options = []
        for (let id in cameras) {
          if (cameraData.indexOf(id) === -1) {
            options.push(
              <Option key={id} value={id}>{cameras[id].name}</Option>
            )
          }
        }
        template.push(
          <Row key={i}>
            <Col span={12}>
              <Select defaultValue='none' style={{ width: 160 }} onChange={this.onSelectMeetingCamera.bind(this)}>
                <Option key={'none'} value='none' style={{ display: 'none' }}>选择摄像头</Option>
                {options}
              </Select>
            </Col>
            <Col span={12}>
              <Button type='primary' size='small' onClick={this.addMeetingCamera.bind(this)}><Icon type='plus' /></Button>
            </Col>
          </Row>
        )
      }
    }
    return template
  }

  render () {
    const { getFieldDecorator } = this.props.form
    const { meetingDetail, setActivePage } = this.props
    const { rangeTime, admins, cameras, targetKeys } = meetingDetail
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 4 }
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 20 }
      }
    }
    return (
      <div style={{ position: 'relative' }}>
        <Icon type='close' className='meeting-create-close-btn' onClick={setActivePage.bind(this, 'list')} />
        <h2 className='form-title'>基本信息</h2>
        <Row style={{ maxWidth: '500px', margin: '30px auto' }}>
          <Form>
            <FormItem {...formItemLayout} label={'会议名称'}>
              {getFieldDecorator('meetingName', {
                rules: [
                  {
                    required: true,
                    message: '会议名称不能为空'
                  }
                ]
              })(<Input placeholder='请输入会议名称' onChange={this.onNameInputChange.bind(this)} />)}
            </FormItem>
            <FormItem {...formItemLayout} label={'会议地点'}>
              {getFieldDecorator('meetingAddress', {
                rules: [
                  {
                    required: true,
                    message: '会议地点不能为空'
                  }
                ]
              })(<Input placeholder='请输入会议地点' onChange={this.onAddressInputChange.bind(this)} />)}
            </FormItem>
            <FormItem {...formItemLayout} label={'会议时间'}>
              {getFieldDecorator('meetingTime', {
                rules: [
                  {
                    required: true,
                    type: 'array',
                    message: '会议时间不能为空'
                  }
                  // {
                  //   validator: (rule, value, callback) => {
                  //     if(value[0] && value[1]){
                  //       callback();
                  //     }else{
                  //       callback(new Error("会议时间不能为空"));
                  //     }
                  //   }
                  // }
                ]
              })(
                <TimeFilter
                  startDate={rangeTime[0]}
                  endDate={rangeTime[1]}
                  onStartChange={this.onStartChange.bind(this)}
                  onEndChange={this.onEndChange.bind(this)}
                  onStartOk={this.onStartOk.bind(this)}
                  onEndOk={this.onEndOk.bind(this)}
                  format={'YYYY-MM-DD HH:mm:ss'}
                  showTime
                />
              )}
            </FormItem>
            <FormItem {...formItemLayout} label={'会议管理员'}>
              {getFieldDecorator('meetingAdmin', {
                rules: [
                  {
                    required: true,
                    type: 'array',
                    message: '未添加会议管理员'
                  }
                ],
                validateTrigger: null,
                trigger: null
              })(
                <div>
                  {this.renderMeetingAdmins(admins)}
                </div>
              )}
            </FormItem>
            <FormItem {...formItemLayout} label={'识别设备'}>
              {getFieldDecorator('meetingCamera', {
                rules: [
                  {
                    required: true,
                    type: 'array',
                    message: '未选择签到识别设备'
                  }
                ]
              })(
                <div>
                  {this.renderCamera(cameras)}
                </div>
              )}
            </FormItem>
          </Form>
        </Row>
        <h2 className='form-title'>参会人员</h2>
        <FormItem className='transfer-form-item'>
          {getFieldDecorator('meetingPerson', {
            rules: [
              {
                required: true,
                message: '参会人员不能为空'
              }
            ]
          })(
            <Transfer
              className='select-teacher-transfer'
              dataSource={this.state.dataSource}
              showSearch
              titles={['待选人员', '参会人员']}
              // filterOption={this.filterOption}
              targetKeys={targetKeys}
              onChange={this.onTransferChange.bind(this)}
              render={item => item.name}
              onSearchChange={this.onTransferSearchChange.bind(this)}
              searchPlaceholder='请输入工号或姓名'
              notFoundContent='暂无数据'
              lazy={false}
              listStyle={{
                width: 220,
                height: 300
              }}
            />
          )}
        </FormItem>
        <div className='meeting-submit-btns'>
          <Button type='primary' htmlType='submit' onClick={this.handleSubmit.bind(this)}>保存</Button>
          <Button onClick={setActivePage.bind(this, 'list')}>取消</Button>
        </div>
        {/* <div>
          <MyTransfer
            leftData={this.state.transferLeftData}
            rightData={this.state.transferRightData}
            onLeftSearch={}
          />
        </div> */}
      </div>
    )
  }
}
const CreateMeeting = Form.create({
  mapPropsToFields: props => {
    const { name, address, rangeTime, admins, cameras, targetKeys } = props.meetingDetail
    return {
      meetingName: Form.createFormField({
        value: name
      }),
      meetingAddress: Form.createFormField({
        value: address
      }),
      meetingTime: Form.createFormField({
        value: rangeTime
      }),
      meetingAdmin: Form.createFormField({
        value: admins
      }),
      meetingCamera: Form.createFormField({
        value: cameras
      }),
      meetingPerson: Form.createFormField({
        value: targetKeys
      })
    }
  }
})(CreateMeetingForm)
export default CreateMeeting
