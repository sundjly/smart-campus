import PropTypes from 'prop-types';
import React, { Component } from 'react';
import moment from 'moment';
import { DatePicker } from 'antd';

const defaultProps = {
  startDate: '',
  endDate: ''
};

const propTypes = {
  // startDate: PropTypes.string.isRequired,
  // endDate: PropTypes.string.isRequired,
  onStartChange: PropTypes.func.isRequired,
  onEndChange: PropTypes.func.isRequired,
  //   onEndStart: PropTypes.func.isRequired,
  onEndOk: PropTypes.func.isRequired
};

// DatePicker语言设置
const locale = {
  lang: {
    placeholder: "请选择到校日期",
    rangePlaceholder: ["开始日期", "结束日期"],
    today: "今天",
    now: "现在",
    backToToday: "Back to today",
    ok: "确定",
    clear: "清除",
    month: "月",
    year: "年",
    timeSelect: "时间选择",
    dateSelect: "日期选择",
    monthSelect: "选择一个月份",
    yearSelect: "选择一个年份",
    decadeSelect: "选择一个年份段",
    yearFormat: "YYYY",
    dateFormat: "M/D/YYYY",
    dayFormat: "D",
    dateTimeFormat: "M/D/YYYY HH:mm:ss",
    monthFormat: "MMMM",
    monthBeforeYear: true,
    previousMonth: "Previous month (PageUp)",
    nextMonth: "Next month (PageDown)",
    previousYear: "Last year (Control + left)",
    nextYear: "Next year (Control + right)",
    previousDecade: "Last decade",
    nextDecade: "Next decade",
    previousCentury: "Last century",
    nextCentury: "Next century",
  },
  timePickerLocale: {
    placeholder: "请选择时间",
  },
};

class TimeFilter extends Component {
  state = {
    endOpen: false //控制开始结束日期弹窗关闭联动
  };
  //控制不可选开始日期
  disabledStartDate = startValue => {
    const endValue = moment(this.props.endDate);
    if (!startValue || !endValue) {
      return false;
    }
    return startValue.valueOf() > endValue.valueOf();
  };
  //控制不可选结束日期
  disabledEndDate = endValue => {
    const startValue = moment(this.props.startDate);
    if (!endValue || !startValue) {
      return false;
    }
    return endValue.valueOf() <= startValue.valueOf();
  };
  //控制开始日期弹窗显示
  handleStartOpenChange = open => {
    if (!open) {
      this.setState({ endOpen: true });
    }
  };
  //控制结束日期弹窗显示
  handleEndOpenChange = open => {
    this.setState({ endOpen: open });
  };
  //改变开始日期
  onStartChange = (date, dateString) => {
    if (this.props.onStartChange) {
      this.props.onStartChange(date, dateString);
    }
  };
  //改变结束日期
  onEndChange = (date, dateString) => {
    if (this.props.onEndChange) {
      this.props.onEndChange(date, dateString);
    }
  };
  //点击开始日期确定按钮
  onStartOk = date => {
    if (this.props.onStartOk) {
      this.props.onStartOk(date);
    }
  };
  //点击结束日期确定按钮
  onEndOk = date => {
    if (this.props.onEndOk) {
      this.props.onEndOk(date);
    }
  };
  render() {
    const { startDate, endDate } = this.props;
    return (
      <div style={{ display: 'inline-block', verticalAlign: 'top' }}>
        <DatePicker
          // style={{ marginLeft: '10px' }}
          disabledDate={this.disabledStartDate}
          showTime={this.props.showTime}
          allowClear={false}
          format={this.props.format ? this.props.format : 'YYYY-MM-DD'}
          locale={locale}
          value={moment(startDate)}
          placeholder="开始日期"
          onChange={this.onStartChange}
          onOpenChange={this.handleStartOpenChange}
          onOk={this.onStartOk}
        />
        <span style={{margin: '0 20px'}} >~</span>
        <DatePicker
          // style={{ marginLeft: '50px' }}
          disabledDate={this.disabledEndDate}
          showTime={this.props.showTime}
          allowClear={false}
          format={this.props.format ? this.props.format : 'YYYY-MM-DD'}
          locale={locale}
          value={moment(endDate)}
          placeholder="结束日期"
          onChange={this.onEndChange}
          open={this.state.endOpen}
          onOpenChange={this.handleEndOpenChange}
          onOk={this.onEndOk}
        />
      </div>
    );
  }
}

TimeFilter.defaultProps = defaultProps;
TimeFilter.propTypes = propTypes;

export default TimeFilter;
