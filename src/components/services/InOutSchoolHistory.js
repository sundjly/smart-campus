import React, { Component } from 'react'
import { Card, Row, Button, DatePicker, Icon, Menu, Pagination, Spin, message } from 'antd'
// import intl from "react-intl-universal";
import moment from 'moment'
import CommonModal from '../common/CommonModal'

import './InOutSchool.less'

// DatePicker语言设置
const locale = {
  lang: {
    placeholder: '请选择日期',
    rangePlaceholder: ['开始日期', '结束日期'],
    today: '今天',
    now: '现在',
    backToToday: 'Back to today',
    ok: '确定',
    clear: '清除',
    month: '月',
    year: '年',
    timeSelect: '时间选择',
    dateSelect: '日期选择',
    monthSelect: '选择一个月份',
    yearSelect: '选择一个年份',
    decadeSelect: '选择一个年份段',
    yearFormat: 'YYYY',
    dateFormat: 'M/D/YYYY',
    dayFormat: 'D',
    dateTimeFormat: 'M/D/YYYY HH:mm:ss',
    monthFormat: 'MMMM',
    monthBeforeYear: true,
    previousMonth: 'Previous month (PageUp)',
    nextMonth: 'Next month (PageDown)',
    previousYear: 'Last year (Control + left)',
    nextYear: 'Next year (Control + right)',
    previousDecade: 'Last decade',
    nextDecade: 'Next decade',
    previousCentury: 'Last century',
    nextCentury: 'Next century'
  },
  timePickerLocale: {
    placeholder: '请选择时间'
  }
}
class InOutSchoolHistory extends Component {
  constructor (props) {
    super(props)
    this.state = {
      updatedAttendanceParams: {
        teacherId: '',
        studentId: '',
        cState: '0'
      },
      activeTabKey: '0',
      isStateModalShow: false,
      // stateModalActiveKey: "0",
      modalOkLoading: false,
      date: moment().format('YYYY-MM-DD'),
      page: 1,
      pageSize: 30,
      spinning: false
    }
  }

  componentWillMount () {
    const { getStudentAttendanceRecord } = this.props
    this.setState({
      spinning: true
    }, () => {
      getStudentAttendanceRecord(this.getAttendanceRecordParams()).then(() => {
        this.setState({
          spinning: false
        })
      })
    })
  }

  onClickSetState (teacherId, studentId) {
    console.log('set state, teacherId, studentId', teacherId, studentId)
    if (this.isToday()) {
      this.setState({
        isStateModalShow: true,
        updatedAttendanceParams: Object.assign(this.state.updatedAttendanceParams, {
          teacherId,
          studentId
        })
      })
    } else {
      message.error('只能修改当天出勤状态')
    }
  };

  onClickSearchFace () {
    message.error('搜索功能暂未支持')
  };

  renderAttendanceList (data) {
    return data.map((v, i) => {
      return <div className='student-face-item' key={`state-${v.cState}-${i}`}>
        <div className='student-face-wrap'>
          <img
            className='student-face-img'
            src={v.cState === 3 || !v.photoData ? v.originalPhoto : v.photoData}
            alt='StudentFace'
          />
          <Icon type='setting' className='set-state-icon face-icon' onClick={this.onClickSetState.bind(this, v.teacherId, v.pId)} />
          <Icon type='search' className='face-search-icon face-icon' onClick={this.onClickSearchFace} />
        </div>
        <div className='student-face-btns'>
          <span className='student-face-name ellipsis' title={v.realName}>
            {v.realName}
          </span>
          <span className='student-face-num ellipsis' title={v.number}>
            {v.number}
          </span>
        </div>
      </div>
    })
  }

  getAttendanceRecordParams () {
    const { page, pageSize, date, activeTabKey } = this.state
    return {
      startTime: moment(date).format('YYYY-MM-DD') + ' 00:00:00',
      endTime: moment(date).format('YYYY-MM-DD') + ' 23:59:59',
      cState: activeTabKey,
      page,
      pageSize
    }
  }

  isToday (dateString) {
    return this.state.date === moment().format('YYYY-MM-DD')
  }

  onClickMenu (e) {
    console.log('click menu', e)
    this.setState({
      activeTabKey: e.key,
      page: 1,
      updatedAttendanceParams: Object.assign(this.state.updatedAttendanceParams, {
        cState: e.key
      }),
      spinning: true
    }, () => {
      this.props.getStudentAttendanceRecord(this.getAttendanceRecordParams())
        .then(() => {
          this.setState({
            spinning: false
          })
        })
    })
  };

  getTotalCount (data) {
    let total = 0
    data.forEach((v) => {
      total = total + Number(v)
    })
    return total
  }

  setPaddingTop (len) {
    const percent = 10 * (3 - Math.ceil(len / 10)) + '%'
    const px = 30 * (3 - Math.ceil(len / 10)) + 'px'
    return `calc(${percent} + ${px})`
  };

  onChangeAttendanceState (key) {
    console.log('change state', key)
    this.setState({
      updatedAttendanceParams: Object.assign(this.state.updatedAttendanceParams, {
        cState: key
      })
    })
  }

  onDateChange (date, dateString) {
    console.log('onEndChange', date, dateString)
    console.log('date 00:00', new Date(new Date(dateString).toLocaleDateString()))
    console.log('date 23:59', new Date(new Date(new Date(dateString).toLocaleDateString()).getTime() + 24 * 60 * 60 * 1000 - 1))
    this.setState({
      date: dateString,
      spinning: true
    }, () => {
      this.props.getStudentAttendanceRecord(this.getAttendanceRecordParams())
        .then(() => {
          this.setState({
            spinning: false
          })
        })
    })
  };

  onChangePage (page) {
    console.log('change page', page)
    this.setState({
      page: page,
      spinning: true
    }, () => {
      this.props.getStudentAttendanceRecord(this.getAttendanceRecordParams())
        .then(() => {
          this.setState({
            spinning: false
          })
        })
    })
  };

  render () {
    const { isHistoryShow, setHistoryShow, attendanceRecord, attendanceTotal, getStudentAttendanceRecord, updateStudentAttendance } = this.props
    const { isStateModalShow, updatedAttendanceParams, modalOkLoading, date, page, spinning, activeTabKey } = this.state
    const noData = <div className='student-list-noData'>暂无数据</div>
    // 修改出勤状态Modal配置
    const setAttendanceStateModal = {
      title: '更改出勤状态',
      visible: isStateModalShow,
      wrapClassName: 'attendance-state-modal',
      cancelCallback: () => {
        this.setState({
          isStateModalShow: false
        })
      },
      okCallback: () => {
        this.setState({
          modalOkLoading: true
        }, () => {
          updateStudentAttendance(updatedAttendanceParams)
            .then(res => {
              this.setState({
                isStateModalShow: false,
                modalOkLoading: false,
                updatedAttendanceParams: Object.assign(updatedAttendanceParams, {
                  cState: this.state.activeTabKey
                })
              })
              if (res && res.data && !res.errCode) {
                this.setState({
                  spinning: true
                }, () => {
                  getStudentAttendanceRecord(this.getAttendanceRecordParams()).then(() => {
                    this.setState({
                      spinning: false
                    })
                  })
                })
              } else {
                message.error('修改出勤状态失败')
              }
            })
        })
      },
      okText: '确定',
      cancelText: '取消',
      maskClosable: false,
      confirmLoading: modalOkLoading,
      content: (
        <div className='attendance-state-content'>
          <div className='attendance-state-line'>
            <Button type={updatedAttendanceParams.cState === '0' ? 'primary' : 'default'} key='0' className='attendance-state' onClick={this.onChangeAttendanceState.bind(this, '0')}>
              未到校
            </Button>
            <Button type={updatedAttendanceParams.cState === '1' ? 'primary' : 'default'} key='1' className='attendance-state' onClick={this.onChangeAttendanceState.bind(this, '1')}>
              已到校
            </Button>
          </div>
          <div className='attendance-state-line'>
            <Button type={updatedAttendanceParams.cState === '2' ? 'primary' : 'default'} key='2' className='attendance-state' onClick={this.onChangeAttendanceState.bind(this, '2')}>
              已离校
            </Button>
            <Button type={updatedAttendanceParams.cState === '3' ? 'primary' : 'default'} key='3' className='attendance-state' onClick={this.onChangeAttendanceState.bind(this, '3')}>
              请假
            </Button>
          </div>
        </div>
      )
    }
    return (
      <div>
        <Row
          className='inout-history-wrap'
          style={{
            maxWidth: '1000px',
            margin: '40px auto',
            display: isHistoryShow ? 'block' : 'none'
          }}
        >
          <CommonModal modalOption={setAttendanceStateModal} />
          <Card title={'基本信息'} bordered={false}>
            <Icon
              type='close'
              className='inout-history-close-btn'
              onClick={setHistoryShow.bind(this, false)}
            />
            <Row>
              <div className='inout-history-header'>
                <div className='inout-history-capture-count'>识别人数：{this.getTotalCount(attendanceTotal)}</div>
              </div>
              <div style={{ display: 'inline-block', verticalAlign: 'top' }}>
                选择日期：
                <DatePicker
                  locale={locale}
                  onChange={this.onDateChange.bind(this)}
                  value={moment(date)}
                  allowClear={false}
                />
              </div>
            </Row>
            <Row>
              <div className='inout-history-content'>
                <Menu
                  defaultSelectedKeys={['0']}
                  mode='horizontal'
                  onClick={this.onClickMenu.bind(this)}
                >
                  <Menu.Item key='0'>未到校（{attendanceTotal[0]}）</Menu.Item>
                  <Menu.Item key='1'>已到校（{attendanceTotal[1]}）</Menu.Item>
                  <Menu.Item key='2'>已离校（{attendanceTotal[2]}）</Menu.Item>
                  <Menu.Item key='3'>请假（{attendanceTotal[3]}）</Menu.Item>
                </Menu>
                <div className='inout-history-student-list'>
                  {attendanceRecord.length > 0 && this.renderAttendanceList(attendanceRecord)}
                  {attendanceRecord.length === 0 && !spinning && noData}
                  <Spin
                    className='loading-center zi1060'
                    spinning={spinning}
                  />
                  <Pagination
                    className='student-list-pagination'
                    style={{
                      opacity: attendanceRecord.length ? '1' : '0',
                      paddingTop: this.setPaddingTop(attendanceRecord.length)
                    }}
                    defaultCurrent={1}
                    total={attendanceTotal[activeTabKey]}
                    current={page}
                    defaultPageSize={30}
                    onChange={this.onChangePage.bind(this)}
                  />
                </div>
              </div>
            </Row>
          </Card>
        </Row>
      </div>
    )
  }
}
export default InOutSchoolHistory
