import React from 'react';
import { Form, Icon, Input, Button, Checkbox} from 'antd';
import PropsTypes from 'prop-types';
import { browserType } from '../../utils/global';
import { keepLogin } from '../../utils/config';
import './Login.less';
const FormItem = Form.Item;

const defaultProps = {
  username:'',
  password:'',
  isError:'',
  errorMsg:'',
};

const propTypes ={
  username: PropsTypes.string.isRequired,
  password: PropsTypes.string.isRequired,
  isError: PropsTypes.bool.isRequired,
  errorMsg: PropsTypes.string.isRequired,
  loginType: PropsTypes.string.isRequired,
  initAuth: PropsTypes.func.isRequired
};

class Login extends React.Component {
  state = {
    downloadChrome: false
  };
  componentWillMount() {
    console.log('sdj, this.props', this.props);
    const { initAuth } = this.props;
    initAuth(this.props);
    let browser_type = browserType();
    if (browser_type !== 'Chrome') {
      this.setState({ downloadChrome: true });
    }
  }
    // componentWillMount() {
    //     const { receiveData } = this.props;
    //     receiveData(null, 'auth');
    // }
    // componentWillReceiveProps(nextProps) {
    //     const { auth: nextAuth = {} } = nextProps;
    //     const { history } = this.props;
    //     if (nextAuth.data && nextAuth.data.uid) {   // 判断是否登陆
    //         localStorage.setItem('user', JSON.stringify(nextAuth.data));
    //         history.push('/');
    //     }
    // }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
                const { login } = this.props;
                // if (values.userName === 'admin' && values.password === 'admin') fetchData({funcName: 'admin', stateName: 'auth'});
                // if (values.userName === 'guest' && values.password === 'guest') fetchData({funcName: 'guest', stateName: 'auth'});
                login(values, this.props);

            }
        });
    };

    render() {
        const { getFieldDecorator } = this.props.form;
        const {
          username,
          password,
        } = this.props;
        return (
          <div  id="login-content" className="login">
              <div className="login-form" >
                <div className="login-logo">
                  <span>智慧校园后台管理系统</span>
                </div>
                <Form onSubmit={this.handleSubmit} style={{maxWidth: '300px'}}>
                  <FormItem>
                    {getFieldDecorator('userName', {
                      rules: [{ required: true, message: '请输入用户名!' }],
                      initialValue: username

                    })(
                      <Input
                        prefix={<Icon type="user" style={{ fontSize: 13 }} />}
                        placeholder="请输入用户名"
                      />
                    )}
                  </FormItem>
                  <FormItem>
                    {getFieldDecorator('password', {
                      rules: [{ required: true, message: '请输入密码!' }],
                      initialValue: password
                    })(
                      <Input
                        prefix={<Icon type="lock" style={{ fontSize: 13 }} />}
                        type="password" placeholder="请输入密码"

                      />
                    )}
                  </FormItem>
                  <FormItem>
                    {getFieldDecorator('remember', {
                      valuePropName: 'checked',
                      // initialValue: true,
                      initialValue: keepLogin
                    })(
                      <Checkbox>记住我</Checkbox>
                    )}
                    <a className="login-form-forgot" href="" style={{float: 'right'}}>忘记密码</a>
                    <Button type="primary" htmlType="submit" className="login-form-button" style={{width: '100%'}}>
                      登录
                    </Button>
                    <p style={{display: 'flex', justifyContent: 'space-between'}}>
                      {/* <a href="">或 现在就去注册!</a> */}
                      {/* <a onClick={this.gitHub} ><Icon type="github" />(第三方登录)</a> */}
                    </p>
                  </FormItem>
                </Form>
              </div>
              <footer className="footer copyright">
                <p>intellif.com All Rights Reserved.</p>
              </footer>
            </div>
        );
    }
}

// const mapStateToPorps = state => {
//     const { auth } = state.httpData;
//     return { auth };
// };
// const mapDispatchToProps = dispatch => ({
//     // fetchData: bindActionCreators(fetchData, dispatch),
//     // receiveData: bindActionCreators(receiveData, dispatch)
// });

Login.defaultProps = defaultProps;
Login.propTypes = propTypes;

export default Form.create()(Login);