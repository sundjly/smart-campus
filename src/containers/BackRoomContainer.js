import { connect } from "react-redux";
import BackRoomManagement from "../components/services/BackRoomManager/BackRoomManager";
import {
  getInOutSchoolOption,
  setInOutSchoolOption,
  updateInOutSchoolOption,
  getStudentAttendanceRecord,
  updateStudentAttendance
} from "../actions/BackRoomAction";
import {
  getMeetingList,
  getMeetingDetail,
  cancelMeeting,
  deleteMeeting,
  createMeeting,
  updateMeeting,
  updateDetail
} from "../actions/MeetingManagementAction";

const mapstateToProps = state => ({
  ...state.meetingManagementReducer,
  ...state.appReducer,
  ...state.backRoomReducer,
});

export default connect(mapstateToProps, {
  getMeetingList,
  getMeetingDetail,
  cancelMeeting,
  deleteMeeting,
  createMeeting,
  updateMeeting,
  updateDetail,
  getInOutSchoolOption,
  setInOutSchoolOption,
  updateInOutSchoolOption,
  getStudentAttendanceRecord,
  updateStudentAttendance
})(BackRoomManagement);
