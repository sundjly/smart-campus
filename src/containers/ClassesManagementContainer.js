import { connect } from 'react-redux';
import ClassesManagement from '../components/basicInfoManagement/classesManagement/ClassesManagement';
import {
    getGradeTreeList_dispatch,
    addGradeOrClass_dispatch,
    editGradeOrClass_dispatch,
    deleteGradeOrClass_dispatch
} from '../actions/ClassesManagementAction';

const mapstateToProps = state => (
    {
        ...state.classesManagementReducer
    }
);

export default connect(mapstateToProps,
    {
        getGradeTreeList_dispatch,
        addGradeOrClass_dispatch,
        editGradeOrClass_dispatch,
        deleteGradeOrClass_dispatch
    })(ClassesManagement);

