import { connect } from 'react-redux';
import FacilitiesTypeManagement from '../components/basicInfoManagement/facilitiesTypeManagement/FacilitiesTypeManagement';
import {
    getFacilitiesTypeList_dispatch,
    addFacilitiesType_dispatch,
    modifyFacilitiesType_dispatch,
    deleteFacilitiesType_dispatch
} from '../actions/FacilitiesTypeManagementAction';

const mapstateToProps = state => (
    {
        ...state.facilitiesTypeManagementReducer
    }
);

export default connect(mapstateToProps, {
    getFacilitiesTypeList_dispatch,
    addFacilitiesType_dispatch,
    modifyFacilitiesType_dispatch,
    deleteFacilitiesType_dispatch
})(FacilitiesTypeManagement);

