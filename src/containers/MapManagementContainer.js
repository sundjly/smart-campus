import { connect } from 'react-redux';
import MapManagement from '../components/basicInfoManagement/mapManagement/MapManagement';
import {
    getMapList_dispatch,
    deleteMap_dispatch
} from '../actions/MapManagementAction';

const mapstateToProps = state => (
    {
        ...state.mapManagementReducer
    }
);

export default connect(mapstateToProps, {
    getMapList_dispatch,
    deleteMap_dispatch,
})(MapManagement);

