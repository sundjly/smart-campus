import {connect} from 'react-redux';
import Visitor from '../components/visitorManagement/Visitor';
import {
  getVisitorList,
  changeSpining,
} from '../actions/visitorManagement/VisitorAction';


const mapstateToProps = state=> (
  {
    ...state.vistorReducer,
  }
);

export default connect(mapstateToProps, {
  getVisitorList,
  changeSpining,
})(Visitor);