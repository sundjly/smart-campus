import { connect } from "react-redux";
import AccountManager from "../components/systemAccount/AccountManager/AccountManager";
import {
    getAllUserAccount,
    getRoleList,
    reviseAccount,
    addAccount,
    deleteAccount
} from "../actions/AccountManagerAction";

const mapstateToProps = state => ({
    ...state.accountManagerReducer
});

export default connect(mapstateToProps, {
    getAllUserAccount,
    getRoleList,
    reviseAccount,
    addAccount,
    deleteAccount
})(AccountManager);
