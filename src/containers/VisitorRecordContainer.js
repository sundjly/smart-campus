import {connect} from 'react-redux';
import VisitorRecord from '../components/visitorManagement/VisitorRecord';
import {
  getVisitorRecord,
  changeSpining,
} from '../actions/visitorManagement/VisitorRecordAction';

const mapstateToProps = state=> (
  {
    ...state.visitorRecordReducer,
  }
);

export default connect(mapstateToProps, {
  getVisitorRecord,
  changeSpining,
})(VisitorRecord);