import {connect} from 'react-redux';
import NewStudentsManagement from '../components/userManagement/studentManagement/NewStudentsManagement';
import {

} from '../actions/userManagement/NewStudentsManagementAction';

const mapstateToProps = state => (
  {
    ...state.studentsManagementReducer,
    classGrade: state.appReducer.classGrade
  }
);

export default connect(mapstateToProps,{


})(NewStudentsManagement);

