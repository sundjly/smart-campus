import { connect } from 'react-redux';
import AddOrEditMap from '../components/basicInfoManagement/mapManagement/AddOrEditMap';
import {
    addOrEditMap_dispatch,
    uploadMap_dispatch
} from '../actions/MapManagementAction';
import {
    getFacilitiesList_dispatch
} from '../actions/FacilitiesManagementAction';

const mapstateToProps = state => (  
    {
        facilitiesList: state.facilitiesManagementReducer.facilitiesList,
        ...state.mapManagementReducer
    }
);

//connect 里面有三个参数。

export default connect(mapstateToProps,
    {
        getFacilitiesList_dispatch,
        addOrEditMap_dispatch,
        uploadMap_dispatch
    }
)(AddOrEditMap);

