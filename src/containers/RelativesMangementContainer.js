import { connect } from 'react-redux';
import RelativeManagement from '../components/userManagement/RelativesMangement';

import {
  changeModalUpload,
  submitTemplate_dispatch,
  multiRegisterProgress,
  getProgressErrorUrl,
  operateModal_dispatch,
  cancelUpLoadModal,
  getRelativesList,

}from '../actions/userManagement/RelativesMangementAction';

const mapStateToProps =state => (
  {
    ...state.relativesMangementReducer
  }
)

export default connect(mapStateToProps, {
  changeModalUpload,
  submitTemplate_dispatch,
  multiRegisterProgress,
  getProgressErrorUrl,
  operateModal_dispatch,
  cancelUpLoadModal,
  getRelativesList,

})(RelativeManagement);