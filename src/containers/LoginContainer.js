import { connect } from 'react-redux';
import Login from '../components/pages/Login';
import { initAuth, login, changeLoginType } from '../actions/LoginAction';

const mapStateToProps = state => ({
  ...state.loginReducer,
  // username: state.loginReducer.username,
  // password: state.loginReducer.password,
  // isError: state.loginReducer.isError,
  // errorMsg: state.loginReducer.errorMsg,
  // loginType: state.loginReducer.loginType
});

export default connect(mapStateToProps, {
  initAuth,
  login,
  changeLoginType
})(Login);


