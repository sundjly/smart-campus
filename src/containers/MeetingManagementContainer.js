import { connect } from 'react-redux'
import MeetingManagement from '../components/services/MeetingManagement'
import {
  getMeetingList,
  getMeetingDetail,
  cancelMeeting,
  deleteMeeting,
  createMeeting,
  updateMeeting,
  updateDetail,
  searchTeacher
} from '../actions/MeetingManagementAction'

const mapstateToProps = state => ({
  ...state.meetingManagementReducer,
  ...state.appReducer
})

export default connect(mapstateToProps, {
  getMeetingList,
  getMeetingDetail,
  cancelMeeting,
  deleteMeeting,
  createMeeting,
  updateMeeting,
  updateDetail,
  searchTeacher
})(MeetingManagement)
