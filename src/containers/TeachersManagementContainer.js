import { connect } from 'react-redux';
import TeachersManagement from '../components/userManagement/TeachersManagement';

import {
  changeModalUpload,
  submitTemplate_dispatch,
  multiRegisterProgress,
  getProgressErrorUrl,
  operateModal_dispatch,
  cancelUpLoadModal,
  getRelativesList,

}from '../actions/userManagement/TeachersManagementAction';

const mapStateToProps =state => (
  {
    ...state.teachersManagementReducer
  }
)

export default connect(mapStateToProps, {
  changeModalUpload,
  submitTemplate_dispatch,
  multiRegisterProgress,
  getProgressErrorUrl,
  operateModal_dispatch,
  cancelUpLoadModal,
  getRelativesList,

})(TeachersManagement);