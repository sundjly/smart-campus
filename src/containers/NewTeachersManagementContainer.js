import {connect} from 'react-redux';
import NewTeachersManagement from '../components/userManagement/teachersManagement/NewTeachersManagement';
import {
  addTeachersFormSubmit,
  pushStudent,
} from '../actions/userManagement/NewTeachersManagementAction';

const mapStateToProps = state => (
  {
    ...state.newTeachersManagementReducer,
    studentsTree: state.appReducer.classGrade
  }
);

export default connect (mapStateToProps, {
  addTeachersFormSubmit,
  pushStudent,

})(NewTeachersManagement);