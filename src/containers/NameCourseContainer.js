import { connect } from "react-redux";
import NameCourseManager from "../components/services/NameCourseManager/NameCourseManager";
import {
  getInOutSchoolOption,
  setInOutSchoolOption,
  updateInOutSchoolOption,
  getStudentAttendanceRecord,
  updateStudentAttendance,
  getCourseList
} from "../actions/NameCourseAction";

const mapstateToProps = state => ({
  ...state.appReducer,
  ...state.nameCourseReducer,
});

export default connect(mapstateToProps, {
  getInOutSchoolOption,
  setInOutSchoolOption,
  updateInOutSchoolOption,
  getStudentAttendanceRecord,
  updateStudentAttendance,
  getCourseList
})(NameCourseManager);
