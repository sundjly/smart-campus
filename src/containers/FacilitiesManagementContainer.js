import { connect } from 'react-redux';
import FacilitiesManagement from '../components/basicInfoManagement/facilitiesManagement/FacilitiesManagement';
import { getFacilitiesTypeList_dispatch } from '../actions/FacilitiesTypeManagementAction';
import {
    getFacilitiesList_dispatch,
    addFacilities_dispatch,
    modifyFacilities_dispatch,
    deleteFacilities_dispatch,
} from '../actions/FacilitiesManagementAction';

const mapstateToProps = state => (
    {
        facilitiesTypeList: state.facilitiesTypeManagementReducer.facilitiesTypeList,
        ...state.facilitiesManagementReducer
    }
);

export default connect(mapstateToProps, {
    getFacilitiesList_dispatch,
    addFacilities_dispatch,
    modifyFacilities_dispatch,
    deleteFacilities_dispatch,
    getFacilitiesTypeList_dispatch
})(FacilitiesManagement);

