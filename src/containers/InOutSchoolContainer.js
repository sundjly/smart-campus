import { connect } from 'react-redux';
import InOutSchool from '../components/services/InOutSchool';
import { 
    getInOutSchoolOption, 
    setInOutSchoolOption, 
    updateInOutSchoolOption,
    getStudentAttendanceRecord,
    updateStudentAttendance,
    creatSchoolOption,
} from '../actions/InOutSchoolAction';

const mapstateToProps = state => (
    {
        ...state.inOutSchoolReducer,
        ...state.appReducer
    }
);

export default connect(mapstateToProps, {
    getInOutSchoolOption,
    setInOutSchoolOption,
    updateInOutSchoolOption,
    getStudentAttendanceRecord,
    updateStudentAttendance,
    creatSchoolOption,

})(InOutSchool);

