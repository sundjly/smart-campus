import {connect} from 'react-redux';
import StudentsManagement from '../components/userManagement/StudentsManagement';
import {
  submitTemplate_dispatch,
  changeModalUpload,
  getClassGradeList,
  getStudentsList,
  exportExcel,
  deleteStudentInfo,
  operateModal_dispatch,
  cancelUpLoadModal,
} from '../actions/userManagement/StudentsManagementAction';

const mapstateToProps = state => (
  {
    ...state.studentsManagementReducer,
    classGrade: state.appReducer.classGrade,
  }
);

export default connect(mapstateToProps,{
  submitTemplate_dispatch,
  changeModalUpload,
  getClassGradeList,
  getStudentsList,
  exportExcel,
  deleteStudentInfo,
  operateModal_dispatch,
  cancelUpLoadModal,

})(StudentsManagement);

