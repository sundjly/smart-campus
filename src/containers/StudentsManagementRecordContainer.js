import { connect } from 'react-redux';
import StudentsManagementRecord from '../components/userManagement/studentManagement/StudentsManagementRecord';
import {
  getStudentRecord,

} from '../actions/userManagement/StudentsManagementRecordAction';

const mapstateToProps = state => (
  {
    ...state.studentsManagementRecordReducer
  }
);

export default connect(mapstateToProps,{
  getStudentRecord,

})(StudentsManagementRecord);