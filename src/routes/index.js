import React, {Component} from "react";
// import { Router, Route, hashHistory, IndexRedirect } from 'react-router';
import {Route, Redirect, Switch} from "react-router-dom";

import NewRoleManagement from "../components/systemAccount/NewRoleManagement";
import RoleManagement from "../components/systemAccount/RoleManagement";
import OperationLog from "../components/systemSetting/OperationLog";
import MessageTemplate from "../components/systemSetting/MessageTemplate";
import Dashboard from "../components/dashboard/Dashboard";
import PhoneStudentsRecord from "../components/PhoneStudentsRecord";
import PhoneAllStudentsRecord from "../components/PhoneAllStudentsRecord";

import InOutSchoolManagement from "../containers/InOutSchoolContainer";
import MeetingManagement from "../containers/MeetingManagementContainer";
import BackRoom from "../containers/BackRoomContainer";
import NameCourse from "../containers/NameCourseContainer";
import AccountManager from "../containers/AccountManagerContainer";

import Visitor from '../containers/VisitorContainer.js';
import VisitorRecord from '../containers/VisitorRecordContainer.js';


import ClassesManagement from '../containers/ClassesManagementContainer';
import FacilitiesTypeManagement from '../containers/FacilitiesTypeManagementContainer';
import FacilitiesManagement from '../containers/FacilitiesManagementContainer';
import MapManagement from '../containers/MapManagementContainer';
import AddOrEditMap from '../containers/AddOrEditMapContainer';
import StudentsManagement from "../containers/StudentsManagementContainer";
import NewStudentsManagement from "../containers/NewStudentsManagementContainer";
import StudentsManagementRecord from "../containers/StudentsManagementRecordContainer";
import RelativesMangementContainer from '../containers/RelativesMangementContainer';
import TeachersManagementContainer from '../containers/TeachersManagementContainer';
import NewTeachersManagementContainer from '../containers/NewTeachersManagementContainer';

export default class CRouter extends Component {
  requireAuth = (permission, component) => {
    const {auth} = this.props;
    const {permissions} = auth.data;
    // const { auth } = store.getState().httpData;
    if (!permissions || !permissions.includes(permission))
      return <Redirect to={"404"}/>;
    return component;
  };

  render() {
    return (
      <Switch>
        {/*首页*/}
        <Route exact path="/app/dashboard/index" component={Dashboard}/>

        {/*系统设置 start*/}
        <Route exact path="/app/systemAccount/accountManager" component={AccountManager} />
        <Route exact path="/app/systemAccount/RoleManagement" component={RoleManagement}/>
        <Route exact path="/app/systemAccount/newRoleManagement" component={NewRoleManagement}/>
        <Route exact path="/app/systemSetting/OperationLog" component={OperationLog}/>
        <Route exact path="/app/systemSetting/MessageTemplate" component={MessageTemplate}/>
        {/*系统设置 end*/}

        {/*用户管理*/}
        <Route exact path="/app/userManagement/studentsManagement" component={StudentsManagement}/>
        <Route exact path="/app/userManagement/newStudentsManagement" component={NewStudentsManagement}/>
        <Route exact path="/app/userManagement/studentsManagementRecord" component={StudentsManagementRecord}/>
        <Route exact path="/app/userManagement/relativesManagement" component={RelativesMangementContainer}/>
        <Route exact path="/app/userManagement/teacherManagement" component={TeachersManagementContainer}/>
        <Route exact path="/app/userManagement/newTeacherManagement" component={NewTeachersManagementContainer}/>

        {/*访客管理*/}

        <Route exact path="/app/visitorManagement/visitor" component={Visitor}/>
        <Route exact path="/app/visitorManagement/visitorRecord" component={VisitorRecord}/>

        {/* 业务管理 start */}
        <Route exact path="/app/service/inOut" component={InOutSchoolManagement}/>
        <Route exact path="/app/service/meeting" component={MeetingManagement}/>
        <Route exact path="/app/service/map" component={PhoneStudentsRecord}/>
        <Route exact path="/app/service/appSchoolRecord" component={PhoneAllStudentsRecord}/>
        <Route exact path="/app/service/backRoom" component={BackRoom}/>
        <Route exact path="/app/service/nameCourse" component={NameCourse}/>

        {/* 业务管理 end */}

        {/*基础信息管理*/}
        <Route exact path="/app/basicInfoManagement/classesManagement" component={ClassesManagement}/>
        <Route exact path="/app/basicInfoManagement/facilitiesTypeManagement" component={FacilitiesTypeManagement}/>
        <Route exact path="/app/basicInfoManagement/facilitiesManagement" component={FacilitiesManagement}/>
        <Route exact path="/app/basicInfoManagement/mapManagement" component={MapManagement}/>
        <Route exact path="/app/basicInfoManagement/addOrEditMap" component={AddOrEditMap}/>

        <Route render={() => <Redirect to="/404"/>}/>
      </Switch>
      )
    ;
  }
}
