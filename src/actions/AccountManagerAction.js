/* 出入校管理 actions */
import * as types from "../constants/ActionTypes";
import request from "../utils/request";
import { api } from "../constants/API";
import { message } from "antd";

//新增账号
export const addAccount = (params, setActivePage) => {
    return dispatch => {
        return request({
            url: api.accountBaseUrl,
            method: "post",
            data: params
        }).then(res => {
            console.log(res);
            if (res && res.data && !res.errCode) {
                dispatch(getAllUserAccount());
                setActivePage("list");
            } else if (res.errCode) {
                message.error(res.data);
            }
        });
    };
};

//修改账号
export const reviseAccount = (params, setActivePage) => {
    return dispatch => {
        return request({
            url: api.accountBaseUrl,
            method: "put",
            data: params
        }).then(res => {
            console.log(res);
            if (res && res.data && !res.errCode) {
                dispatch(getAllUserAccount());
                setActivePage("list");
            } else if (res.errCode) {
                message.error(res.data);
            }
        });
    };
};

//删除账号
export const deleteAccount = id => {
    return dispatch => {
        return request({
            url: api.accountBaseUrl + "/" + id,
            method: "DELETE",
            data: {}
        }).then(res => {
            console.log(res);
            if (res && res.data && !res.errCode) {
                dispatch(getAllUserAccount());
            } else if (res.errCode) {
                message.error(res.data);
            }
        });
    };
};

export const getAllUserAccount = (page, pageSize, name, roleId) => {
    console.log(page, pageSize);
    return dispatch => {
        return request({
            url: api.accountBaseUrl + "/search",
            method: "post",
            data: {
                page: page || 1,
                pageSize: pageSize || 10,
                login: "",
                name: name || "",
                roleIds: roleId || ""
            }
        }).then(res => {
            console.log(res);
            let UserAccountList = [];
            if (res && res.data && !res.errCode) {
                let total = res.total;
                for (var i = 0, item = res.data; i < item.length; i++) {
                    var target = {};
                    Object.assign(target, item[i][0], item[i][1]);
                    UserAccountList.push(target);
                }
                dispatch({
                    type: types.GET_ALL_USER_ACCOUNT,
                    payload: {
                        UserAccountList,
                        total
                    }
                });
            } else if (res.total == 0 && !res.errCode) {
                dispatch({
                    type: types.GET_ALL_USER_ACCOUNT,
                    payload: {
                        UserAccountList: [],
                        total: 0
                    }
                });
            } else if (res.errCode) {
                message.error(res.data);
            }
        });
    };
};

//查询角色
export const getRoleList = () => {
    return dispatch => {
        return request({
            url: api.getRoleList,
            method: "post",
            data: {
                page: 0,
                pageSize: 1000
            }
        }).then(res => {
            console.log(res);
            if (res && res.data && !res.errCode) {
                dispatch({
                    type: types.GET_ALL_ROLES,
                    payload: {
                        roleList: res.data
                    }
                });
            } else if (res.errCode) {
                message.error(res.data);
            }
        });
    };
};
