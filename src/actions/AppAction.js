/* 全局APP actions */
import * as types from '../constants/ActionTypes';
import request from '../utils/request';
import { api } from '../constants/API';
import { message } from 'antd';

// 获取全部摄像头数据
export const getAllCameras = () => {
  return dispatch => {
    return request({
      url: api.getAllCameras,
      method: 'get',
      data: {}
    }).then(res => {
      console.log('all cameras res', res);
      if(res && res.data && !res.errCode){
        let cameras = {};
        res.data.forEach((v) => {
          cameras[v.id] = v;
        })
        dispatch({
          type: types.GET_ALL_CAMERAS,
          payload: {
            cameras
          }
        })
      }else{
        message.error('获取摄像头数据失败')
      }
    })
  }
};