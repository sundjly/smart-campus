import request from '../utils/request';
import { message } from 'antd';
import { api } from '../constants/API';
import * as types from '../constants/ActionTypes';

//获取年级列表
export const getGradeTreeList_dispatch = () => dispatch => {
    dispatch({
        type: types.TOGGLE_SPIN,
        payload: {
            spinning: true
        }
    });
    return request({
        url: api.getGradeTreeList,
        method: 'get',
        data: {},
    }).then(res => {
        if (res && res.errCode === 0) {
            const data = res.data && res.data.length ? res.data : [];
            //把childList字段替换成children
            let newData = replaceChildNode(data, /childList/g, 'children');
            //把id字段替换成key
            newData = replaceChildNode(newData, /id/g, 'key');
            //把父级名字塞进子级对象
            newData.map((item, index, array) => {
                Array.isArray(item.children) && item.children.map((v, i, arr) => {
                    v.parentName = item.name;
                });
            });

            dispatch({
                type: types.RENDER_GRADE_TREE_LIST,
                payload: {
                    gradeTreeList: newData,
                    spinning: false
                }
            })
        } else {
            dispatch({
                type: types.TOGGLE_SPIN,
                payload: {
                    spinning: false
                }
            })
            message.error(`服务器出现错误，错误代码为${res.errCode}`);
        }
    })
};

/* 将数组对象树中子节点的名称childList替换成children   antd的table树组件用到的 */
const replaceChildNode = (data, reg, newName) => {
    let newData = '';
    if (Object.prototype.toString.call(data) == '[object Array]') {
        let dataStr = JSON.stringify(data);
        newData = dataStr.replace(reg, newName);
        return JSON.parse(newData);
    } else {
        return data;
    }
}
//添加班级或年级
export const addGradeOrClass_dispatch = options => dispatch => {
    return request({
        url: api.addGradeOrClass,
        method: 'post',
        data: options
    }).then(res => {
        if (res && res.errCode === 0) {
            message.success(`新增成功!`);
        } else if (res.errCode === 1001) {
            message.error(`${res.data}!`);
        } else {
            message.error(`${res.data}`);
        }
        return res.errCode;
    })
};
//编辑班级或年级
export const editGradeOrClass_dispatch = option => dispatch => {
    return request({
        url: api.operateGradeOrClass + option.id,
        method: 'put',
        data: option
    }).then(res => {
        if (res && res.errCode === 0) {
            message.success(`修改成功!`);
        } else if (res && res.errCode === 1001) {
            message.error(res.data);
        } else {
            message.error(`服务器出现错误，错误代码为${res.errCode}`);
        }
        return res.errCode;
    })
}

//删除班级或年级
export const deleteGradeOrClass_dispatch = option => dispatch => {
    return request({
        url: api.operateGradeOrClass + option.id,
        method: 'delete',
        data: {}
    }).then(res => {
        if (res && res.errCode === 0) {
            message.success(`删除成功！`);
        } else if (res && res.errCode === 1001) {
            message.error(`${res.data}`);
        } else {
            message.error(`服务器出现错误，错误代码为${res.errCode}`);
        }
        return res.errCode;
    })
}


