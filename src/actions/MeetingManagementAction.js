/* 出入校管理 actions */
import * as types from '../constants/ActionTypes'
import request from '../utils/request'
import { api } from '../constants/API'
import { message } from 'antd'

export const getMeetingList = data => {
  return dispatch => {
    return request({
      url: api.getMeetingList,
      method: 'post',
      data
    }).then(res => {
      if (res && res.data && !res.errCode) {
        dispatch({
          type: types.UPDATE_MEETING_LIST,
          payload: {
            meetingList: res.data,
            meetingTotal: res.total
          }
        })
        return res
      } else {
        message.error('获取会议列表失败!')
      }
    })
  }
}

export const getMeetingDetail = (data, stateId) => {
  return dispatch => {
    return request({
      url: api.getMeetingDetail,
      method: 'get',
      data
    }).then(res => {
      if (res && res.data && !res.errCode) {
        dispatch({
          type: types.UPDATE_MEETING_PERSON_LIST,
          payload: {
            meetingPersonList: res.data[stateId]
          }
        })
      } else {
        message.error('获取会议人员列表失败')
      }
    })
  }
}

export const cancelMeeting = data => {
  return dispatch => {
    return request({
      url: api.cancelMeeting,
      method: 'put',
      data
    }).then(res => {
      return res
    })
  }
}

export const deleteMeeting = data => {
  return dispatch => {
    return request({
      url: api.deleteMeeting,
      method: 'delete',
      data
    }).then(res => {
      return res
    })
  }
}

export const updateMeeting = data => {
  return dispatch => {
    return request({
      url: api.updateMeeting,
      method: 'post',
      data
    }).then(res => {
      return res
    })
  }
}

export const createMeeting = data => {
  return dispatch => {
    return request({
      url: api.createMeeting,
      method: 'post',
      data
    }).then(res => {
      return res
    })
  }
}

export const updateDetail = payload => {
  return dispatch => {
    dispatch({
      type: types.UPDATE_MEETING_DETAIL,
      payload
    })
  }
}

export const searchTeacher = data => {
  return dispatch => {
    return request({
      url: api.queryStudents, // 暂时使用搜索学生接口作为测试，后续改为老师接口
      method: 'post',
      data
    }).then(res => {
      return res
    })
  }
}
