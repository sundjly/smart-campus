import request from '../utils/request';
import { message } from 'antd';
import { api } from '../constants/API';
import * as types from '../constants/ActionTypes';

//获取地图列表
export const getMapList_dispatch = (requestData) => dispatch => {
    dispatch({
        type: types.TOGGLE_MAP_LIST_SPIN,
        payload: { spinning: true }
    });
    return request({
        url: api.getMapList,
        method: 'post',
        data: requestData,
    }).then(
        res => {
            if (res.errCode === 0) {
                if (Array.isArray(res.data) && res.data.length > 0) {
                    dispatch({
                        type: types.RENDER_MAP_LIST,
                        payload: {
                            mapList: res.data
                        }
                    });
                }
                dispatch({
                    type: types.TOGGLE_MAP_LIST_SPIN,
                    payload: { spinning: false }
                });
            } else {
                dispatch({
                    type: types.TOGGLE_MAP_LIST_SPIN,
                    payload: {
                        mapList: [],
                        spinning: false
                    }
                });
                message.error("获取地图列表失败!")
            }
            return res.errCode;
        }
    );

};
//新增或编辑地图
export const addOrEditMap_dispatch = (requestData) => dispatch => {
    return request({
        url: api.addOrEditMap,
        method: 'post',
        data: requestData,
    }).then(
        res => {
            if (res.errCode === 0) {
                requestData.mapId ? message.success("编辑地图成功!") : message.success("新增地图成功!")
            } else {
                requestData.mapId ? message.error("编辑地图失败!") : message.error("新增地图失败!")
            }
            return res.errCode;
        }
    );

};
//删除地图
export const deleteMap_dispatch = (id) => dispatch => {
    return request({
        url: api.deleteMap + id,
        method: 'delete',
        data: {},
    }).then(
        res => {
            if (res.errCode === 0) {
                message.success("删除地图成功!")
            } else {
                message.error("删除地图失败!")
            }
            return res.errCode;
        }
    );

};

//上传地图
export const uploadMap_dispatch = (requestData) => dispatch => {
    return request({
        url: api.uploadMap,
        method: 'upload',
        data: requestData,
    }).then(
        res => {
            if (res.errCode === 0) {
                message.success("上传地图成功!")
                return res.data;
            } else {
                message.error("上传地图失败!")
                return null;
            }
            
        }
    );

};
