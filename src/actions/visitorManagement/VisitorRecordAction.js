import * as types from '../../constants/ActionTypes';
import request from '../../utils/request';
import { api } from '../../constants/API';
import { message } from 'antd';

export const getVisitorRecord = (data, isFirst) => dispatch => {
  if(!isFirst){
    dispatch({
      type: types.VISITOR_MANAGEMENT,
      payload:{
        viewListSpinning: true
      }
    })
  }

  return request({
    url:api.getVisitorList,
    method:'post',
    data:data
  }).then(res => {
    if(!res.errCode){
      dispatch({
        type: types.VISITOR_MANAGEMENT,
        payload:{
          viewListSpinning: false,
          visitorsList: res.data
        }
      })
    }else{
      dispatch({
        type: types.VISITOR_MANAGEMENT,
        payload:{
          viewListSpinning: false,
          visitorsList: []
        }
      })
    }
  })
}


export const changeSpining = val => dispatch => {
  dispatch({
    type: types.VISITOR_MANAGEMENT,
    payload:{
      viewListSpinning: val
    }
  })
}