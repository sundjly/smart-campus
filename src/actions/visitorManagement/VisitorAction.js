import * as types from '../../constants/ActionTypes';
import request from '../../utils/request';
import { api } from '../../constants/API';
import { message } from 'antd';

// getVisitorList 获取访客访问记录列表。
// action 中的方法给组件调用。

export const getVisitorList = (data, isFirst) => dispatch => {
  if(!isFirst){
    dispatch({
      type: types.VISITOR_RECORD,
      payload:{
        viewListSpinning: true
      }
    })
  }
  //  pType=5
  //payload中可以传递参数。
  return request({
    url: api.queryStudents,
    method: 'post',
    data: data
  }).then(res => {
    if(!res.errCode && res.data && res.data.length){
      dispatch({
        type: types.VISITOR_RECORD,
        payload:{
          visitorList: res.data,
          viewListSpinning: false,
          total: res.total
        }
      })
    }else{
      dispatch({
        type: types.VISITOR_RECORD,
        payload:{
          visitorList: [],
          viewListSpinning: false,
          total: 0
        }
      })
    }
  })
}


export const changeSpining = val => dispatch => {
  dispatch({
    type: types.VISITOR_RECORD,
    payload:{
      viewListSpinning: val
    }
  })
}

//获取单个人的访问记录
export const getSigleVisitorList = (data, isFirst=false) => {
  return request({
    url: api.getVisitorDetail,
    method: 'post',
    data: data
  })
}
