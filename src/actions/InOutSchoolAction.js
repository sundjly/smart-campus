/* 出入校管理 actions */
import * as types from '../constants/ActionTypes'
import request from '../utils/request'
import { api } from '../constants/API'
import { message } from 'antd'

export const getInOutSchoolOption = () => {
  return dispatch => {
    return request({
      url: api.getInOutSchoolOption,
      method: 'get',
      data: {}
    }).then(res => {
      if (res && res.data && !res.errCode) {
        dispatch({
          type: types.GET_IN_OUT_SCHOOL_OPTION,
          payload: {
            schoolDate: res.data.timeRange.split(','),
            inCameraIds: res.data.aCameraIds.split(','),
            outCameraIds: res.data.lCameraIds.split(','),
            optionId: res.data.id
          }
        })
      } else {
        message.success('获取不到出入校配置!')
      }
    })
  }
}
// 新建
export const creatSchoolOption = data => dispatch => {
  return request({
    url: api.getInOutSchoolOption,
    method: 'post',
    data
  })
}

export const setInOutSchoolOption = data => {
  return dispatch => {
    return request({
      url: api.setInOutSchoolOption,
      method: 'put',
      data
    })
  }
}

export const updateInOutSchoolOption = payload => {
  return dispatch => {
    dispatch({
      type: types.CHANGE_IN_OUT_SCHOOL_OPTION,
      payload
    })
  }
}

export const getStudentAttendanceRecord = data => {
  return dispatch => {
    return request({
      url: api.getStudentAttendance,
      method: 'post',
      data
    }).then(res => {
      if (res && res.data && !res.errCode) {
        dispatch({
          type: types.GET_STUDENT_ATTENDANCE_RECORD,
          payload: {
            attendanceRecord: res.data.detail,
            attendanceTotal: res.data.total
          }
        })
      } else {
        message.error('获取出入校历史记录失败!')
      }
    })
  }
}

export const updateStudentAttendance = data => {
  return dispatch => {
    return request({
      url: api.setStudentAttendance,
      method: 'put',
      data
    }).then(res => {
      return res
    })
  }
}
