/* 登录页 actions */
import Cookies from 'js-cookie';
import md5 from 'react-native-md5';
import * as types from '../constants/ActionTypes';
import request from '../utils/request';
import { api } from '../constants/API';
import { tokenInvalid } from '../utils/config';

export const login = (data, props) => {
  const { userName, password, remember } = data;
  return async dispatch => {
    request({
      url: api.userLogin,
      method: 'form',
      data: {
        username: userName,
        password: md5.hex_md5(password),
        ...{
          grant_type: 'password',
          scope: 'read write',
          client_secret: '123456',
          client_id: 'clientapp'
        }
      }
    }).then(res => {
      if (res && !res.error) {
        const userInfo = res;
        //设置当前登录有效
        window.sessionStorage.setItem(
          'sessionUserInfo',
          JSON.stringify(userInfo)
        );
        // window.localStorage.setItem('oauth', JSON.stringify(userInfo));
        // window.localStorage.setItem('token', res.access_token);
        Cookies.set('token', JSON.stringify(res.access_token));
        //设置下次登录也有效
        if (remember) {
          Cookies.set('userInfo', JSON.stringify(userInfo), {
            expires: tokenInvalid
          });
        }
        props.history.push('/app/dashboard/index');
      } else {
        if (res.statusCode === 0) {
          dispatch(loginFail('系统维护中，请稍后再试!'));
          const timer = setTimeout(() => {
            dispatch({
              type: types.LOGIN_SUCCESS,
              payload: {
                isError: false,
                errorMsg: ''
              }
            });
            clearTimeout(timer);
          }, 2000);
        } else {
          dispatch(loginFail('账号或密码输入错误!'));
          const timer = setTimeout(() => {
            dispatch({
              type: types.LOGIN_SUCCESS,
              payload: {
                isError: false,
                errorMsg: ''
              }
            });
            clearTimeout(timer);
          }, 2000);
        }
      }
    });
  };
};

const loginSuccess = data => ({
  type: types.LOGIN_SUCCESS,
  payload: {
    isError: false,
    errorMsg: '',
    userInfo: data
  }
});

const loginFail = errorMsg => ({
  type: types.LOGIN_FAIL,
  payload: {
    isError: true,
    errorMsg: errorMsg
  }
});

export const logout = props => dispatch => {
  const logoutTxt = props.logoutTxt ? props.logoutTxt : "";
  request({
    url: api.userLogout,
    method: 'get',
    data: {},
    apiSource: "old"
  }).then(res => {
    if (res && res.errCode === 0) {
      Cookies.remove('userInfo');
      Cookies.remove('token');
      window.sessionStorage.removeItem('sessionUserInfo');
      dispatch({
        type: types.LOGOUT,
        payload: {
          userInfo: ''
        }
      });
      props.history.push({
        pathname: '/login',
        state: { logoutTxt }
      });
    }
  });
};

export const initAuth = props => (dispatch, ownProps) => {
  const userInfo =
    Cookies.get('userInfo') || window.sessionStorage.getItem('sessionUserInfo');

  if (userInfo) {
    let userInfoObj = JSON.parse(userInfo);
    const { oauth_AIK_user_info } = userInfoObj;
    if (
      oauth_AIK_user_info &&
      oauth_AIK_user_info.faceId &&
      oauth_AIK_user_info.faceId !== '0'
    ) {
      request({
        url: api.imageService,
        method: 'get',
        data: {
          urlParams: {
            id: oauth_AIK_user_info.faceId
          }
        },
        apiSource: "old"
      }).then(res => {
        userInfoObj.userAvatar = '';
        if (res && res.errCode === 0 && res.data) {
          userInfoObj.userAvatar = res.data.uri;
        }
        dispatch(loginSuccess(userInfoObj));
      });
    } else {
      userInfoObj.userAvatar = '';
      dispatch(loginSuccess(userInfoObj));
    }
    if (props.location.pathname === '/login') {
      props.history.push('/');
    }
  } else if (props.location.pathname !== '/login') {
    props.history.push('/login');
  }
};
export const changeLoginType = loginType => ({
  type: types.LOGIN_CHANGE_LOGIN_TYPE,
  payload: {
    loginType: loginType
  }
});
