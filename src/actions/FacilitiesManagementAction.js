import request from '../utils/request';
import { message } from 'antd';
import { api } from '../constants/API';
import * as types from '../constants/ActionTypes';

//获取设备列表
export const getFacilitiesList_dispatch = (requestData) => dispatch => {
    dispatch({
        type: types.TOGGLE_FACILITIES_LIST_SPIN,
        payload: { spinning: true }
    });
    return request({
        url: api.getFacilitiesList,
        method: 'post',
        data: requestData,
    }).then(
        res => {
            if (res.errCode === 0) {
                if (Array.isArray(res.data) && res.data.length > 0) {
                    res.data.map((item, value, arr) => {
                        item.key = item.id;
                        if (item.online === 4) {
                            item.online = "异常";
                        } else if (item.online === 5) {
                            item.online = "在线";
                        } else if (item.online === 6) {
                            item.online = "断线";
                        } else {
                            item.online = "无状态";
                        }

                    })
                }
                dispatch({
                    type: types.RENDER_FACILITIES_LIST,
                    payload: {
                        facilitiesList: res.data,
                        spinning: false
                    }
                });
            } else {
                dispatch({
                    type: types.RENDER_FACILITIES_LIST,
                    payload: {
                        facilitiesList: [],
                        spinning: false
                    }
                });
                message.error("获取设备列表失败!")
            }
            return res.errCode;
        }
    );

};
//新增设备
export const addFacilities_dispatch = (requestData) => dispatch => {
    return request({
        url: api.addFacilities,
        method: 'post',
        data: requestData,
    }).then(
        res => {
            if (res.errCode === 0) {
                message.success("新增设备成功!")
            } else {
                message.error(res.data);
            }
            return res.errCode;
        }
    );

};
//修改设备
export const modifyFacilities_dispatch = (requestData) => dispatch => {
    return request({
        url: api.modifyFacilities,
        method: 'post',
        data: requestData,
    }).then(
        res => {
            if (res.errCode === 0) {
                message.success("编辑设备成功!")
            } else {
                message.error("编辑设备失败!")
            }
            return res.errCode;
        }
    );

};
//删除设备
export const deleteFacilities_dispatch = (requestData) => dispatch => {
    return request({
        url: api.deleteFacilities + requestData.id,
        method: 'delete',
        data: {},
    }).then(
        res => {
            if (res.errCode === 0) {
                message.success("删除设备成功!")
            } else {
                message.error(res.data)
            }
            return res.errCode;
        }
    );

};