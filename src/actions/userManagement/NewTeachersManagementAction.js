import request from '@/utils/request';
import {message} from 'antd';
import {api} from '../../constants/API';
import * as types from '../../constants/ActionTypes';
import studentsManagementRecordReducer from "../../reducers/studentsManagementRecordReducer";


//  新增老师档案
export const addTeachersFormSubmit = (data, history = null) => dispatch => {
  console.log('sdj', data);
  let imageIds = [];
  data.imageIds.forEach((val, index) => {
    const imgId = val.response.data && val.response.data.id ? val.response.data.id : null;
    if (imgId) {
      imageIds.push(imgId);
    }
  });

  return request({
    url: api.addTeachersForm,
    method: 'post',
    data: {
      ...data,
      imageIds: imageIds.join(',')
    }
  }).then(res => {
    if (res.errCode === 0) {
      // message.success('提交成功,即将跳转回教师管理界面');
      // setTimeout(() => {
      //   history.push('/app/userManagement/teacherManagement');
      // }, 100)
      dispatch({
        type: types.ADD_TEACHERS_INFO,
        payload:{
          teacherId: res.data.id
        }
      })
    } else {
      console.log('res.data', res.data)
      dispatch({
        type: types.ADD_TEACHERS_INFO,
        payload:{
          teacherId: ''
        }
      })
      if (res.data.indexOf('number') !== -1) {
        message.error('提交失败, 失败原因：学号出现重复');
      } else {
        message.error('提交失败,失败原因:' + res.data)
      }
    }
  })
};

// 新增学生关联老师
export const pushStudent = (data, history) => dispatch => {
  return request({
    url: api.pushStudents,
    method: 'post',
    data: {
      ...data
    }
  }).then(res => {
    if (res.errCode === 0) {
      message.success('修改成功');
      history.push('/app/userManagement/teacherManagement');
    } else {
      if (res.data.indexOf('number duplicated') !== -1) {
        message.error('提交失败, 失败原因：学号出现重复');
      } else {
        message.error('提交失败,失败原因:' + res.data)
      }
    }
  })
}

// 获取编辑的教师信息
export const getEditInfo = teacherId => {
  if(!teacherId || teacherId=='') return message.error('获取不到教师的信息！')
  return request({
    url: `${api.getEditInfo}?teacherId=${teacherId}`,
    method:'post',
    data:{
    }
  });
}

//编辑教师档案
export const editTeacherForm = ({imageIds, ...data}, history) => {
  //  imageIds暂时无法改变
  console.log('editStudentsInfo data', data);
  return request({
    url: api.addTeachersForm,
    method: 'put',
    data: data
  }).then(res => {
    if (res.errCode === 0) {
      message.success('修改成功');
      // history.push('/app/userManagement/teacherManagement');
    } else {
      if (res.data.indexOf('number duplicated') !== -1) {
        message.error('提交失败, 失败原因：学号出现重复');
      } else {
        message.error('提交失败,失败原因:' + res.data)
      }
    }
  })
}


