import request from '@/utils/request';
import {message} from 'antd';
import {api} from '../../constants/API';
import * as types from '../../constants/ActionTypes';

//提交批量导入的数据
export const submitTemplate_dispatch = (formData, key) => {
  return (dispatch) => {
    dispatch({
      type: types.RELATIVE_UPLOAD,
      payload: {
        multiRegisterVisible: true,
        multiRegisterLoading: true,
        errorInfo: null
      }
    });
    request({
      url: api.submitTemplate+key+'?type=1',
      method: 'upload',
      data: formData,
    }).then(
      res => {
        if (res.errCode === 0) {
          dispatch({
            type: types.RELATIVE_UPLOAD,
            payload: {
              multiRegisterVisible: false,
              multiRegisterLoading: false,
              errorInfo: null,
              multiRegisterProgressVisible: true,
              multiRegisterProgress: {
                percent: 0,
                successNum: 0,
                failedNum: 0
              },
              progressError: {
                1: 0,
                2: 0,
                3: 0,
                4: 0,
                5: 0
              }
            }
          });
          let interval = setInterval(function () {
            dispatch(multiRegisterProgress(key, interval))
          }, 500);

        } else {
          // message.error(`导入失败!失败原因${res.data}`);
          if(res.errCode === 1001 && res.data.indexOf('file format is incorrect') !== -1){
            message.error(`导入失败!失败原因:格式错误`);
            dispatch({
              type: types.RELATIVE_UPLOAD,
              payload: {
                errorInfo: null,
                multiRegisterLoading: false
              }
            });
          }else if(res.errCode === 2000 && res.data.url !==''){
            dispatch({
              type: types.RELATIVE_UPLOAD,
              payload: {
                errorInfo: res.data.url,
                multiRegisterLoading: false
              }
            });
          }
          else if(res.errCode === 2001 && JSON.stringify(res.data) !=='{}'){
            dispatch({
              type: types.RELATIVE_UPLOAD,
              payload: {
                errorInfo: res.data,
                multiRegisterLoading: false
              }
            });
          }
        }
      }
    );
  }
};

//批量导入进度
export const multiRegisterProgress = (key, interval) => {
  return dispatch => {
    console.log('批量导入进度', key);
    request({
      url: api.uploadProgress+ key,
      method: 'post',
      data: {}
    }).then(res => {
      console.log('批量导入进度res', res);
      if (!res.errCode && res.data) {
        const { failedNum, successNum, totalSize } = res.data;
        dispatch({
          type: types.RELATIVE_UPLOAD,
          payload: {
            multiRegisterProgress: {
              percent: Math.round(
                100 * (Number(successNum) + Number(failedNum)) / Number(totalSize)
              ),
              successNum,
              failedNum
            },
            errorInfo: null,
            progressError: res.data.detailMap,
            progressErrorInfo: null
          }
        });
        if (failedNum + successNum >= totalSize) {
          console.log('批量导入完成');
          // message.success(`批量导入完成`);
          clearInterval(interval);
          dispatch(getProgressErrorUrl(key))
          //  3s后自动关闭
          // setTimeout(() => {
          //   dispatch({
          //     type: types.RELATIVE_UPLOAD,
          //     payload: {
          //       multiRegisterProgressVisible: false
          //     }
          //   });
          // }, 3000);
        }
      }
    })
  }
}
// 进度完成后显示错误日志
export const getProgressErrorUrl = key => dispatch => {
  return request({
    url: api.getUploadProgressErrUrl+key,
    method:'post',
    data:{}
  }).then(res => {
    console.log('sdj', res)
    if(!res.errCode ){
      dispatch({
        type: types.RELATIVE_UPLOAD,
        payload: {
          progressErrorInfo: res.data
        }
      })
    }
  })
}

// 取消导入进度
export const cancelUpLoadModal = key => dispatch =>{
  return request({
    url: api.cancelUpload+key,
    method:'post',
    data:{}
  }).then(res => {
    console.log('sdj cancelUpLoadModal', res)
    if(!res.errCode && res.data){
      message.success('取消导入成功');
    }
  })
}

//操作批量导入弹出框
export const operateModal_dispatch = (requestData) => {
  return {
    type: types.RELATIVE_UPLOAD,
    payload: requestData
  };
};

// 是否显示下载模态框
export const changeModalUpload = ({multiRegisterVisible,multiRegisterLoading}) => {
  return {
    type: types.RELATIVE_UPLOAD,
    payload:{
      multiRegisterVisible: multiRegisterVisible,
      multiRegisterLoading: multiRegisterLoading
    }
  }
}



//  获取列表数据
export const getRelativesList = (requestData, isFirst = false) => {
  return (dispatch) => {
    dispatch({
      type: types.RELATIVE_MANAGEMENT,
      payload: {
        viewListSpinning: true
      }
    });
    request({
      url: api.queryStudents,
      method: 'post',
      data: {
        ...requestData
      },
    }).then(
      res => {
        console.log('sdj s',res)
        if (res.errCode === 0) {
          const resData = res.data;
          dispatch({
            type: types.RELATIVE_MANAGEMENT,
            payload: {
              relativesList: resData,
              viewListSpinning: false,
              total: res.total
            }
          });
          if(!isFirst){
            console.log(`api.queryStudents 搜索成功！`);
          }
        } else {
          dispatch({
            type: types.RELATIVE_MANAGEMENT,
            payload: {
              relativesList: [],
              viewListSpinning: false,
              total: 0
            }
          });
          message.error(`查询失败!失败原因${res.data}`);
        }
      }
    );
  }
};


// 删除单个学生的记录
export const deleteStudentInfo = (id, fn, data)=> dispatch=>{
  return request({
    url:api.deleteStudentRecord+id,
    method:'delete',
    data:{
      // id: id
    }
  }).then(res => {
    if(res.errCode ===0){
      message.success('删除成功');
      fn.getTaskList(data);
    }else{
      message.error('删除信息失败')
    }
  })
}

//  导出表格记录
export const exportExcel = data => {
  request({
    url:api.downloadStudentRecord,
    method:'post',
    data:{
      ...data
    }
  }).then(res => {
    if(res.errCode ===0 && res.data.downloadUrl){
      console.log('sdj ', res)
      // window.open(res.data.downloadUrl,'_blank');
      window.location.href=res.data.downloadUrl;
    }
  })
}