// 学生出入校记录
import * as types from '../../constants/ActionTypes';
import request from '../../utils/request';
import { api } from '../../constants/API';
import { message } from 'antd';


export const getStudentRecord =(data, isFirst = false)  => dispatch => {
  dispatch({
    type:types.STUDENTS_RECORD_LIST,
    payload:{
      spining: true
    }
  });
  request({
    url:api.getStudentAttendance,
    method:'post',
    data:{
      ...data
    }
  }).then(res => {
    if(res.errCode === 0){
      if(!isFirst) message.success('查询成功');

      dispatch({
        type:types.STUDENTS_RECORD_LIST,
        payload:{
          studentRecordList: res.data.detail,
          spining: false,
          total: res.total
        }
      })
    }else {
      dispatch({
        type:types.STUDENTS_RECORD_LIST,
        payload:{
          studentRecordList: [],
          spining: false,
          total:0
        }
      })
      message.error(`出现错误，错误代码为${res.data}`)
    }
  })
}


