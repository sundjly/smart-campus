import request from '@/utils/request';
import {message} from 'antd';
import {api} from '../../constants/API';
import * as types from '../../constants/ActionTypes';

export const addStudentFormSubmit = ({name, classGrade, registered,...data,}, history) => {
  console.log('data,',data);
  let imageIds = [];
  data.imageIds.forEach((val, index) => {
    const id = val.response.data && val.response.data.id ?val.response.data.id:null;
    if(id){
      imageIds.push(id);
    }
  });

  return request({
    url:api.addStudentInfo,
    method:'post',
    data:{
      ...data,
      imageIds: imageIds.join(',')
    }
  }).then(res=> {
    if(res.errCode === 0){
      message.success('提交成功,即将跳转回学生管理界面');
      setTimeout(()=> {
        history.push('/app/userManagement/studentsManagement');
      },100)
    }else{
      console.log('res.data',res.data)
      if(res.data.indexOf('number')!==-1){
        message.error('提交失败, 失败原因：学号出现重复');
      }else{
        message.error('提交失败,失败原因:'+res.data)
      }
    }
  })
}

export const editStudentsInfo = ({imageIds, name, classGrade, registered,...data}, history)=>{
  //  imageIds暂时无法改变
  console.log('editStudentsInfo data',data);
  let newData = data;
  if(data.phone.indexOf('*')!==-1){
    const{phone, ...disData} = data;
    newData = disData;
  }
  return request({
    url:api.addStudentInfo,
    method:'put',
    data:{
      ...newData
    }
  }).then(res=> {
    if(res.errCode === 0){
      message.success('修改成功');
      history.push('/app/userManagement/studentsManagement');
    }else{
      if(res.data.indexOf('number duplicated')!==-1){
        message.error('提交失败, 失败原因：学号出现重复');
      }else{
        message.error('提交失败,失败原因:'+res.data)
      }
    }
  })
}
// 电话详情
export const getPhoneDetail = (studentId, cb) => {
  return request({
    url:api.getStudentAttendance,
    method: 'post',
    data:{
      studentId
    }
  }).then(res=>{
    let cPhone = '';
    console.log('sdj getPhoneDetail', res)
    if(res.data && res.data.detail && res.data.detail.length){
      const val = res.data.detail[0];
      cPhone =  val.phone? val.phone:'';
    }
    console.log('sdj cPhone promise', cPhone);
    cb({
      phone: cPhone
    })
  });
}


