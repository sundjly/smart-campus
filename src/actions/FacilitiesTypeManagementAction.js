import request from '../utils/request';
import { message } from 'antd';
import { api } from '../constants/API';
import * as types from '../constants/ActionTypes';

//获取设备类型列表
export const getFacilitiesTypeList_dispatch = (requestData) => dispatch => {
    dispatch({
        type: types.TOGGLE_FACILITIES_TYPE_LIST_SPIN,
        payload: { spinning: true }
    });
    return request({
        url: api.getFacilitiesTypeList,
        method: 'post',
        data: requestData,
    }).then(
        res => {
            if (res.errCode === 0) {
                if (Array.isArray(res.data) && res.data.length > 0) {
                    res.data.map((item, value, arr) => {
                        item.key = item.id;
                    })
                }
                dispatch({
                    type: types.RENDER_FACILITIES_TYPE_LIST,
                    payload: { facilitiesTypeList: res.data, spinning: false }
                });
            } else {
                dispatch({
                    type: types.RENDER_FACILITIES_TYPE_LIST,
                    payload: { facilitiesTypeList: [], spinning: false }
                });
                message.error("获取设备类型列表失败!")
            }
            return res.errCode;
        }
    );

};
//新增设备类型
export const addFacilitiesType_dispatch = (requestData) => dispatch => {
    return request({
        url: api.addFacilitiesType,
        method: 'post',
        data: requestData,
    }).then(
        res => {
            if (res.errCode === 0) {
                message.success("新增设备类型成功!");
            } else {
                message.error(`${res.data}!`);
            }
            return res.errCode;
        }
    );

};
//修改设备类型
export const modifyFacilitiesType_dispatch = (requestData) => dispatch => {
    return request({
        url: api.modifyFacilitiesType,
        method: 'post',
        data: requestData,
    }).then(
        res => {
            if (res.errCode === 0) {
                message.success("编辑设备类型成功!")
            } else {
                message.error(`${res.data}!`);
            }
            return res.errCode;
        }
    );

};
//删除设备类型
export const deleteFacilitiesType_dispatch = (requestData) => dispatch => {
    return request({
        url: api.deleteFacilitiesType + requestData.id,
        method: 'delete',
        data: {},
    }).then(
        res => {
            if (res.errCode === 0) {
                message.success("删除设备类型成功!")
            } else {
                message.error("删除设备类型失败!")
            }
            return res.errCode;
        }
    );

};