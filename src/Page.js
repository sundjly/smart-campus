import React from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import NotFound from './components/pages/NotFound';

import Login from './containers/LoginContainer';
import App from './App';
import PhoneStudentsRecord from './components/PhoneStudentsRecord';
export default () => (
    <Router>
        <Switch>
          {/*初始化首页的*/}
          {/*<Route exact path="/" render={() => <Redirect to="/app/dashboard/index" push />} />*/}
            <Route exact path="/" component={Login} />
            <Route path="/app" component={App} />
            <Route path="/404" component={NotFound} />
            <Route path="/login" component={Login} />
            {/*<Route path="/phone" component={PhoneStudentsRecord} />*/}
            <Route component={NotFound} />
        </Switch>
    </Router>
)