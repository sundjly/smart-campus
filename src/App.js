import React, {Component} from 'react';
import {Layout, notification, Icon, LocaleProvider, message} from 'antd';
import './style/index.less';
import SiderCustom from './components/SiderCustom';
import HeaderCustom from './components/HeaderCustom';
import {receiveData} from './actions';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import Routes from './routes';
import {getAllCameras} from './actions/AppAction';
import zhCN from 'antd/lib/locale-provider/zh_CN';
import {getClassGradeList} from './actions/userManagement/StudentsManagementAction';

const {Content, Footer} = Layout;

class App extends Component {
  state = {
    collapsed: false,
  };

  componentWillMount() {
    const {receiveData, getAllCameras, getClassGradeList} = this.props;
    const user = JSON.parse(localStorage.getItem('user'));
    user && receiveData(user, 'auth');
    // receiveData({a: 213}, 'auth');
    // fetchData({funcName: 'admin', stateName: 'auth'});
    this.getClientWidth();
    getAllCameras();
    getClassGradeList();
    window.onresize = () => {
      // console.log('屏幕变化了');
      this.getClientWidth();
      // console.log(document.body.clientWidth);
    }

  }

  componentDidMount() {
    const openNotification = () => {
      notification.open({
        message: '你好，欢迎你回来',
        description: (
          <div>
            {/*<p>*/}
            {/*GitHub地址： <a href="https://github.com/yezihaohao" target="_blank" rel="noopener noreferrer">https://github.com/yezihaohao</a>*/}
            {/*</p>*/}
            {/*<p>*/}
            {/*博客地址： <a href="https://yezihaohao.github.io/" target="_blank" rel="noopener noreferrer">https://yezihaohao.github.io/</a>*/}
            {/*</p>*/}
          </div>
        ),
        icon: <Icon type="smile-circle" style={{color: 'red'}}/>,
        duration: 0,
      });
      localStorage.setItem('isFirst', JSON.stringify(true));
    };
    const isFirst = JSON.parse(localStorage.getItem('isFirst'));
    !isFirst && openNotification();
    // message的全局配置   http://ant-design.gitee.io/components/message-cn/#header
    message.config({
      top: "40%",
      duration: 2,
      maxCount: 3,
    });
  }

  getClientWidth = () => {    // 获取当前浏览器宽度并设置responsive管理响应式
    const {receiveData} = this.props;
    const clientWidth = document.body.clientWidth;
    console.log(clientWidth);
    receiveData({isMobile: clientWidth <= 992}, 'responsive');
  };
  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };

  render() {
    // console.log(this.props.auth);
    // console.log(this.props.responsive);
    const {auth, responsive} = this.props;
    return (
      <Layout>
        {!responsive.data.isMobile && <SiderCustom collapsed={this.state.collapsed}/>}
        <Layout style={{flexDirection: 'column'}}>
          <HeaderCustom toggle={this.toggle} collapsed={this.state.collapsed} user={auth.data || {}}/>
          <Content style={{margin: '16px', overflow: 'initial', background: "#fff", padding: "10px"}}>
            <LocaleProvider locale={zhCN}>
              <Routes auth={auth}/>
            </LocaleProvider>
          </Content>
          {/*<Footer style={{ textAlign: 'center' }}>*/}
          {/*后台管理系统 ©2018 Intellif */}
          {/*</Footer>*/}
        </Layout>

        {
          responsive.data.isMobile && (   // 手机端对滚动很慢的处理
            <style>
              {`
                #root{
                    height: auto;
                }
              `}
            </style>
          )
        }
      </Layout>
    );
  }
}

const mapStateToProps = state => {
  const {auth = {data: {}}, responsive = {data: {}}} = state.httpData;
  return {auth, responsive, ...state.appReducer};
};
const mapDispatchToProps = dispatch => ({
  receiveData: bindActionCreators(receiveData, dispatch),
  getAllCameras: bindActionCreators(getAllCameras, dispatch),
  getClassGradeList: bindActionCreators(getClassGradeList, dispatch)
});

export default connect(mapStateToProps,
  mapDispatchToProps,
)(App);
