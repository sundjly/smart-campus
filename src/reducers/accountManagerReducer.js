import * as types from "../constants/ActionTypes";

const initState = {
    UserAccountList: [],
    roleList: [],
};

export default (state = initState, action) => {
    switch (action.type) {
        case types.GET_ALL_USER_ACCOUNT:
            return { ...state, ...action.payload };
        case types.GET_ALL_ROLES:
            return { ...state, ...action.payload };
        default:
            return { ...state };
    }
};
