import * as types from '../constants/ActionTypes';

const INITIAL_STATE = {
  multiRegisterVisible: false,
  multiRegisterLoading: false,
  studentsList:[],//学生信息的列表
  viewListSpinning: false, //加载loading
  total: 0,

  errorInfo:null,// 错误提示信息
  multiRegisterProgress: {
    percent: 0,
    successNum: 0,
    failedNum: 0
  },
  multiRegisterProgressVisible: false,
  progressError: {
    1: 0,
    2: 0,
    3: 0,
    4: 0,
    5: 0
  },
  progressErrorInfo: null,// 进度完成后请求错误日志
};

/**
 * @param {Object} state - Default application state
 * @param {Object} action - Action from action creator
 * @returns {Object} New state
 */
export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case types.SHOW_MULTI_REGISTER_MODAL:
      return {
        ...state,
        ...action.payload
      };
    default: return state;
  }
};
