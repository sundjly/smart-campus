import * as types from '../constants/ActionTypes';

//初始化页面数据
const INITAL_STATE ={
  total:0,
  viewListSpinning: false,
  multiRegisterVisible: false,
  multiRegisterLoading: false,
  errorInfo:null,// 错误提示信息
  relativesList: [], // 亲属列表
  multiRegisterProgress: {
    percent: 0,
    successNum: 0,
    failedNum: 0
  },
  multiRegisterProgressVisible: false,
  progressError: {
    1: 0,
    2: 0,
    3: 0,
    4: 0,
    5: 0
  },
  progressErrorInfo: null,// 进度完成后请求错误日志
}

//根据数据返回。
export default (state = INITAL_STATE, action) => {
  switch(action.type) {
    case types.TEACHERS_MANAGEMENT:
      return {
        ...state,
        ...action.payload
      };
    case types.TEACHERS_UPLOAD:
      return {
        ...state,
        ...action.payload
      }
    default: return state;
  }
}