import * as types from "../constants/ActionTypes";

const INITIAL_STATE = {
    facilitiesList: [],
    spinning: false
};

/**
* @param {Object} state - Default application state
* @param {Object} action - Action from action creator
* @returns {Object} New state
*/
export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case types.RENDER_FACILITIES_LIST:
            return {
                ...state,
                ...action.payload
            };
        case types.TOGGLE_FACILITIES_LIST_SPIN:
            return {
                ...state,
                ...action.payload
            };
        default: return state;
    }
};