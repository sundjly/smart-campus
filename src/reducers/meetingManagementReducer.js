import * as types from '../constants/ActionTypes'
import moment from 'moment'

const initState = {
  meetingList: [],
  meetingPersonList: [],
  meetingTotal: 0,
  meetingDetail: {
    name: '',
    address: '',
    rangeTime: [
      moment().format('YYYY-MM-DD HH:mm:ss'),
      moment(new Date().getTime() + 3600000).format('YYYY-MM-DD HH:mm:ss')
    ],
    admins: [],
    cameras: [],
    targetKeys: []
  }
}

export default (state = initState, action) => {
  switch (action.type) {
    case types.UPDATE_MEETING_LIST:
      return {
        ...state,
        ...action.payload
      }
    case types.UPDATE_MEETING_DETAIL:
      return {
        ...state,
        ...action.payload
      }
    case types.UPDATE_MEETING_PERSON_LIST:
      return {
        ...state,
        ...action.payload
      }
    default: return state
  }
}
