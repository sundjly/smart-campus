import * as types from '../constants/ActionTypes';

const INITIAL_STATE = {
  visitorsList:[],//学生信息的列表
  viewListSpinning: false, //加载loading
  total: 0,
};

/**
 * @param {Object} state - Default application state
 * @param {Object} action - Action from action creator
 * @returns {Object} New state
 */
export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case types.VISITOR_MANAGEMENT:
      return {
        ...state,
        ...action.payload
      };
    default: return state;
  }
};
