import * as types from '../constants/ActionTypes'

const initState = {
  schoolDate: [], // 出入校时间段
  inCameraIds: [], // 入校摄像头
  outCameraIds: [], // 出校摄像头
  optionId: '', // 出入校配置id
  attendanceRecord: [],
  attendanceTotal: [0, 0, 0, 0]
}

export default (state = initState, action) => {
  switch (action.type) {
    case types.SET_IN_OUT_SCHOOL_OPTION:
      return {
        ...state,
        ...action.payload
      }
    case types.GET_IN_OUT_SCHOOL_OPTION:
      return {
        ...state,
        ...action.payload
      }
    case types.CHANGE_IN_OUT_SCHOOL_OPTION:
      return {
        ...state,
        ...action.payload
      }
    case types.GET_STUDENT_ATTENDANCE_RECORD:
      return {
        ...state,
        ...action.payload
      }
    case types.UPDATE_STUDENT_ATTENDANCE:
      return {
        ...state,
        ...action.payload
      }
    default: return state
  }
}
