import * as types from "../constants/ActionTypes";

const INITIAL_STATE = {
    facilitiesTypeList: []
};

/**
* @param {Object} state - Default application state
* @param {Object} action - Action from action creator
* @returns {Object} New state
*/
export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case types.RENDER_FACILITIES_TYPE_LIST:
            return {
                ...state,
                ...action.payload
            };
        case types.TOGGLE_FACILITIES_TYPE_LIST_SPIN:
            return {
                ...state,
                ...action.payload
            };
        default: return state;
    }
};