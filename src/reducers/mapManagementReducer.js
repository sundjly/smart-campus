import * as types from "../constants/ActionTypes";

const INITIAL_STATE = {
    total: 0,
    spinning: false,
    mapList: []
};

/**
* @param {Object} state - Default application state
* @param {Object} action - Action from action creator
* @returns {Object} New state
*/
export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case types.RENDER_MAP_LIST:
            return {
                ...state,
                ...action.payload
            };
        case types.TOGGLE_MAP_LIST_SPIN:
            return {
                ...state,
                ...action.payload
            };
        default: return state;
    }
};