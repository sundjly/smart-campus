import * as types from '../constants/ActionTypes';

const INIIAL_STATE ={

  viewListSpinning: false, //加载loading

  visitorList:[],
  total: 0
  // errorInfo:null,// 错误提示信息
  // multiRegisterProgress: {
  //   percent: 0,
  //   successNum: 0,
  //   failedNum: 0
  // },
  // multiRegisterProgressVisible: false,
  // progressError: {
  //   1: 0,
  //   2: 0,
  //   3: 0,
  //   4: 0,
  //   5: 0
  // },
  // progressErrorInfo: null,// 进度完成后请求错误日志
};

//加入redux之后的数据返回和直接的数据的返回有什么关系。为什么需要redux.在这个例子中加入的redux并没有作用。

export default (state = INIIAL_STATE, action) =>{
  
  switch (action.type) {
    case types.VISITOR_RECORD:
      return {
        ...state,
        ...action.payload
      };
    default: return state;
  }

}