import { combineReducers } from "redux";
import * as type from "../constants/ActionTypes";
import appReducer from "./appReducer";
import loginReducer from "./loginReducer";
import newStudentsManagementReducer from "./newStudentsManagementReducer";
import studentsManagementReducer from "./studentsManagementReducer";
import inOutSchoolReducer from "./inOutSchoolReducer";
import studentsManagementRecordReducer from "./studentsManagementRecordReducer";
import meetingManagementReducer from "./meetingManagementReducer";
import classesManagementReducer from "./classesManagementReducer";
import facilitiesTypeManagementReducer from "./facilitiesTypeManagementReducer";
import facilitiesManagementReducer from "./facilitiesManagementReducer";
import mapManagementReducer from "./mapManagementReducer";
import visitorRecordReducer from "./visitorRecordReducer";
import teachersManagementReducer from "./teachersManagementReducer";
import relativesMangementReducer from "./relativesMangementReducer";
import vistorReducer from "./visitorReducer";
import nameCourseReducer from "./nameCourseReducer";
import backRoomReducer from "./backRoomReducer";
import newTeachersManagementReducer from "./newTeachersManagementReducer";
import accountManagerReducer from "./accountManagerReducer";

const handleData = (state = { isFetching: true, data: {} }, action) => {
    switch (action.type) {
        case type.REQUEST_DATA:
            return { ...state, isFetching: true };
        case type.RECEIVE_DATA:
            return { ...state, isFetching: false, data: action.data };
        default:
            return { ...state };
    }
};

const httpData = (state = {}, action) => {
    switch (action.type) {
        case type.RECEIVE_DATA:
        case type.REQUEST_DATA:
            return {
                ...state,
                [action.category]: handleData(state[action.category], action)
            };
        default:
            return { ...state };
    }
};

export default combineReducers({
    appReducer,
    httpData,
    loginReducer,
    studentsManagementReducer,
    newStudentsManagementReducer,
    inOutSchoolReducer,
    studentsManagementRecordReducer,
    meetingManagementReducer,
    classesManagementReducer,
    facilitiesTypeManagementReducer,
    facilitiesManagementReducer,
    mapManagementReducer,
    vistorReducer,
    visitorRecordReducer,
    relativesMangementReducer,
    teachersManagementReducer,
    newTeachersManagementReducer,
    nameCourseReducer,
    backRoomReducer,
    accountManagerReducer
});
