import * as types from '../constants/ActionTypes';
const INITIAL_STATE ={
  multiRegisterVisible: false,
  multiRegisterLoading: false,
  errorInfo:null,// 错误提示信息
  viewListSpinning: false, //加载loading
  teacherId: ''
}


/**
 * @param {Object} state - Default application state
 * @param {Object} action - Action from action creator
 * @returns {Object} New state
 */
export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case types.ADD_TEACHERS_INFO:
      return {
        ...state,
        ...action.payload
      };
    default: return state;
  }
};
