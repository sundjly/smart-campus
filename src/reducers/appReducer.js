import * as types from '../constants/ActionTypes';

const initialState = {
  cameras: {},
  classGrade:[],//班级信息列表
};
const appReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.GET_ALL_CAMERAS:
      return {
        ...state,
        ...action.payload
      }
    default:
      return state;
  }
};

export default appReducer;
