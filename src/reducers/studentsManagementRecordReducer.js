import * as types from '../constants/ActionTypes';

const INITIAL_STATE = {
  studentRecordList:[],
  spining: false,
  total: 0

};

/**
 * @param {Object} state - Default application state
 * @param {Object} action - Action from action creator
 * @returns {Object} New state
 */
export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case types.STUDENTS_RECORD_LIST:
      return {
        ...state,
        ...action.payload
      };
    default: return state;
  }
};
