export const mock = (options, response) => {
  const { url } = options;
  let mockData;
  if (false && url.indexOf('share/face/search/face') !== -1) {
    mockData = {
      data: [
        {
          id: '7037006856346920',
          sourceId: '9',
          sourceType: 0,
          time: 1514011581000,
          imageData:
            'http://192.168.2.11/store3/store3_0/FaceWareHouse/src_0_9/20171223/20171223T144621_94718_130399.jpg',
          race: 0,
          fromImageId: '7037006856319064',
          json:
            '{ "Rect":{ "left":635,"top":472,"right":743,"bottom":580}, "FaceImageRect":{ "left":581,"top":418,"right":797,"bottom":634},"Attr":{"Detector":4,"ExtFrom":0}}',
          quality: 0,
          sequence: 683439,
          score: 0.0
        }
      ],
      errCode: 0,
      maxPage: 0,
      total: 800
    };
    return {
      data: mockData
    };
  } else {
    return response;
  }
};
