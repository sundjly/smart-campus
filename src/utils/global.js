import $IF from './config';
import moment from 'moment';

//获取环境配置信息
export function getEnviroment(scope) {
    var host = window.location.host;
    //  一级路径匹配
    var curEnv = '';
    for (let env in scope.environments) {
        if (scope.environments[env].host === host) {
            curEnv = env;
        }
    }
    if (curEnv === '') {
        alert('配置文件错误！无此环境配置信息');
        return;
    }
    console.info('当前配置环境为:', curEnv, $IF.environments[curEnv]);
    window.config.env = $IF.environments[curEnv];
    return $IF.environments[curEnv];
}

//判断ie浏览器的版本
export function iEVersion() {
    var userAgent = navigator.userAgent; //取得浏览器的userAgent字符串
    var isIE = userAgent.indexOf('compatible') > -1 && userAgent.indexOf('MSIE') > -1; //判断是否IE<11浏览器
    var isEdge = userAgent.indexOf('Edge') > -1 && !isIE; //判断是否IE的Edge浏览器
    var isIE11 = userAgent.indexOf('Trident') > -1 && userAgent.indexOf('rv:11.0') > -1;
    if (isIE) {
        var reIE = new RegExp('MSIE (\\d+\\.\\d+);');
        var result = reIE.exec(userAgent);
        var fIEVersion = parseInt(result[1], 10);
        if (fIEVersion === 7) {
            return 7;
        } else if (fIEVersion === 8) {
            return 8;
        } else if (fIEVersion === 9) {
            return 9;
        } else if (fIEVersion === 10) {
            return 10;
        } else {
            return 6; //IE版本<=7
        }
    } else if (isEdge) {
        return 'edge'; //edge
    } else if (isIE11) {
        return 11; //IE11
    } else {
        return -1; //不是ie浏览器
    }
}

//判断浏览器类型
export function browserType() {
    var userAgent = navigator.userAgent; //取得浏览器的userAgent字符串

    var isIE = userAgent.indexOf('compatible') > -1 && userAgent.indexOf('MSIE') > -1; //判断是否IE<11浏览器
    var isEdge = userAgent.indexOf('Edge') > -1 && !isIE; //判断是否IE的Edge浏览器
    var isIE11 = userAgent.indexOf('Trident') > -1 && userAgent.indexOf('rv:11.0') > -1; //判断是否IE11浏览器
    if (isIE || isEdge || isIE11) {
        return 'IE';
    } //判断是否IE浏览器
    if (userAgent.indexOf('Opera') > -1 || userAgent.indexOf('OPR') > -1) {
        return 'Opera';
    } //判断是否Opera浏览器
    if (userAgent.indexOf('Firefox') > -1) {
        return 'FF';
    } //判断是否Firefox浏览器
    if (userAgent.indexOf('Chrome') > -1) {
        return 'Chrome';
    }
    if (userAgent.indexOf('Safari') > -1) {
        return 'Safari';
    } //判断是否Safari浏览器
}

//服务器连接IP替换
export function replaceImgDomain(data) {
    if (!window.$IF.env)
        return;
    const env = window.$IF.env;
    if (env.domain.replaceDomain === false) {
        return data;
    }
    var replaceData = data;
    if (env.domain.replaceDomain && JSON.stringify(env.domain.path) === '{}'){
      replaceData = replaceData.replace(new RegExp('http://', 'gm'), 'http://');
    }
    if (env.domain.replaceDomain && env.domain.path && JSON.stringify(env.domain.path) !== '{}'){
      const path = env.domain.path
      Object.keys(path).forEach(function (d, i) {
        let pathChild = path[d];
        replaceData = replaceData.replace(pathChild.src, pathChild.dst);
      });
    }

    return replaceData;
}

/*
** randomWord 产生任意长度随机字母数字组合
** randomFlag-是否任意长度 min-任意长度最小位[固定位数] max-任意长度最大位
*/
export const randomWord = (randomFlag, min, max) => {
    let str = '',
        range = min,
        pos = null,
        arr = [
            '0',
            '1',
            '2',
            '3',
            '4',
            '5',
            '6',
            '7',
            '8',
            '9',
            'a',
            'b',
            'c',
            'd',
            'e',
            'f',
            'g',
            'h',
            'i',
            'j',
            'k',
            'l',
            'm',
            'n',
            'o',
            'p',
            'q',
            'r',
            's',
            't',
            'u',
            'v',
            'w',
            'x',
            'y',
            'z',
            'A',
            'B',
            'C',
            'D',
            'E',
            'F',
            'G',
            'H',
            'I',
            'J',
            'K',
            'L',
            'M',
            'N',
            'O',
            'P',
            'Q',
            'R',
            'S',
            'T',
            'U',
            'V',
            'W',
            'X',
            'Y',
            'Z'
        ];

    // 随机产生
    if (randomFlag) {
        range = Math.round(Math.random() * (max - min)) + min;
    }
    for (let i = 0; i < range; i++) {
        pos = Math.round(Math.random() * (arr.length - 1));
        str += arr[pos];
    }
    return str;
};

//计算距离现在多久
export function timeLeaveNow(timeStamp) {
    var now = new Date().getTime();
    var timeLeaveNow = now - timeStamp;
    var returnText = '';
    if (timeLeaveNow > 1000 * 60 * 60 * 24 * 31) {
        returnText = Math.floor(timeLeaveNow / (1000 * 60 * 60 * 24 * 31)) + '个月';
    } else if (timeLeaveNow > 1000 * 60 * 60 * 24 * 7) {
        returnText = Math.floor(timeLeaveNow / (1000 * 60 * 60 * 24 * 7)) + '周';
    } else if (timeLeaveNow > 1000 * 60 * 60 * 24) {
        returnText = Math.floor(timeLeaveNow / (1000 * 60 * 60 * 24)) + '天';
    } else if (timeLeaveNow > 1000 * 60 * 60) {
        returnText = Math.floor(timeLeaveNow / (1000 * 60 * 60)) + '小时';
    } else if (timeLeaveNow > 1000 * 60) {
        returnText = Math.floor(timeLeaveNow / (1000 * 60)) + '分钟';
    } else if (timeLeaveNow > 30000) {
        returnText = '1分钟';
    } else {
        returnText = '0分钟';
    }
    return returnText;
}
