/* global window */
import axios from 'axios';
import qs from 'qs';
// import jsonp from 'jsonp'
import lodash from 'lodash';
// import pathToRegexp from 'path-to-regexp'
import { message } from 'antd';
// import config from './config';
import { replaceImgDomain } from './global';
import Cookies from 'js-cookie';
import Url from './Url';
import { mock } from './mock';

function buildParam(url, params) {
    return url.replace(
        /\/:(\w+)/gm,
        index =>
            `/${index === '/:id' ? '' : index.replace(/\/:/g, '') + '/'}${
            params[`${index.replace(/\/:/g, '')}`]
            }`
    );
}

const fetch = options => {
    let { method = 'get', data, url } = options;
    try {
        url = buildParam(url, data.urlParams);
    } catch (e) {
        throw new Error('请求链接参数错误.');
    }

    delete data.urlParams;
    const cloneData = lodash.cloneDeep(data);

    //配置axios请求默认值
    let token = '';
    let userInfo =
        Cookies.get('userInfo') || window.sessionStorage.getItem('sessionUserInfo');
    if (userInfo) {
        token = JSON.parse(userInfo).access_token;
    }

    axios.defaults.baseURL = window.$IF.env.apiBaseURL; //默认Url地址
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;

    // try {
    //   let domin = ''
    //   if (url.match(/[a-zA-z]+:\/\/[^/]*/)) {
    //     domin = url.match(/[a-zA-z]+:\/\/[^/]*/)[0]
    //     url = url.slice(domin.length)
    //   }
    //   const match = pathToRegexp.parse(url)
    //   url = pathToRegexp.compile(url)(data)
    //   for (let item of match) {
    //     if (item instanceof Object && item.name in cloneData) {
    //       delete cloneData[item.name]
    //     }
    //   }
    //   url = domin + url
    // } catch (e) {
    //   message.error(e.message)
    // }

    switch (method.toLowerCase()) {
        case 'get':
            return axios.get(url, {
                params: cloneData
            });
        case 'delete':
            return axios.delete(url, {
                data: cloneData
            });
        case 'post':
            return axios.post(url, cloneData);
        case 'put':
            return axios.put(url, cloneData);
        case 'patch':
            return axios.patch(url, cloneData);
        case 'form':
            return axios.post(url, qs.stringify(cloneData), {
                headers: {
                    Accept: 'application/json, text/javascript, */*; q=0.01',
                    Authorization: 'Basic ' + btoa('clientapp:123456'),
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                }
            });
        case 'upload':
            if (options.progress) {
                return axios.post(url, data, {
                    onUploadProgress: options.progressCallback
                });
            } else {
                return axios.post(url, data, {
                    headers: {
                        Accept: 'application/json, text/javascript, */*; q=0.01',
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                    }
                });
            }
        default:
            return axios(options);
    }
};

export default function request(options) {
    return fetch(options)
        .then(response => {
            //restore all response img url
            // return JSON.parse(Url.restoreAll(JSON.stringify(response)));
            return JSON.parse(
                Url.restoreAll(replaceImgDomain(JSON.stringify(response)))
            );
            // return JSON.parse(JSON.stringify(response));
        })
        .then(response => {
            return mock(options, response);
        })
        .then(response => {
            const { data } = response;

            return Promise.resolve({
                ...data
            });
        })
        .catch(error => {
            const { response } = error;
            let msg;
            let statusCode;
            if (response && response instanceof Object) {
                const { data, status, statusText } = response;
                statusCode = response.status;
                msg = data.message || statusText;
                if (response.status === 401) {
                    window.location = '/login';
                    Cookies.remove('userInfo');
                    window.sessionStorage.removeItem('sessionUserInfo');
                } else if (
                    status === 403 &&
                    statusText === 'Forbidden' &&
                    error.message &&
                    error.message.indexOf('no privilege') !== -1
                ) {
                    message.error('接口没配置权限，自动退出系统', () => {
                        window.location = '/login';
                        Cookies.remove('userInfo');
                        window.sessionStorage.removeItem('sessionUserInfo');
                    });
                } else if (
                    status === 405 &&
                    statusText === 'Forbidden' &&
                    error.message &&
                    error.message.indexOf('no privilege') !== -1
                ) {
                    message.error('禁止IP，自动退出系统', () => {
                        window.location = '/login';
                        Cookies.remove('userInfo');
                        window.sessionStorage.removeItem('sessionUserInfo');
                    });
                }
            } else {
                message.error('系统服务异常，自动退出系统', () => {
                    window.location = '/login';
                    Cookies.remove('userInfo');
                    Cookies.remove('token');
                    window.sessionStorage.removeItem('sessionUserInfo');
                });
            }
            return Promise.resolve({
                error: true,
                statusCode: statusCode,
                statusMessage: msg
            });
        });
}
