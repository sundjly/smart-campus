
//根据 YYYY-MM-DD返回到一个月时间的数据
export const  getMothEnd = endTime => {
  let endArr= endTime.split('-');
  if(endArr.length!==2) return console.log('格式不规范')
  const endMoth = endArr[1];
  let endtime = '';
  switch(endMoth){
    case '01':
    case '03':
    case '05':
    case '07':
    case '08':
    case '10':
    case '12':
      endtime = '-31 00:00:00';
      break;
    case '02':
      endtime = '-28 00:00:00';
      break;
    case '04':
    case '06':
    case '09':
    case '11':
      endtime='-30 00:00:00';
      break;
    default:
      break;
  }
  return endTime+endtime;
}

