import { IPMapList } from './config';
import { getEnviroment } from './global';
import $IF from './config';

const PicTypes = ['jpg', 'png', 'jpeg'];

// 配置全局环境变量
window.$IF = $IF;
window.$IF.env = getEnviroment($IF);
const centerProxyServerPrefix = window.$IF.env.centerProxyServerPrefix;

export default class Url {
  static restore(url) {
    if (String(url).indexOf('http://') === -1) {
      url = centerProxyServerPrefix + url;
    }
    return url;
  }
  static restoreAll(str) {
    return String(str)
      .split('"')
      .map(function(data) {
        if (data) {
          var lastPointIndex = data.lastIndexOf('.');

          if (lastPointIndex > -1 && data.indexOf('http://') === -1) {
            var type = data.slice(lastPointIndex + 1).toLowerCase();

            if (PicTypes.indexOf(type) !== -1) {
              data =
                data.slice(0,1) === '/'
                  ? centerProxyServerPrefix + data
                  : centerProxyServerPrefix + '/' + data;
            }
          }
        }

        // data = this.MapIP(data);

        return data;
      })
      .join('"');
  }
  static MapIP(str) {
    if (IPMapList && Object.keys(IPMapList).length > 0) {
      Object.keys(IPMapList).forEach(function(key) {
        if (str.indexOf(key) !== -1) {
          str = str.replace(new RegExp(key, 'gm'), IPMapList[key]);
        }
      });
    }
    return str;
  }
}
