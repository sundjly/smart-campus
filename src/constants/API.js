const API1 = "/api";
const API2 = "/api/intellif";
export const api = {
  //登录
  userLogin: `${API1}/oauth/token`,
  userLogout: `${API2}/server/logoff`,
  imageService: `${API2}/image/:id`,
  //学生管理
  submitTemplate: `${API2}/zhxy/upload/zip/upload/`,
  classGradeList: `${API2}/zhxy/area/tree`,
  addStudentInfo: `${API2}/zhxy/document`, // 新增  修改学生管理信息接口
  queryStudents: `${API2}/zhxy/person/query`,
  deleteStudentRecord: `${API2}/zhxy/document/`,
  downloadStudentRecord: `${API2}/zhxy/person/query/download`,
  uploadProgress: `${API2}/zhxy/upload/zip/upload/progress/`,
  cancelUpload: `${API2}/zhxy/upload/zip/upload/cancel/`,
  getUploadProgressErrUrl: `${API2}/zhxy/upload/zip/upload/eDownload/`,
  //出入校管理
  //教师管理
  addTeachersForm: `${API2}/zhxy/document`,
  pushStudents: `${API2}/zhxy/document/relationship`,
  getEditInfo: `${API2}/zhxy/document/teacher/info`,
  // 角色管理
  roleList: `${API2}/zhxy/system/role/get`,
  roleManage: `${API2}/zhxy/system/role`,
  // 账号管理
  accountList: `${API2}/zhxy/system/user/search`,
  // 系统操作日志
  logList: `${API2}/zhxy/system/log`,
  // 访客管理
  getVisitorList: `${API2}/zhxy/visit/record/list`,
  getVisitorDetail: `${API2}/zhxy/visit/guest/detail`,

  getAllCameras: `${API2}/camera`,
  getStudentAttendance: `${API2}/zhxy/student/attendance/record`,
  setStudentAttendance: `${API2}/zhxy/student/attendance/update`,
  getInOutSchoolOption: `${API2}/zhxy/attendance`,
  setInOutSchoolOption: `${API2}/zhxy/attendance/:id`,
  //会议管理
  cancelMeeting: `${API2}/zhxy/meet/cancel/:meetId`,
  deleteMeeting: `${API2}/zhxy/meet/delete/:meetId`,
  updateMeeting: `${API2}/zhxy/meet/update`,
  createMeeting: `${API2}/zhxy/meet/create`,
  getMeetingDetail: `${API2}/zhxy/meet/detail`,
  getMeetingList: `${API2}/zhxy/meet/list`,
  //班级管理
  getGradeTreeList: `${API2}/zhxy/area/tree`,
  addGradeOrClass: `${API2}/zhxy/area/create`,
  operateGradeOrClass: `${API2}/zhxy/area/`,
  //设备类型管理
  getFacilitiesTypeList: `${API2}/zhxy/camera/type/list`,
  addFacilitiesType: `${API2}/zhxy/camera/type/add`,
  modifyFacilitiesType: `${API2}/zhxy/camera/type/update`,
  deleteFacilitiesType: `${API2}/zhxy/camera/type/delete/`,
  //设备管理
  getFacilitiesList: `${API2}/zhxy/camera/list`,
  addFacilities: `${API2}/zhxy/camera/add`,
  modifyFacilities: `${API2}/zhxy/camera/update`,
  deleteFacilities: `${API2}/zhxy/camera/delete/`,
  //地图管理
  getMapList: `${API2}/zhxy/map/list`,
  addOrEditMap: `${API2}/zhxy/map/info/save`,
  deleteMap: `${API2}/zhxy/map/delete/`,
  uploadMap: `${API2}/zhxy/map/img/upload`,

  //课程管理
  getCourseList: `${API2}/zhxy/course/list`,
  // 账号管理
  accountBaseUrl: `${API2}/zhxy/system/user`,
  // 获取角色信息
  getRoleList: `${API2}/zhxy/system/role/get`,

  //消息模板
  getMesgTplList: `${API2}/zhxy/system/message/get`, //GET
  addMesgTpl: `${API2}/zhxy/system/message`, //POST
  updateMesgTpl: `${API2}/zhxy/system/message`, //PUT
  delMesgTpl: `${API2}/zhxy/system/message/`, //DELETE
};
