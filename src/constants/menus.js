import intl from "react-intl-universal";
export const menus = [
  { key: "/app/dashboard/index", title: intl.get("HOME_PAGE"), icon: "home" },
  {
    key: "/app/systemAccount",
    title: intl.get("SYSTEM_ACCOUNT"),
    icon: "user",
    sub: [
      {
        key: "/app/systemAccount/RoleManagement",
        title: intl.get("ROLE_MANAGEMENT"),
        icon: ""
      },
      {
        key: "/app/systemAccount/accountManager",
        title: intl.get("ACCOUNT_MANAGEMENT"),
        icon: ""
      }
    ]
  },
  {
    key: "/app/systemSetting",
    title: intl.get("SYSTEM_SETTING"),
    icon: "setting",
    sub: [
      {
        key: "/app/systemSetting/OperationLog",
        title: intl.get("ACTION_LOG"),
        icon: ""
      },
      {
        key: "/app/systemSetting/MessageTemplate",
        title: intl.get("MSG_MODAL"),
        icon: ""
      }
    ]
  },
  {
    key: "/app/userManagement",
    title: intl.get("USER_MANAGEMENT"),
    icon: "rocket",
    sub: [
      { key: '/app/userManagement/teacherManagement', title: intl.get('TEACHER_RECORD'), icon: '', },
      // { key: '/app/systemAccount/notifications', title: '通知提醒框', icon: '', },
      // { key: '/app/systemAccount/tabs', title: '标签页', icon: '', },
      { key: "/app/userManagement/studentsManagement",title: intl.get("STUDENT_RECORD"), icon: ""},
      { key: "/app/userManagement/relativesManagement",title: intl.get("RELATIVE_RECORD"), icon: ""}
    ]
  },
  {
    key: "/app/visitorManagement",
    title: intl.get("VISITOR_MANAGEMENT"),
    icon: "rocket",
    sub: [
      { key: "/app/visitorManagement/visitor", title: intl.get("VISITOR_FILE"), icon: "" },
      { key: "/app/visitorManagement/visitorRecord", title:intl.get("VISITOR_RECORD"), icon: "" }
    ]
  },
  {
    key: "/app/basicInfoManagement",
    title: intl.get("BASIC_INFO_MANAGEMENT"),
    icon: "copy",
    sub: [
      {
        key: "/app/basicInfoManagement/classesManagement",
        title: intl.get("CLASSES_MANAGEMENT"),
        icon: ""
      },
      {
        key: "/app/basicInfoManagement/facilitiesTypeManagement",
        title: intl.get("FACILITIES_TYPE_MANAGEMENT"),
        icon: ""
      },
      {
        key: "/app/basicInfoManagement/facilitiesManagement",
        title: intl.get("FACILITIES_MANAGEMENT"),
        icon: ""
      },
      {
        key: "/app/basicInfoManagement/mapManagement",
        title: intl.get("MAP_MANAGEMENT")
      }
    ]
  },
  {
    key: "/app/service",
    title: intl.get("SERVICE_MANAGEMENT"),
    icon: "edit",
    sub: [
      {
        key: "/app/service/meeting",
        title: intl.get("MEETING_MANAGEMENT"),
        icon: ""
      }, // { key: '/app/service/basicTable', title: '归寝管理', icon: '', },
      // { key: '/app/animation/banners', title: '亲属档案', icon: '', },
      // { key: '/app/systemAccount/map', title: '地图'},
      // { key: '/app/visitorManagement/stranger', title: '陌生人档案', icon: '', },
      // { key: '/app/visitorManagement/visitorRecord', title: '访问记录', icon: '', },
      // { key: '/app/visitorManagement/map', title: '地图'},
      // { key: '/app/service/advancedTable', title: '课堂点名管理', icon: '', },
      {
        key: "/app/service/backRoom",
        title: intl.get("BACK_ROOM_MANAGER"),
        icon: ""
      },
      {
        key: "/app/service/nameCourse",
        title: intl.get("NAME_COURSE_MANAGER"),
        icon: ""
      },
      {
        key: "/app/service/inOut",
        title: intl.get("IN_OUT_MANAGEMENT"),
        icon: ""
      }, // { key: '/app/service/map', title: '手机端查看页面'},
      { key: "/app/service/appSchoolRecord", title: "查看全校信息" }
    ]
  }
];
// {
//     key: '/sub4', title: '数据分析', icon: 'switcher',
//     sub: [
//         { key: '/login', title: '登录', icon: '', },
//         { key: '/404', title: '404', icon: '', },
//     ],
// },
// {
//     key: '/app/auth', title: '权限管理', icon: 'safety',
//     sub: [
//         { key: '/app/auth/basic', title: '基础演示', icon: '', },
//         { key: '/app/auth/routerEnter', title: '路由拦截', icon: '', },
//     ],
// },
// {
//     key: '/app/cssModule', title: 'cssModule演示', icon: 'star',
// },
