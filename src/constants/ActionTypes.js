export const REQUEST_DATA = 'REQUEST_DATA'

export const RECEIVE_DATA = 'RECEIVE_DATA'

// APP
export const GET_ALL_CAMERAS = 'GET_ALL_CAMERAS'

// Login
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS'
export const LOGIN_FAIL = 'LOGIN_FAIL'
export const LOGIN_CHANGE_LOGIN_TYPE = 'LOGIN_CHANGE_LOGIN_TYPE'
export const LOGOUT = 'LOGOUT'
// studentsManagement
export const SHOW_MULTI_REGISTER_MODAL = 'SHOW_MULTI_REGISTER_MODAL'
export const ADD_STUDENTS_INFO = 'ADD_STUDENTS_INFO'
export const STUDENTS_RECORD_LIST = 'STUDENTS_RECORD_LIST'
export const RELATIVE_MANAGEMENT = 'RELATIVE_MANAGEMENT'
export const RELATIVE_UPLOAD = 'RELATIVE_UPLOAD'
// visitorManagement
export const VISITOR_RECORD = 'VISITOR_RECORD'
export const VISITOR_MANAGEMENT = 'VISITOR_MANAGEMENT'
// teachersManagement
export const TEACHERS_MANAGEMENT = 'TEACHERS_MANAGEMENT'
export const TEACHERS_UPLOAD = 'TEACHERS_UPLOAD'
export const ADD_TEACHERS_INFO='ADD_TEACHERS_INFO'
// inOutSchoolManagement
export const SET_IN_OUT_SCHOOL_OPTION = 'SET_IN_OUT_SCHOOL_OPTION'
export const GET_IN_OUT_SCHOOL_OPTION = 'GET_IN_OUT_SCHOOL_OPTION'
export const CHANGE_IN_OUT_SCHOOL_OPTION = 'CHANGE_IN_OUT_SCHOOL_OPTION'
export const GET_STUDENT_ATTENDANCE_RECORD = 'GET_STUDENT_ATTENDANCE_RECORD'
export const UPDATE_STUDENT_ATTENDANCE = 'UPDATE_STUDENT_ATTENDANCE'
// meetingManagement
export const UPDATE_MEETING_LIST = 'UPDATE_MEETING_LIST'
export const UPDATE_MEETING_DETAIL = 'UPDATE_MEETING_DETAIL'
export const UPDATE_MEETING_PERSON_LIST = 'UPDATE_MEETING_PERSON_LIST'
// ClassManagement
export const RENDER_GRADE_TREE_LIST = 'RENDER_GRADE_TREE_LIST'
export const TOGGLE_SPIN = 'TOGGLE_SPIN'

// FacilitiesTypeListManagement
export const RENDER_FACILITIES_TYPE_LIST = 'RENDER_FACILITIES_TYPE_LIST'
export const TOGGLE_FACILITIES_TYPE_LIST_SPIN = 'TOGGLE_FACILITIES_TYPE_LIST_SPIN'

// FacilitiesManagement
export const TOGGLE_FACILITIES_LIST_SPIN = 'TOGGLE_FACILITIES_LIST_SPIN'
export const RENDER_FACILITIES_LIST = 'RENDER_FACILITIES_LIST'

//MapManagement
export const TOGGLE_MAP_LIST_SPIN = "toogle_map_list_spin";
export const RENDER_MAP_LIST = "render_map_list";

//账号管理
export const GET_ALL_USER_ACCOUNT = 'GET_ALL_USER_ACCOUNT';
export const GET_ALL_ROLES = 'GET_ALL_ROLES';
