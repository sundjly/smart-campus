window.config = {
  version: '1.0.0-zhxy',
  projectName: 'Zhxy',
  systemName: '智慧校园', //系统名称
  environments: {
      bjw: {
          host: 'localhost:3006',
          // host: '192.168.6.150:3006',
          //端口配置。
          centerProxyServerPrefix: 'http://192.168.11.165', //中心代理服务器前缀
          // apiBaseURL: 'http://192.168.2.15:8092', //api端口
          // apiBaseURL: 'http://192.168.11.193:8066', //api端口  测试服务器
          // apiBaseURL: 'http://192.168.11.165:8083', //api端口
          apiBaseURL: 'http://192.168.11.58:8083', //api端口
          mqttServer: {
              host: '192.168.11.165',
              port: 9001
          },
          domain: {
              replaceDomain: false
          },
          pathMiddle: ""
      },
      ifaas: {
          host: '192.168.11.193:8066',
          apiBaseURL: 'http://192.168.11.65:8083', //api端口
          centerProxyServerPrefix: 'http://192.168.11.165', //中心代理服务器前缀
          mqttServer: {
              host: '192.168.11.65',
              port: 9001
          },
          domain: {
              replaceDomain: false
          },
          pathMiddle: ""
      },
      internet: {
          host: '183.3.223.120:8066',
          apiBaseURL: 'http://183.3.223.120:7083', //api端口
          centerProxyServerPrefix: 'http://192.168.11.165', //中心代理服务器前缀
          mqttServer: {
              host: '192.168.11.65',
              port: 9001
          },
          pathMiddle: "",
          domain: {
              replaceDomain: true,
              path: {
                1:{
                  src: /192.168.11.193:80/g,
                  dst: '183.3.223.120:7081'
                },
                2:{
                  src: /192.168.11.193:8066/g,
                  dst: '183.3.223.120:7081'
                },
                3:{
                  src: /192.168.2.27:80/g,
                  dst: '183.3.223.120:7081'
                },
                4:{
                  src: /192.168.11.193/g,
                  dst: '183.3.223.120:7081'
                }
              }
          }
      }
  },
  // centerProxyServerPrefix: 'http://192.168.11.165', //中心代理服务器前缀
  keepLogin: true, // 是否保持登录
  tokenInvalid: 1, // token保存时间(天)
  streamMediaConfig: {
    mediaType: 'rtmp',
    playStartGap: 20000,
    playEndGap: 2000,
    hasStreamService: true
  },
  systemDateRange: 90, // (系统时间范围，单位:天),
  playMode: 'rtmp'
}
